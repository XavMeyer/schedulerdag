//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//============================================================================
// Name        : main.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================


using namespace std;

#define DEBUG_MC3 1

#include <stddef.h>

#include "Parallel/Manager/MpiManager.h"
#include "Utils/Code/IDManagement/IDManager.h"
#include "Utils/XML/ReaderXML.h"


using namespace Sampler::Strategies;
using namespace StatisticalModel;
using namespace StatisticalModel::Likelihood;

void testIdManager() {
	Utils::IDManagement::IDManager mgr;

	mgr.markAsUsed(5);
	std::cout << "Id 5 is written as used" << std::endl;

	std::cout << "-----------------------------------------" << std::endl;
	for(size_t i=0; i<20; ++ i) {
		std::cout << i << " >> Allocate : " << mgr.allocateId() << std::endl;
	}
	std::cout << "-----------------------------------------" << std::endl;
	size_t nb[5] = {2, 5, 7, 9, 19};
	for(size_t i=0; i<5; ++i) {
		mgr.freeId(nb[i]);
		std::cout << i << " >> Release : " << nb[i] << std::endl;
	}
	std::cout << "-----------------------------------------" << std::endl;
	for(size_t i=0; i<20; ++ i) {
		std::cout << i << " >> Allocate : " << mgr.allocateId() << std::endl;
	}
	std::cout << "-----------------------------------------" << std::endl;

}

void testMolecularDefinitionProfiles() {
	/*
	std::cout << "NB PAIRS : " << MD::getNucleotidesPairs()->NB_NUCL_PAIR << std::endl;
	std::cout << "NB PROF PAIRS : " << MD::getNucleotidesPairProfiles()->NB_POSSIBLE_PROFILES << std::endl;
	for(size_t i=0; i<MD::getNucleotidesPairProfiles()->ALL_POSSIBLE_PROFILES.size(); ++i) {
		std::cout << i  << "\t" << MD::getNucleotidesPairProfiles()->profileToString(MD::getNucleotidesPairProfiles()->ALL_POSSIBLE_PROFILES[i]) << std::endl;
	}
	std::cout << "-----------------------------------------" << std::endl;

	for(size_t i=0; i<vecProfilesId.size(); ++i) {
		MD::profile_t profile = MD::getNucleotidesPairProfiles()->idToProfile(vecProfilesId[i]);
		std::cout << i << "\t" << vecProfilesId[i] << "\t" << MD::getNucleotidesPairProfiles()->profileToString(profile) << std::endl;
	}
	getchar();
	*/
}

void removeGaps() {
	namespace DL = ::MolecularEvolution::DataLoader;

	std::vector<std::string> files = boost::assign::list_of<std::string>("ENSGT00390000004753/Euteleostomi/originalAllignment_ENSGT00390000004753.Euteleostomi.001.fasta")
	("ENSGT00390000005200/Euteleostomi/originalAllignment_ENSGT00390000005200.Euteleostomi.001.fasta")
	("ENSGT00390000014814/Euteleostomi/originalAllignment_ENSGT00390000014814.Euteleostomi.001.fasta")
	("ENSGT00390000018085/Euteleostomi/originalAllignment_ENSGT00390000018085.Euteleostomi.001.fasta")
	("ENSGT00390000018808/Euteleostomi/originalAllignment_ENSGT00390000018808.Euteleostomi.002.fasta")
	("ENSGT00530000063476/Euteleostomi/originalAllignment_ENSGT00530000063476.Euteleostomi.001.fasta")
	("ENSGT00550000074423/Euteleostomi/originalAllignment_ENSGT00550000074423.Euteleostomi.001.fasta")
	("ENSGT00550000074566/Euteleostomi/originalAllignment_ENSGT00550000074566.Euteleostomi.001.fasta")
	("ENSGT00560000076937/Euteleostomi/originalAllignment_ENSGT00560000076937.Euteleostomi.001.fasta")
	("ENSGT00600000084009/Euteleostomi/originalAllignment_ENSGT00600000084009.Euteleostomi.001.fasta")
	("ENSGT00600000084235/Euteleostomi/originalAllignment_ENSGT00600000084235.Euteleostomi.001.fasta")
	("ENSGT00620000087601/Euteleostomi/originalAllignment_ENSGT00620000087601.Euteleostomi.002.fasta")
	("ENSGT00640000091102/Euteleostomi/originalAllignment_ENSGT00640000091102.Euteleostomi.001.fasta")
	("ENSGT00660000095099/Euteleostomi/originalAllignment_ENSGT00660000095099.Euteleostomi.001.fasta")
	("ENSGT00660000095099/Euteleostomi/originalAllignment_ENSGT00660000095099.Euteleostomi.003.fasta")
	("ENSGT00660000095126/Euteleostomi/originalAllignment_ENSGT00660000095126.Euteleostomi.001.fasta")
	("ENSGT00660000095289/Euteleostomi/originalAllignment_ENSGT00660000095289.Euteleostomi.001.fasta")
	("ENSGT00660000095289/Euteleostomi/originalAllignment_ENSGT00660000095289.Euteleostomi.003.fasta")
	("ENSGT00660000095289/Euteleostomi/originalAllignment_ENSGT00660000095289.Euteleostomi.004.fasta")
	("ENSGT00660000095396/Euteleostomi/originalAllignment_ENSGT00660000095396.Euteleostomi.001.fasta")
	("ENSGT00660000095485/Euteleostomi/originalAllignment_ENSGT00660000095485.Euteleostomi.001.fasta")
	("ENSGT00670000097815/Euteleostomi/originalAllignment_ENSGT00670000097815.Euteleostomi.002.fasta")
	("ENSGT00670000097815/Euteleostomi/originalAllignment_ENSGT00670000097815.Euteleostomi.003.fasta")
	("ENSGT00670000097815/Euteleostomi/originalAllignment_ENSGT00670000097815.Euteleostomi.004.fasta")
	("ENSGT00680000099518/Euteleostomi/originalAllignment_ENSGT00680000099518.Euteleostomi.001.fasta")
	("ENSGT00680000099543/Euteleostomi/originalAllignment_ENSGT00680000099543.Euteleostomi.003.fasta")
	("ENSGT00680000099581/Euteleostomi/originalAllignment_ENSGT00680000099581.Euteleostomi.001.fasta")
	("ENSGT00680000099584/Euteleostomi/originalAllignment_ENSGT00680000099584.Euteleostomi.002.fasta")
	("ENSGT00680000099904/Euteleostomi/originalAllignment_ENSGT00680000099904.Euteleostomi.001.fasta")
	("ENSGT00680000099915/Euteleostomi/originalAllignment_ENSGT00680000099915.Euteleostomi.001.fasta");


	for(size_t i=0; i<files.size(); ++i) {
		std::string fileAlign = "/data/pMCMC/Coev/Dataset/EmpiricData/selectome/" + files[i];

		std::cout << "Processing : " << fileAlign << std::endl;

		// READ
		DL::FastaReader fr(fileAlign);
		DL::MSA msa(DL::MSA::NUCLEOTIDE, fr.getAlignments(), false);

		// CLEAR
		std::vector<size_t> corresp;
		std::vector< std::string > taxaNames(msa.getOrderedTaxaNames());
		std::vector< std::string > cleanedAlignements(taxaNames.size());

		for(size_t iT=0; iT<taxaNames.size(); ++iT) {
			DL::Alignment align = msa.getAlignment(taxaNames[iT]);
			std::string fullAlign(align.getAligmnmentAsString());
			assert(fullAlign.size() == msa.getNSite());

			for(size_t iS=0; iS<msa.getNSite(); ++iS) {
				if(!msa.isAllGapSite(iS)) {
					cleanedAlignements[iT].push_back(fullAlign[iS]);
					if(iT == 0) {
						corresp.push_back(iS);
					}
				}
			}
		}

		// WRITE
		// Corespondance
		std::string fnCorresp(fileAlign);
		boost::replace_all(fnCorresp, "originalAllignment", "corresp_original");
		std::cout << "Writing " << fnCorresp << std::endl;
		std::ofstream oFileCorresp(fnCorresp.c_str());
		for(size_t iC=0; iC<corresp.size(); ++iC) {
			oFileCorresp << iC << "\t" << corresp[iC] << std::endl;
		}
		oFileCorresp.close();

		// Fasta
		std::string fnFasta(fileAlign);
		boost::replace_all(fnFasta, "originalAllignment", "filteredAllignment");
		std::cout << "Writing " << fnFasta << std::endl;
		std::ofstream oFileFasta(fnFasta.c_str());
		for(size_t iT=0; iT<taxaNames.size(); ++iT) {
			oFileFasta << ">" << taxaNames[iT] << std::endl;
			for(size_t iS=0; iS<cleanedAlignements[iT].size(); ++iS) {
				oFileFasta << cleanedAlignements[iT][iS];
			}
			oFileFasta << std::endl;
		}
		oFileFasta.close();
		std::cout << "----------------------------" << std::endl;
	}
}



int main(int argc, char** argv) {

	//testNewFunctionnalities();

	// Init the MPI environnement
	Parallel::mpiMgr().init(&argc, &argv);

	XML::ReaderXML readerXML(argv[1]);
	readerXML.run();
	return 0;
}
