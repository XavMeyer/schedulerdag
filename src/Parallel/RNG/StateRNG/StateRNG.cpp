//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file StateRNG.cpp
 *
 * @date Sep 9, 2015
 * @author meyerx
 * @brief
 */
#include "StateRNG.h"

#include <boost/serialization/nvp.hpp>

StateRNG::StateRNG() {

	seed = 0;
	o_cnt = 0;
	s0 = s1 = s2 = s3 = 0;
	k0 = k1 = k2 = k3 = 0;

}

StateRNG::~StateRNG() {
}

template<class Archive>
void StateRNG::serialize(Archive & ar, const unsigned int version) {
	ar & BOOST_SERIALIZATION_NVP( seed );
	ar & BOOST_SERIALIZATION_NVP( o_cnt );
	ar & BOOST_SERIALIZATION_NVP( s0 );
	ar & BOOST_SERIALIZATION_NVP( s1 );
	ar & BOOST_SERIALIZATION_NVP( s2 );
	ar & BOOST_SERIALIZATION_NVP( s3 );
	ar & BOOST_SERIALIZATION_NVP( k0 );
	ar & BOOST_SERIALIZATION_NVP( k1 );
	ar & BOOST_SERIALIZATION_NVP( k2 );
	ar & BOOST_SERIALIZATION_NVP( k3 );
}

std::string StateRNG::toString() const {
	std::stringstream ss;
	ss << seed << "\t" << o_cnt << "\t" << s0 << "\t" << s1 << "\t" << s2 << "\t" << s3 << "\t" << k0 << "\t" << k1 << "\t" << k2 << "\t" << k3;
	return ss.str();
}

template void StateRNG::serialize<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version);
template void StateRNG::serialize<boost::archive::text_iarchive>(boost::archive::text_iarchive & ar, const unsigned int version);
template void StateRNG::serialize<boost::archive::xml_oarchive>(boost::archive::xml_oarchive & ar, const unsigned int version);
template void StateRNG::serialize<boost::archive::xml_iarchive>(boost::archive::xml_iarchive & ar, const unsigned int version);
