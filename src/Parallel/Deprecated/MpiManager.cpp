//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * MpiManager.cpp
 *
 *  Created on: 30 sept. 2013
 *      Author: meyerx
 */

#include "MpiManager.h"

namespace Parallel {

const int MpiManager::MainProcessor = 0;

MpiManager::MpiManager() : globalCom(MPI_COMM_WORLD), globalGroup(MPI_GROUP_NULL), proposalGroup(MPI_GROUP_NULL), proposalCom(MPI_COMM_WORLD),  myRank(0), nProc(0), topology() {
	prng = RNG::createSitmo11RNG();
}

MpiManager::~MpiManager() {
	delete prng;
	MPI_Finalize();
}

void MpiManager::init(int *argc, char ***argv){
	MPI_Init(argc, argv);
	MPI_Comm_rank(globalCom, &myRank);
	MPI_Comm_size(globalCom, &nProc);

	// Extract the original group handle
	MPI_Comm_group(MPI_COMM_WORLD, &globalGroup);

	topology.init(myRank, nProc);
	prng->setSeed(RNG::DEFAULT_SEED*myRank+12);
}

void MpiManager::defineTopology(const int aNProposal, const int aNChainMCMC, const int aNChainMC3){
	topology.setNProposalWorker(aNProposal);
	topology.setNChainMCMC(aNChainMCMC);
	topology.setNChainMC3(aNChainMC3);
	topology.defineRole();
	if(topology.checkRoleProposal(topology.Manager) || topology.checkRoleProposal(topology.Worker)) {
		initProposalCommunicator();
	}
}

void MpiManager::initProposalCommunicator() {
	if(topology.checkRoleProposal(topology.None)) return;

	// Get Ranks
	int nRanks = 1+topology.getWorkerProposal().size();
	int ranks[nRanks];
	ranks[0] = topology.getManagerProposal();
	for(int iS=0; iS<nRanks-1; ++iS){
		ranks[iS+1] = topology.getWorkerProposal()[iS];
	}

	// Create new group
	MPI_Group_incl(globalGroup, nRanks, ranks, &proposalGroup);

	// Create new communicator and then perform collective communications
	MPI_Comm_create(MPI_COMM_WORLD, proposalGroup, &proposalCom);
}

template <>
void MpiManager::send<double>(double* data, const int size, const int dest, const tag_t tag, const MPI_Comm com) const {
	MPI_Send(data, size, MPI_DOUBLE, dest, tag, com);
}

template <>
void MpiManager::send<char>(char* data, const int size, const int dest, const tag_t tag, const MPI_Comm com) const {
	MPI_Send(data, size, MPI_CHAR, dest, tag, com);
}

template <>
void MpiManager::send<int>(int* data, const int size, const int dest, const tag_t tag, const MPI_Comm com) const {
	MPI_Send(data, size, MPI_INT, dest, tag, com);
}

template <>
void MpiManager::send<size_t>(size_t* data, const int size, const int dest, const tag_t tag, const MPI_Comm com) const {
	MPI_Send(data, size, MPI_INT, dest, tag, com);
}

template <>
void MpiManager::recv<double>(double* data, const int size, const int dest, const tag_t tag, const MPI_Comm com) const {
	MPI_Status status;
	MPI_Recv(data, size, MPI_DOUBLE, dest, tag, com, &status);
}

template <>
void MpiManager::recv<char>(char* data, const int size, const int dest, const tag_t tag, const MPI_Comm com) const {
	MPI_Status status;
	MPI_Recv(data, size, MPI_CHAR, dest, tag, com, &status);
}

template <>
void MpiManager::recv<int>(int* data, const int size, const int dest, const tag_t tag, const MPI_Comm com) const {
	MPI_Status status;
	MPI_Recv(data, size, MPI_INT, dest, tag, com, &status);
}

template <>
void MpiManager::recv<size_t>(size_t* data, const int size, const int dest, const tag_t tag, const MPI_Comm com) const {
	MPI_Status status;
	MPI_Recv(data, size, MPI_UNSIGNED_LONG, dest, tag, com, &status);
}

template <>
void MpiManager::bcast<double>(double* data, const int size, const int root, const MPI_Comm com) const {
	MPI_Bcast(data, size, MPI_DOUBLE, root, com);
}


template <>
void MpiManager::exchangeAll<double>(int mySize, double* myData, int* sizes, double* data, const MPI_Comm com) const {
	uint nProc = topology.getNProposal()+1;
	int displs[nProc];

	// Compute displacement vectors
	displs[0] = 0;
	for(uint i=1; i<nProc; ++i){
		displs[i] = displs[i-1] + sizes[i-1];
	}
	MPI_Allgatherv(myData, mySize, MPI_DOUBLE, data, sizes, displs, MPI_DOUBLE, com);
}

template <>
void MpiManager::bcast<char>(char* data, const int size, const int root, const MPI_Comm com) const {
	MPI_Bcast(data, size, MPI_CHAR, root, com);
}

template <>
void MpiManager::bcast<int>(int* data, const int size, const int root, const MPI_Comm com) const {
	MPI_Bcast(data, size, MPI_INT, root, com);
}

template <>
void MpiManager::bcast<size_t>(size_t* data, const int size, const int root, const MPI_Comm com) const {
	MPI_Bcast(data, size, MPI_UNSIGNED_LONG, root, com);
}

void MpiManager::sendSerializedSVec(const int size, char* data, const int dest) const {
	send(data, size, dest, MCMC_TAG, globalCom);
}

void MpiManager::recvSerializedSVec(const int size, char *data, const int src) const {
	recv(data, size, src, MCMC_TAG, globalCom);
}

void MpiManager::sendSerializedSample(const int size, char* data, const int dest) const {
	send(data, size, dest, PROPOSAL_TAG, globalCom);
}

void MpiManager:: recvSerializedSample(const int size, char *data, const int src) const {
	recv(data, size, src, PROPOSAL_TAG, globalCom);
}

void MpiManager::sendScores(const int size, double *data, const int dest) const {
	send(data, size, dest, PROPOSAL_TAG, globalCom);
}

void MpiManager::recvScores(const int size, double *data, const int src) const{
	recv(data, size, src, PROPOSAL_TAG, globalCom);
}

void MpiManager::gatherScores(const int size, double* inData, double* outData, const int root) const {
	int curRoot, newRoot, mSize;
	curRoot = root;
	MPI_Group_translate_ranks(globalGroup, 1, &curRoot, proposalGroup, &newRoot);
	// Compute recvCount and displ
	// Gather
	mSize = size;
	MPI_Gather(inData, mSize, MPI_DOUBLE, outData, mSize, MPI_DOUBLE, newRoot, proposalCom);
}

void MpiManager::sendProcessedValues(const double aPrior, const double aLikelihood, const double aPosterior, const int dest) const {
	const size_t size=3;
	double data[size];
	// Format the data into an array
	data[0] = aPrior;
	data[1] = aLikelihood;
	data[2] = aPosterior;
	// Send the data
	send(data, size, dest, PROPOSAL_TAG, globalCom);
}

void MpiManager::recvProcessedValues(double &aPrior, double &aLikelihood, double &aPosterior, const int src) const {
	const size_t size=3;
	double data[size];
	// Receive the data
	recv(data, size, src, PROPOSAL_TAG, globalCom);
	// Unpack the data
	aPrior = data[0];
	aLikelihood = data[1];
	aPosterior = data[2];
}

void MpiManager::sendIdBlock(size_t idBlock, const int dest) const {

	send(&idBlock, 1, dest, PROPOSAL_TAG, globalCom);
}

void MpiManager::recvIdBlock(size_t &idBlock, const int src) const {
	recv(&idBlock, 1, src, PROPOSAL_TAG, globalCom);
}

void MpiManager::sendLastRank(int lastARank, const int dest) const {
	send(&lastARank, 1, dest, PROPOSAL_TAG, globalCom);
}

void MpiManager::recvLastRank(int &lastARank, const int src) const {
	recv(&lastARank, 1, src, PROPOSAL_TAG, globalCom);
}

void MpiManager::broadcastValues(const int size, int *data) const {
	bcast(data, size, MainProcessor, globalCom);
}

void MpiManager::broadcastValues(const int size, double *data) const {
	bcast(data, size, MainProcessor, globalCom);
}

void MpiManager::sendSerializedState(const int size, char* data, const int dest) const {
	send(data, size, dest, PROPOSAL_TAG, globalCom);
}

void MpiManager:: recvSerializedState(const int size, char* data, const int src) const {
	recv(data, size, src, PROPOSAL_TAG, globalCom);
}

const Topology& MpiManager::getTopology() const {
	return topology;
}

const RNG* MpiManager::getPRNG() const {
	return prng;
}

const int MpiManager::getRank() const {
	return myRank;
}

const int MpiManager::getNProc() const {
	return nProc;
}


bool MpiManager::isMainProcessor() const {
	return myRank == MainProcessor;
}

void MpiManager::setSeedPRNG(unsigned long int aSeed) {
	prng->setSeed(aSeed);
}

} /* namespace pMCMC */
