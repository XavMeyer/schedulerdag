//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * MpiManager.h
 *
 *  Created onJ': 30 sept. 2013
 *      Author: meyerx
 */

#ifndef MPIMANAGER_H_
#define MPIMANAGER_H_

#include "Topology.h"
#include "Parallel/RNG/RNG.h"

#include <stdlib.h>
#include <mpi.h>
#include <vector>
#include <fstream>
#include <iostream>
using namespace std;

namespace Parallel {

class MpiManager : private Uncopyable {

private:

	static const int MainProcessor;

	enum tag_t {DEFAULT_TAG=0, PROPOSAL_TAG=1, MCMC_TAG=2, MC3_TAG=3};

	const MPI_Comm globalCom;
	MPI_Group globalGroup, proposalGroup;
	MPI_Comm proposalCom;
	int myRank, nProc;

	Topology topology;
	RNG* prng;

	friend MpiManager& mpiMgr();

public:

	void abort(int errCode=0){
		MPI_Abort(globalCom, errCode);
	}

	void print(const string &str){
		cout << "[" << myRank << "] " << str << endl;
	}

	void init(int *argc, char ***argv);

	void defineTopology(const int aNProposal=1, const int aNChainMCMC=1, const int aNChainMC3=0);

	void sendSerializedSVec(const int size, char* data, const int dest) const;
	void recvSerializedSVec(const int size, char *data, const int src) const;

	void sendSerializedSample(const int size, char* data, const int dest) const;
	void recvSerializedSample(const int size, char *data, const int src) const;

	void sendScores(const int size, double *data, const int dest) const;
	void recvScores(const int size, double *data, const int src) const;
	void gatherScores(const int size, double *inData, double *outData, const int root) const;

	void sendProcessedValues(const double aPrior, const double aLikelihood, const double aPosterior, const int dest) const;
	void recvProcessedValues(double &aPrior, double &aLikelihood, double &aPosterior, const int src) const;

	void sendIdBlock(size_t idBlock, const int dest) const;
	void recvIdBlock(size_t &idBlock, const int src) const;

	void sendLastRank(int lastARank, const int dest) const;
	void recvLastRank(int &lastARank, const int src) const;

	void sendSerializedState(const int size, char* data, const int dest) const;
	void recvSerializedState(const int size, char* data, const int src) const;

	void broadcastValues(const int size, int *data) const;
	void broadcastValues(const int size, double *data) const;

	template <typename T>
	void bcastProposal(const int size, T* data, const int root) const {
		int curRoot, newRoot;
		curRoot = root;
		MPI_Group_translate_ranks(globalGroup, 1, &curRoot, proposalGroup, &newRoot);
		bcast<T>(data, size, newRoot, proposalCom);
	}

	template <typename T>
	void exchangeAllProposal(int mySize, T* myData, int* sizes, T* data) const {
		exchangeAll<T>(mySize, myData, sizes, data, proposalCom);
	}

	const Topology& getTopology() const;
	const RNG* getPRNG() const;
	const int getRank() const;
	const int getNProc() const;

	bool isMainProcessor() const;

	void setSeedPRNG(unsigned long int aSeed);

	//ofstream& getOFile();

private:
	// Defined in the body
	MpiManager();
	~MpiManager();
	// Not defined to avoid call
	MpiManager(const MpiManager&);
	MpiManager& operator=(const MpiManager&);

	template <typename T>
	void send(T* data, const int size, const int dest, const tag_t tag, const MPI_Comm com) const;

	template <typename T>
	void recv(T* data, const int size, const int dest, const tag_t tag, const MPI_Comm com) const;

	template <typename T>
	void bcast(T* data, const int size, const int root, const MPI_Comm com) const;

	template <typename T>
	void exchangeAll(int mySize, T* myData, int* sizes, T* data, const MPI_Comm com) const;

	template <typename T, tag_t TAG>
	void bcast(const int size, T* data, const int root, const vector<int> &worker, const int manager) const {
		// If i'm the root i send the data
		if(myRank == root) {
			// Send to each workers
			for(size_t iW=0; iW < worker.size(); ++iW){
				int dest = worker[iW];
				// If the destination is myself, i'm a worker and must
				// send to the manager
				if(myRank == dest) {
					dest = manager;
				}
				send(data, size, dest, TAG, proposalCom);
			}
		} else { // else I receive and set the state
			recv(data, size, root, TAG, proposalCom);
		}
	}

	void initProposalCommunicator();

};


inline MpiManager& mpiMgr() {
    static MpiManager instance;
    return instance;
}

} /* namespace pMCMC */
#endif /* MPIMANAGER_H_ */
