//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Topology.h
 *
 *  Created on: 1 oct. 2013
 *      Author: meyerx
 */

#ifndef TOPOLOGY_H_
#define TOPOLOGY_H_

#include "Utils/Uncopyable.h"

#include <sstream>
#include <string>
#include <stdlib.h>
#include <vector>
using namespace std;

namespace Parallel {

class Topology : private Uncopyable {

public:

	enum role_t {None=0, Worker=1, Manager=2, Sequential=3};

public:

	Topology();
	Topology(int aMyRank, int aNProc);
	virtual ~Topology();

	void init(int aMyRank, int aNProc);

	void defineRole();

	void setNChainMC3(int aNChainMC3);
	void setNChainMCMC(int aNChainMCMC);
	void setNProposalWorker(int aNProposalWorker);

	bool isMCMCSequential() const;
	bool isProposalSequential() const;

	bool checkRoleProposal(role_t aRole) const;
	bool checkRoleMCMC(role_t aRole) const;

	int getManagerMCMC() const;
	const vector<int>& getWorkerMCMC() const;

	int getManagerProposal() const;
	const vector<int>& getWorkerProposal() const;


	int getNProposal() const;
	int getNChain() const;
	int getIdProposal() const;
	int getIdMCMC() const;

	string toString() const;

private:

	void defineRoleProposal();
	void defineRoleMCMC();
	void defineRoleMC3();

	string roleToString(role_t myRole) const;

private:

	int myRank, nProc;
	int nChainMC3, nChainMCMC, nProposalWorker;
	role_t roleProposal, roleMCMC, roleMC3;
	int idProposal, idMCMC, idMC3;

	int managerProposal, managerMCMC, managerMC3;
	vector<int> workerProposal, workerMCMC;

};

} /* namespace pMCMC */
#endif /* TOPOLOGY_H_ */
