//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Topology.cpp
 *
 *  Created on: 1 oct. 2013
 *      Author: meyerx
 */

#include "Topology.h"

namespace Parallel {

Topology::Topology() : roleProposal(None), roleMCMC(None), roleMC3(None) {
	myRank = nProc = managerProposal = managerMCMC = managerMC3 = -1;
	nChainMC3 = nChainMCMC = nProposalWorker = 1;
	idProposal = idMCMC = idMC3 = -1;
}

Topology::Topology(int aMyRank, int aNProc) : myRank(aMyRank), nProc(aNProc), roleProposal(None), roleMCMC(None), roleMC3(None) {
	managerProposal = managerMCMC = managerMC3 = -1;
	nChainMC3 = nChainMCMC = nProposalWorker = 1;
	idProposal = idMCMC = idMC3 = -1;
}

Topology::~Topology() {
}

void Topology::init(int aMyRank, int aNProc){
	myRank = aMyRank;
	nProc = aNProc;
}

void Topology::defineRole(){

	roleProposal = None;
	roleMCMC = None;
	roleMC3 = None;

	managerProposal = -1;
	managerMCMC = -1;

	idProposal = idMCMC = idMC3 = -1;

	workerProposal.clear();
	workerMCMC.clear();

	if((nChainMC3==0 && myRank < (nProposalWorker*nChainMCMC)) ||
		myRank < (nProposalWorker*nChainMCMC*nChainMC3)){
		defineRoleProposal();
		defineRoleMCMC();
		defineRoleMC3();
	}
}

void Topology::defineRoleProposal(){
	if(nProposalWorker < 2 ) {
		idProposal = 0;
		roleProposal = Sequential;
		return;
	}
	// Get the positions
	div_t res = div(myRank, nProposalWorker);
	// - the remainder is the position into the proposal group
	// - the quotient is the position into the MCMC chains group
	// Set the parent for Proposal
	idProposal = res.rem;
	managerProposal = myRank - res.rem;


	// Set the roles
	if(managerProposal == myRank){
		roleProposal = Manager;
	} else {
		roleProposal = Worker;
	}
	// Memorize worker / childs
	for(int i=1; i<nProposalWorker; ++i){
		workerProposal.push_back(i+managerProposal);
	}
}

void Topology::defineRoleMCMC(){

	// If I'm only a proposal agent then I have no MCMC role
	if(roleProposal != Manager && roleProposal != Sequential){
		roleMCMC = None;
		return;
	}
	// If there are no parallel chain we are sequential
	if(nChainMCMC < 2 ) {
		roleMCMC = Sequential;
		return;
	}

	// If we are here we have to manage at least 1 chain.
	// Getting the positions
	div_t resProp = div(myRank, nProposalWorker);
	div_t resMCMC = div(resProp.quot, nChainMCMC);
	// - the remainder is the position into the parallel MCMC
	// - the quotient is the position into the MC3 group
	idMCMC = resMCMC.quot;

	// The parent for a parallel MCMC chain is :
	// The position of the parallel MCMC chain amongst all the pMCMC chain (MC3 group)
	// multiplied by the number of chain by MC3 group and the number of proposal
	managerMCMC = resMCMC.quot*nChainMCMC*nProposalWorker;

	// Set the roles
	if(managerMCMC == myRank){
		roleMCMC = Manager;
		// Memorise worker / childs
		for(int i=1; i<nChainMCMC; ++i){
			workerMCMC.push_back(managerMCMC+i*nProposalWorker);
		}
	} else {
		roleMCMC = Worker;
	}
}

void Topology::defineRoleMC3(){
	roleMC3 = None;
}

void Topology::setNChainMC3(int aNChainMC3){
	nChainMC3 = aNChainMC3;
}

void Topology::setNChainMCMC(int aNChainMCMC){
	nChainMCMC = aNChainMCMC;
}

void Topology::setNProposalWorker(int aNProposalWorker){
	nProposalWorker = aNProposalWorker;
}

bool Topology::isMCMCSequential() const {
	return roleMCMC == Sequential;
}

bool Topology::isProposalSequential() const {
	return roleProposal == Sequential;
}

bool Topology::checkRoleProposal(role_t aRole) const {
	return aRole == roleProposal;
}

bool Topology::checkRoleMCMC(role_t aRole) const {
	return aRole == roleMCMC;
}

int Topology::getManagerMCMC() const {
	return managerMCMC;
}

const vector<int>& Topology::getWorkerMCMC() const {
	return workerMCMC;
}

int Topology::getManagerProposal() const {
	return managerProposal;
}

const vector<int>& Topology::getWorkerProposal() const {
	return workerProposal;
}

int Topology::getNProposal() const {
	return workerProposal.size()+1;
}

int Topology::getNChain() const {
	return nChainMCMC;
}

string Topology::roleToString(role_t myRole) const {
	switch (myRole) {
		case None:
			return "None";
			break;
		case Worker:
			return "Worker";
			break;
		case Manager:
			return "Manager";
			break;
		case Sequential:
			return "Sequential";
			break;
	}

	return "----";
}

int Topology::getIdProposal() const {
	return idProposal;
}

int Topology::getIdMCMC() const {
	return idMCMC;
}

string Topology::toString() const {
	stringstream ss;
	ss << "nProposal=" << nProposalWorker << " & nMCMC=" << nChainMCMC << " & nMC3=" << nChainMC3 << endl;
	ss << "[Proposal] : " << roleToString(roleProposal);
	if(roleProposal == Manager) {
		ss << " (";
		for(size_t i=0; i<workerProposal.size(); ++i){
			ss << workerProposal[i] << ", ";
		}
		ss << ")";
	} else if (roleProposal == Worker) {
		ss << " (" << managerProposal << ")";
	}
	ss << endl;

	ss << "[MCMC] : " << roleToString(roleMCMC);
	if(roleMCMC == Manager) {
		ss << " (";
		for(size_t i=0; i<workerMCMC.size(); ++i){
			ss << workerMCMC[i] << ", ";
		}
		ss << ")";
	} else if (roleMCMC == Worker) {
		ss << "(" << managerMCMC << ")";
	}
	ss << endl;

	ss << "[MC3] : " << roleToString(roleMC3) << endl;
	return ss.str();
}

} /* namespace pMCMC */

