//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AGManager.cpp
 *
 * @date Jan 18, 2017
 * @author meyerx
 * @brief
 */
#include "AGManager.h"

#include <assert.h>

#include "Parallel/Manager/../Topology/BaseTopology.h"
#include "MpiManager.h"
#include "mpi.h"

namespace Parallel {

const size_t AGManager::MAX_PROC_PER_POP = 10;

AGManager::AGManager(){
	roleMigration = None;
	roleParallelPopulation = None;

	PARALLEL_POPULATION_LAYER_ID = 0;
	MIGRATION_LAYER_ID = 0;
}

AGManager::~AGManager() {
}

void AGManager::init(size_t layer, BaseTopology::sharedPtr_t basePtrBT) {

	const CommunicationLayer* parentLayer = basePtrBT->getCommunicationLayer(layer);

	size_t divisor = MAX_PROC_PER_POP;
	while(divisor > 0) {
		if(parentLayer->getLocalNProc() % divisor == 0){
			break;
		}
		divisor--;
	}

	int myRank = Parallel::mpiMgr().getRank();
	int nProc = Parallel::mpiMgr().getNProc();
	size_t nIsland = parentLayer->getLocalNProc()/divisor;
	size_t nPopulationEval = divisor;

	/*if(myRank == 0) {
		std::cout<< "Layer AG has " << parentLayer->getLocalNProc() << " nProc" << std::endl;
		std::cout << "Nb island : " << nIsland << " -- " << " Nb population : " << nPopulationEval << std::endl;
	}*/

	std::vector<size_t> nbProcPerLayer(3+layer);
	size_t iL=0;
	size_t prod = 1;
	for(; iL<layer; ++iL) {
		nbProcPerLayer[iL] = basePtrBT->getCommunicationLayer(iL)->getLocalNProc();
		prod *= nbProcPerLayer[iL];
	}

	nbProcPerLayer[iL] = nPopulationEval;
	prod *= nbProcPerLayer[iL];
	PARALLEL_POPULATION_LAYER_ID = iL;
	++iL;

	nbProcPerLayer[iL] = nIsland;
	prod *= nbProcPerLayer[iL];
	MIGRATION_LAYER_ID = iL;
	++iL;

	nbProcPerLayer[iL] = nProc/prod;
	assert(prod >= 1);

	ptrBT.reset(new BaseTopology(myRank, nProc, basePtrBT->getCommunicationLayer(0)->isSharedMemory(), nbProcPerLayer));
	defineRoles();

	setActive(true);
}

void AGManager::deinit() {

	setActive(false);

	// Deinit roles
	roleMigration = None;
	roleParallelPopulation = None;

	PARALLEL_POPULATION_LAYER_ID = 0;
	MIGRATION_LAYER_ID = 0;

	// Deinit layers
	ptrBT.reset();

}

void AGManager::defineRoles() {
	roleParallelPopulation = defineRole(ptrBT->getCommunicationLayer(PARALLEL_POPULATION_LAYER_ID));
	roleMigration = defineRole(ptrBT->getCommunicationLayer(MIGRATION_LAYER_ID));
}

bool AGManager::checkRolePopulation(role_t aRole)  const {
	return roleParallelPopulation == aRole;
}

bool AGManager::checkRoleMigration(role_t aRole)  const {
	return roleMigration == aRole;
}

bool AGManager::isSequentialPopulation() const {
	return roleParallelPopulation == Sequential;
}

bool AGManager::isSequentialMigration() const {
	return roleMigration == Sequential;
}

bool AGManager::isManagerPopulation() const {
	return (checkRolePopulation(Manager) ||
			checkRolePopulation(Sequential));
}

bool AGManager::isManagerMigration() const {
	return (checkRoleMigration(Manager) ||
			checkRoleMigration(Sequential));
}

void AGManager::gatherSizePopulation(int size, std::vector<int> &sizes) const {
	int myManagerRank = ptrBT->getCommunicationLayer(PARALLEL_POPULATION_LAYER_ID)->getMyMasterLocalRank();
	MPI_Comm myCom = ptrBT->getCommunicationLayer(PARALLEL_POPULATION_LAYER_ID)->getLocalCommMPI();

	// Recv buffer
	int finalSize = ptrBT->getCommunicationLayer(PARALLEL_POPULATION_LAYER_ID)->getLocalNProc();
	if(myManagerRank == ptrBT->getCommunicationLayer(PARALLEL_POPULATION_LAYER_ID)->getMyLocalRank()) {
		sizes.resize(finalSize);
	}

	MPI_Gather(&size, 1, MPI_INT,
				sizes.data(), 1, MPI_INT,
				myManagerRank, myCom);
}

void AGManager::gatherPopulationMgr(std::vector<int> recvCount, Utils::Serialize::buffer_t &buff, Utils::Serialize::buffer_t &result) const {
	int myManagerRank = ptrBT->getCommunicationLayer(PARALLEL_POPULATION_LAYER_ID)->getMyMasterLocalRank();
	MPI_Comm myCom = ptrBT->getCommunicationLayer(PARALLEL_POPULATION_LAYER_ID)->getLocalCommMPI();

	size_t totalSize=recvCount[0];
	std::vector<int> displs(recvCount.size(), 0);
	for(size_t iS=1; iS<recvCount.size(); ++iS) {
		totalSize += recvCount[iS];
		displs[iS] = displs[iS-1] + recvCount[iS-1];
	}
	result.resize(totalSize);

	MPI_Gatherv(buff.data(), buff.size(), MPI_CHAR,
			    result.data(), recvCount.data(), displs.data(), MPI_CHAR,
			    myManagerRank, myCom);
}

void AGManager::gatherPopulationWrk(Utils::Serialize::buffer_t &buff) const {
	int myManagerRank = ptrBT->getCommunicationLayer(PARALLEL_POPULATION_LAYER_ID)->getMyMasterLocalRank();
	MPI_Comm myCom = ptrBT->getCommunicationLayer(PARALLEL_POPULATION_LAYER_ID)->getLocalCommMPI();

	MPI_Gatherv(buff.data(), buff.size(), MPI_CHAR,
			    NULL, NULL, NULL, MPI_CHAR,
			    myManagerRank, myCom);
}

void AGManager::bcastSizePopulation(size_t &size) const {
	int myManager = ptrBT->getCommunicationLayer(PARALLEL_POPULATION_LAYER_ID)->getMyMasterLocalRank();
	MPI_Comm myComm = ptrBT->getCommunicationLayer(PARALLEL_POPULATION_LAYER_ID)->getLocalCommMPI();

	unsigned long int tmp = size;
	MPI_Bcast(&tmp, 1, MPI_LONG_INT, myManager, myComm);
	size = tmp;
}

void AGManager::bcastPopulation(size_t serializedSize, Utils::Serialize::buffer_t &buff) const {
	bcastIndividual(PARALLEL_POPULATION_LAYER_ID, serializedSize, buff);
}

void AGManager::bcastAdjMatrixPopulation(Eigen::MatrixXf &distMatrix) const {
	int myManager = ptrBT->getCommunicationLayer(PARALLEL_POPULATION_LAYER_ID)->getMyMasterLocalRank();
	MPI_Comm myComm = ptrBT->getCommunicationLayer(PARALLEL_POPULATION_LAYER_ID)->getLocalCommMPI();
	MPI_Bcast(distMatrix.data(), distMatrix.size(), MPI_FLOAT, myManager, myComm);
}

void AGManager::recvIndivMigration(Utils::Serialize::buffer_t &buff) const {
	recvIndividuals(MIGRATION_LAYER_ID, buff);
}

void AGManager::sendIndivMigration(Utils::Serialize::buffer_t &buff) const {
	sendIndividuals(MIGRATION_LAYER_ID, buff);
}

void AGManager::gatherIndivMigration(Utils::Serialize::buffer_t &buff, Utils::Serialize::buffer_t &result) const {
	gatherIndividuals(MIGRATION_LAYER_ID, buff, result);
}

void AGManager::bcastIndivMigration(size_t serializedSize, Utils::Serialize::buffer_t &buff) const {
	bcastIndividual(MIGRATION_LAYER_ID, serializedSize, buff);
}


void AGManager::recvIndividuals(layer_id_t layer, Utils::Serialize::buffer_t &buff) const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(layer);

	MPI_Status status;
	// Check if a message is pending (not blocking)
	MPI_Probe(MPI_ANY_SOURCE, PARALLEL_AG_TAG, commL->getLocalCommMPI(), &status);
	// Prepare the receive using the MPI_STATUS
	int src = status.MPI_SOURCE;
	//std::stringstream ss; ss << "After probe : " << src << std::endl; Parallel::mpiMgr().print(ss.str()); // FIXME
	// Create the temporary buffer
	int size = 0;
	MPI_Get_count(&status, MPI_CHAR, &size);
	buff.resize(size);
	// Read the message
	MPI_Recv(buff.data(), size, MPI_CHAR, src, PARALLEL_AG_TAG, commL->getLocalCommMPI(), &status);

	MPI_Barrier(commL->getLocalCommMPI());
}

void AGManager::sendIndividuals(layer_id_t layer, Utils::Serialize::buffer_t &buff) const {
	const CommunicationLayer *commL = ptrBT->getCommunicationLayer(layer);
	int nbr = (commL->getMyLocalRank() + 1) % commL->getLocalNProc();
	//std::stringstream ss; ss << "Sending to nbr : " << nbr << std::endl; Parallel::mpiMgr().print(ss.str()); // FIXME
	MPI_Request req; // We send the move string
	MPI_Isend(const_cast<char*>(buff.data()), buff.size(), MPI_CHAR, nbr, PARALLEL_AG_TAG, commL->getLocalCommMPI(), &req);
	MPI_Request_free(&req);
}

void AGManager::gatherIndividuals(layer_id_t layer, Utils::Serialize::buffer_t &buff, Utils::Serialize::buffer_t &result) const {
	int myManagerRank = ptrBT->getCommunicationLayer(layer)->getMyMasterLocalRank();
	MPI_Comm myCom = ptrBT->getCommunicationLayer(layer)->getLocalCommMPI();

	// Recv buffer
	int finalSize = buff.size()*ptrBT->getCommunicationLayer(layer)->getLocalNProc();
	if(myManagerRank == ptrBT->getCommunicationLayer(layer)->getMyLocalRank()) {
		result.resize(finalSize);
	}

	MPI_Gather(buff.data(), buff.size(), MPI_CHAR,
				result.data(), buff.size(), MPI_CHAR,
				myManagerRank, myCom);
}

void AGManager::bcastIndividual(layer_id_t layer, size_t serializedSize, Utils::Serialize::buffer_t &buff) const {
	int myManagerRank = ptrBT->getCommunicationLayer(layer)->getMyMasterLocalRank();
	MPI_Comm myCom = ptrBT->getCommunicationLayer(layer)->getLocalCommMPI();

	// Recv buffer
	if(myManagerRank != ptrBT->getCommunicationLayer(layer)->getMyLocalRank()) {
		buff.resize(serializedSize);
	} else {
		assert(buff.size() == serializedSize);
	}

	MPI_Bcast(buff.data(), serializedSize, MPI_CHAR, myManagerRank, myCom);
}

int AGManager::getMyRankParallelPop() const {
	return ptrBT->getCommunicationLayer(PARALLEL_POPULATION_LAYER_ID)->getMyLocalRank();
}

int AGManager::getNProcParallelPop() const {
	return ptrBT->getCommunicationLayer(PARALLEL_POPULATION_LAYER_ID)->getLocalNProc();
}

int AGManager::getMyRankMigration() const {
	return ptrBT->getCommunicationLayer(MIGRATION_LAYER_ID)->getMyLocalRank();
}

int AGManager::getNProcMigation() const {
	return ptrBT->getCommunicationLayer(MIGRATION_LAYER_ID)->getLocalNProc();
}

} /* namespace Parallel */
