//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AGManager.h
 *
 * @date Jan 18, 2017
 * @author meyerx
 * @brief
 */
#ifndef AGMANAGER_H_
#define AGMANAGER_H_

#include <stddef.h>
#include <Eigen/Core>

#include "../Topology/BaseTopology.h"
#include "BaseManager.h"
#include "Utils/Code/SerializationSupport.h" // IWYU pragma: keep

namespace Parallel {

class MCMCManager;
class MLManager;

class AGManager : public BaseManager {
public:
	void gatherSizePopulation(int size, std::vector<int> &sizes) const;
	void gatherPopulationMgr(std::vector<int> recvCount, Utils::Serialize::buffer_t &buff, Utils::Serialize::buffer_t &result) const;
	void gatherPopulationWrk(Utils::Serialize::buffer_t &buff) const;
	void bcastSizePopulation(size_t &size) const;
	void bcastPopulation(size_t serializedSize, Utils::Serialize::buffer_t &buff) const;
	void bcastAdjMatrixPopulation(Eigen::MatrixXf &distMatrix) const;

	void recvIndivMigration(Utils::Serialize::buffer_t &buff) const;
	void sendIndivMigration(Utils::Serialize::buffer_t &buff) const;
	void gatherIndivMigration(Utils::Serialize::buffer_t &buff, Utils::Serialize::buffer_t &result) const;
	void bcastIndivMigration(size_t serializedSize, Utils::Serialize::buffer_t &buff) const;

	int getMyRankParallelPop() const;
	int getNProcParallelPop() const;

	int getMyRankMigration() const;
	int getNProcMigation() const;

	bool checkRolePopulation(role_t aRole) const;
	bool checkRoleMigration(role_t aRole) const;

	bool isSequentialPopulation() const;
	bool isSequentialMigration() const;

	bool isManagerPopulation() const;
	bool isManagerMigration() const;

private:

	static const size_t MAX_PROC_PER_POP;

	enum tag_t {PARALLEL_AG_TAG=0, DEFAULT_TAG=100};
	//enum layer_id_t {PARALLEL_POPULATION_LAYER_ID=0, MIGRATION_LAYER_ID=1};
	typedef size_t layer_id_t;

	size_t PARALLEL_POPULATION_LAYER_ID, MIGRATION_LAYER_ID;
	role_t roleParallelPopulation, roleMigration;

	void init(size_t layer, BaseTopology::sharedPtr_t basePtrBT);
	void deinit();

	void defineRoles();

	void recvIndividuals(layer_id_t layer, Utils::Serialize::buffer_t &buff) const;
	void sendIndividuals(layer_id_t layer, Utils::Serialize::buffer_t &buff) const;
	void gatherIndividuals(layer_id_t layer, Utils::Serialize::buffer_t &buff, Utils::Serialize::buffer_t &result) const;
	void bcastIndividual(layer_id_t layer, size_t serializedSize, Utils::Serialize::buffer_t &buff) const;

private:

	// Defined in the body
	AGManager();
	~AGManager();
	// Not defined to avoid call
	AGManager(const AGManager&);
	AGManager& operator=(const AGManager&);

	friend AGManager& agMgr();
	friend class MCMCManager;
	friend class MLManager;
};

inline AGManager& agMgr() {
    static AGManager instance;
    return instance;
}


} /* namespace Parallel */

#endif /* AGMANAGER_H_ */
