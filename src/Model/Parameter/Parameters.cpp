//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Parameters.cpp
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#include "Parameters.h"

#include <assert.h>
#include <sys/types.h>

#include "Model/Parameter/ParamDblDef.h"
#include "Model/Parameter/ParamIntDef.h"
#include "Model/Prior/NoPrior.h"
#include "Model/Prior/PriorInterface.h"

namespace StatisticalModel {

Utils::IDManagement::IDManager Parameters::idManager;

Parameters::Parameters() {

}

Parameters::~Parameters() {
}

void Parameters::addBaseParameter(ParamIntDef aParam){
	intParams.push_back(aParam);
	if(!dynamic_cast<NoPrior*>(aParam.getPrior())){
		intPrior.push_back(make_pair(intParams.size()-1, aParam.getPrior()));
	}
	idManager.allocateId();
}

void Parameters::addBaseParameter(ParamDblDef aParam){
	dblParams.push_back(aParam);
	if(!dynamic_cast<NoPrior*>(aParam.getPrior())){
		dblPrior.push_back(make_pair(dblParams.size()-1, aParam.getPrior()));
	}
	idManager.allocateId();
}

size_t Parameters::addDynamicParameter(ParamIntDef aParam) {
	size_t iParam = idManager.allocateId();

	registerDynamicParameter(iParam, aParam);

	return iParam;
}

size_t Parameters::addDynamicParameter(ParamDblDef aParam) {
	size_t iParam = idManager.allocateId();

	registerDynamicParameter(iParam, aParam);

	return iParam;
}

void Parameters::restoreDynamicParameter(size_t iParam, ParamIntDef aParam) {
	bool wasFree = idManager.markAsUsed(iParam);
	assert(wasFree);
	registerDynamicParameter(iParam, aParam);
}

void Parameters::restoreDynamicParameter(size_t iParam, ParamDblDef aParam) {
	bool wasFree = idManager.markAsUsed(iParam);
	assert(wasFree);
	registerDynamicParameter(iParam, aParam);
}

void Parameters::registerDynamicParameter(size_t iParam, ParamIntDef &aParam) {
	ParameterLocator pLoc;
	pLoc.listType = INT_LIST;
	pLoc.itIList = intDynamicParams.insert(intDynamicParams.end(), aParam);
	mapDynamicParams.insert(std::make_pair(iParam, pLoc));

	if(aParam.getPrior() != NULL) {
		dynamicPrior.push_back(std::make_pair(iParam, aParam.getPrior()));
	}
}

void Parameters::registerDynamicParameter(size_t iParam, ParamDblDef &aParam) {

	ParameterLocator pLoc;
	pLoc.listType = DBL_LIST;
	pLoc.itDList = dblDynamicParams.insert(dblDynamicParams.end(), aParam);
	mapDynamicParams.insert(std::make_pair(iParam, pLoc));

	if(aParam.getPrior() != NULL) {
		dynamicPrior.push_back(std::make_pair(iParam, aParam.getPrior()));
	}
}

void Parameters::removeDynamicParameter(size_t aInd) {
	// Get the position
	std::map<size_t, ParameterLocator>::iterator itFind = mapDynamicParams.find(aInd);
	assert(itFind != mapDynamicParams.end());

	// Get the prior
	/*PriorInterface* aPrior;
	if(itFind->second.listType == INT_LIST) {
		aPrior = itFind->second.itIList->getPrior();
	} else {
		aPrior = itFind->second.itDList->getPrior();
	}*/

	// remove the prior if there is one
	listPriorIt_t itP = dynamicPrior.begin();
	while(itP != dynamicPrior.end()) {
		if(itP->first == aInd) {
			itP = dynamicPrior.erase(itP);
			break;
		} else {
			itP++;
		}
	}

	// remove the parameter
	if(itFind->second.listType == INT_LIST) {
		intDynamicParams.erase(itFind->second.itIList);
	} else {
		dblDynamicParams.erase(itFind->second.itDList);
	}

	// Erase the mapping
	mapDynamicParams.erase(itFind);

	// Release ID
	idManager.freeId(aInd);
}


string Parameters::getName(size_t aInd) const {
	if(aInd < getNBaseParameters()) {
		size_t ivSize = intParams.size();
		if(aInd < ivSize){
			return intParams[aInd].getName();
		} else {
			return dblParams[aInd-ivSize].getName();
		}
	} else {
		std::map<size_t, ParameterLocator>::const_iterator itFind = mapDynamicParams.find(aInd);
		assert(itFind != mapDynamicParams.end());

		if(itFind->second.listType == INT_LIST) {
			return itFind->second.itIList->getName();
		} else {
			return itFind->second.itDList->getName();
		}
	}
}

std::vector<std::string> Parameters::getNames(bool onlyBaseParameters) const {
	std::vector<string> names;

	for(pIntCstIt_t itI=intParams.begin(); itI!=intParams.end(); ++itI){
		names.push_back(itI->getName());
	}

	for(pDblCstIt_t itD=dblParams.begin(); itD!=dblParams.end(); ++itD){
		names.push_back(itD->getName());
	}

	if(!onlyBaseParameters) {
		for(std::list<ParamIntDef>::const_iterator itI=intDynamicParams.begin(); itI!=intDynamicParams.end(); ++itI){
			names.push_back(itI->getName());
		}

		for(std::list<ParamDblDef>::const_iterator itD=dblDynamicParams.begin(); itD!=dblDynamicParams.end(); ++itD){
			names.push_back(itD->getName());
		}
	}

	return names;
}

Sampler::Sample Parameters::generateBaseRandomSample() const {
	Sampler::Sample sample;

	for(pIntCstIt_t itI=intParams.begin(); itI!=intParams.end(); ++itI){
		sample.getIntValues().push_back(itI->generateRandom());
	}

	for(pDblCstIt_t itD=dblParams.begin(); itD!=dblParams.end(); ++itD){
		sample.getDblValues().push_back(itD->generateRandom());
	}

	return sample;
}

double Parameters::processPriorValue(const Sampler::Sample &sample) const {
	// Check bounds for base parameters
	for(uint i=0; i<intParams.size(); ++i){
		long int val = sample.getIntParameter(i);
		if(!intParams[i].isInBound(val)) return 0.;
	}

	for(uint i=0; i<dblParams.size(); ++i){
		double val = sample.getDoubleParameter(i + intParams.size());
		if(!dblParams[i].isInBound(val)) return 0.;
	}

	// Compute prior for base parameters
	double val=1.0;
	for(priorCstIt_t itI=intPrior.begin(); itI!=intPrior.end(); ++itI){
		val *= itI->second->computePDF(itI->first, sample);
	}

	for(priorCstIt_t itD=dblPrior.begin(); itD!=dblPrior.end(); ++itD){
		val *= itD->second->computePDF(itD->first+intParams.size(), sample);
	}

	// TODO CHECK that
	// Compute prior for dynamic parameters
	for(listPriorCstIt_t itDyn=dynamicPrior.begin(); itDyn != dynamicPrior.end(); ++itDyn) {
		val *= itDyn->second->computePDF(itDyn->first, sample);
	}
	/*for(size_t iP=0; iP<mssPInd.size(); ++iP) {
		size_t pInd = mssPInd[iP];
		std::map<size_t, ParameterLocator>::iterator itFind = mapDynamicParams.find(pInd);
		assert(itFind != mapDynamicParams.end());

		if(itFind->second.listType == INT_LIST) {
			if(!itFind->second.itIList->getPrior()->computePDF(pInd, sample);
		} else {
			if(!itFind->second.itDList->getPrior()->computePDF(pInd, sample);
		}
	}*/

	return val;
}

double Parameters::processLogPriorValue(const Sampler::Sample &sample) const {
	for(uint i=0; i<intParams.size(); ++i){
		long int val = sample.getIntParameter(i);
		if(!intParams[i].isInBound(val)) {
			return -numeric_limits<double>::infinity();
		}
	}

	for(uint i=0; i<dblParams.size(); ++i){
		double val = sample.getDoubleParameter(i + intParams.size());
		if(!dblParams[i].isInBound(val)) {
			return -numeric_limits<double>::infinity();
		}
	}

	double val=0.0;
	for(priorCstIt_t itI=intPrior.begin(); itI!=intPrior.end(); ++itI){
		val += log(itI->second->computePDF(itI->first, sample));
	}

	for(priorCstIt_t itD=dblPrior.begin(); itD!=dblPrior.end(); ++itD){
		val += log(itD->second->computePDF(itD->first+intParams.size(), sample));
	}

	// TODO CHECK that
	// Compute prior for dynamic parameters
	for(listPriorCstIt_t itDyn=dynamicPrior.begin(); itDyn != dynamicPrior.end(); ++itDyn) {
		/*std::cout << "ID : " << itDyn->first << " :: Name = " << getName(itDyn->first);
		std::cout << " :: logPrior : " << log(itDyn->second->computePDF(itDyn->first, sample)) << std::endl;*/
		val += log(itDyn->second->computePDF(itDyn->first, sample));
	}

	return val;
}

size_t Parameters::getNBaseParameters() const {
	return intParams.size() + dblParams.size();
}

void Parameters::signalReflections(size_t aInd, int cnt) const {
	if(aInd < getNBaseParameters()) {
		size_t ivSize = intParams.size();
		if(aInd < ivSize){
			return intParams[aInd].signalReflections(cnt);
		} else {
			return dblParams[aInd-ivSize].signalReflections(cnt);
		}
	} else {
		std::map<size_t, ParameterLocator>::const_iterator itFind = mapDynamicParams.find(aInd);
		assert(itFind != mapDynamicParams.end());

		if(itFind->second.listType == INT_LIST) {
			itFind->second.itIList->signalReflections(cnt);
		} else {
			itFind->second.itDList->signalReflections(cnt);
		}
	}
}

bool Parameters::isOverflowed(size_t aInd) const {
	if(aInd < getNBaseParameters()) {
		size_t ivSize = intParams.size();
		if(aInd < ivSize){
			return intParams[aInd].isOverflowed();
		} else {
			return dblParams[aInd-ivSize].isOverflowed();
		}
	} else {
		std::map<size_t, ParameterLocator>::const_iterator itFind = mapDynamicParams.find(aInd);
		assert(itFind != mapDynamicParams.end());

		if(itFind->second.listType == INT_LIST) {
			return itFind->second.itIList->isOverflowed();
		} else {
			return itFind->second.itDList->isOverflowed();
		}
	}
}

void Parameters::disableReflection(size_t aInd) {
	if(aInd < getNBaseParameters()) {
		size_t ivSize = intParams.size();
		if(aInd < ivSize){
			intParams[aInd].disableReflection();
		} else {
			dblParams[aInd-ivSize].disableReflection();
		}
	} else {
		std::map<size_t, ParameterLocator>::const_iterator itFind = mapDynamicParams.find(aInd);
		assert(itFind != mapDynamicParams.end());

		if(itFind->second.listType == INT_LIST) {
			itFind->second.itIList->disableReflection();
		} else {
			itFind->second.itDList->disableReflection();
		}
	}
}

double Parameters::getParameterFreq(size_t aInd) const {
	double freq = 0.;
	if(aInd < getNBaseParameters()) {
		size_t ivSize = intParams.size();
		if(aInd < ivSize){
			freq = intParams[aInd].getFreq();
		} else {
			freq = dblParams[aInd-ivSize].getFreq();
		}
	} else {
		std::map<size_t, ParameterLocator>::const_iterator itFind = mapDynamicParams.find(aInd);
		assert(itFind != mapDynamicParams.end());

		if(itFind->second.listType == INT_LIST) {
			freq = itFind->second.itIList->getFreq();
		} else {
			freq = itFind->second.itDList->getFreq();
		}
	}
	return freq;
}

parameterType_t Parameters::getType(size_t aInd) const {

	parameterType_t cat;
	if(aInd < getNBaseParameters()) {
		size_t ivSize = intParams.size();
		if(aInd < ivSize){
			cat = intParams[aInd].getType();
		} else {
			cat = dblParams[aInd-ivSize].getType();
		}
	} else {
		std::map<size_t, ParameterLocator>::const_iterator itFind = mapDynamicParams.find(aInd);
		assert(itFind != mapDynamicParams.end());

		if(itFind->second.listType == INT_LIST) {
			cat = itFind->second.itIList->getType();
		} else {
			cat = itFind->second.itDList->getType();
		}
	}
	return cat;
}

std::vector<size_t> Parameters::findBaseParametersOfType(parameterType_t pType) const {

	std::vector<size_t> paramsIdx;
	for(size_t iP=0; iP<getNBaseParameters(); ++iP) {
		if(getType(iP) == pType) {
			paramsIdx.push_back(iP);
		}
	}
	return paramsIdx;
}

bool Parameters::isInt(size_t aInd) const {
	if(aInd < getNBaseParameters()) {
		size_t ivSize = intParams.size();
		return (aInd < ivSize);
	} else {
		std::map<size_t, ParameterLocator>::const_iterator itFind = mapDynamicParams.find(aInd);
		assert(itFind != mapDynamicParams.end());
		return itFind->second.listType == INT_LIST;
	}
}

bool Parameters::isDbl(size_t aInd) const {
	return !isInt(aInd);
}

bool Parameters::isInBound(size_t aInd, const double value) const {
	if(aInd < getNBaseParameters()) {
		size_t ivSize = intParams.size();
		if(aInd < ivSize){
			return intParams[aInd].isInBound(value);
		} else {
			return dblParams[aInd-ivSize].isInBound(value);
		}
	} else {
		std::map<size_t, ParameterLocator>::const_iterator itFind = mapDynamicParams.find(aInd);
		assert(itFind != mapDynamicParams.end());

		if(itFind->second.listType == INT_LIST) {
			return itFind->second.itIList->isInBound(value);
		} else {
			return itFind->second.itDList->isInBound(value);
		}
	}
}

bool Parameters::requireReflection(size_t aInd) const {
	if(aInd < getNBaseParameters()) {
		size_t ivSize = intParams.size();
		if(aInd < ivSize){
			return intParams[aInd].requireReflection();
		} else {
			return dblParams[aInd-ivSize].requireReflection();
		}
	} else {
		std::map<size_t, ParameterLocator>::const_iterator itFind = mapDynamicParams.find(aInd);
		assert(itFind != mapDynamicParams.end());

		if(itFind->second.listType == INT_LIST) {
			return itFind->second.itIList->requireReflection();
		} else {
			return itFind->second.itDList->requireReflection();
		}
	}
}

double Parameters::getBoundMin(size_t aInd) const {
	if(aInd < getNBaseParameters()) {
		size_t ivSize = intParams.size();
		if(aInd < ivSize){
			return intParams[aInd].getBoundMin();
		} else {
			return dblParams[aInd-ivSize].getBoundMin();
		}
	} else {
		std::map<size_t, ParameterLocator>::const_iterator itFind = mapDynamicParams.find(aInd);
		assert(itFind != mapDynamicParams.end());

		if(itFind->second.listType == INT_LIST) {
			return itFind->second.itIList->getBoundMin();
		} else {
			return itFind->second.itDList->getBoundMin();
		}
	}
}

double Parameters::getBoundMax(size_t aInd) const {
	if(aInd < getNBaseParameters()) {
		size_t ivSize = intParams.size();
		if(aInd < ivSize){
			return intParams[aInd].getBoundMax();
		} else {
			return dblParams[aInd-ivSize].getBoundMax();
		}
	} else {
		std::map<size_t, ParameterLocator>::const_iterator itFind = mapDynamicParams.find(aInd);
		assert(itFind != mapDynamicParams.end());

		if(itFind->second.listType == INT_LIST) {
			return itFind->second.itIList->getBoundMax();
		} else {
			return itFind->second.itDList->getBoundMax();
		}
	}
}

void Parameters::setBoundMin(size_t aInd, const double value) {
	if(aInd < getNBaseParameters()) {
		size_t ivSize = intParams.size();
		if(aInd < ivSize){
			return intParams[aInd].setMin(value);
		} else {
			return dblParams[aInd-ivSize].setMin(value);
		}
	} else {
		std::map<size_t, ParameterLocator>::iterator itFind = mapDynamicParams.find(aInd);
		assert(itFind != mapDynamicParams.end());

		if(itFind->second.listType == INT_LIST) {
			return itFind->second.itIList->setMin(value);
		} else {
			return itFind->second.itDList->setMin(value);
		}
	}
}

void Parameters::setBoundMax(size_t aInd, const double value) {
	if(aInd < getNBaseParameters()) {
		size_t ivSize = intParams.size();
		if(aInd < ivSize){
			intParams[aInd].setMax(value);
		} else {
			dblParams[aInd-ivSize].setMax(value);
		}
	} else {
		std::map<size_t, ParameterLocator>::iterator itFind = mapDynamicParams.find(aInd);
		assert(itFind != mapDynamicParams.end());

		if(itFind->second.listType == INT_LIST) {
			return itFind->second.itIList->setMax(value);
		} else {
			return itFind->second.itDList->setMax(value);
		}
	}
}

} // namespace StatisticalModel

