//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DiscreteUniform.h
 *
 * @date Apr 26, 2017
 * @author meyerx
 * @brief
 */
#ifndef DISCRETEUNIFORM_H_
#define DISCRETEUNIFORM_H_

#include <stddef.h>

#include "PriorInterface.h"

class RNG;
namespace Sampler { class Sample; }

namespace StatisticalModel {

class DiscreteUniform: public PriorInterface {
public:
	DiscreteUniform(const RNG *aRNG, long int aStart, long int aEnd);
	~DiscreteUniform();

	double sample() const;
	double computePDF(const size_t iP, const Sampler::Sample &sample) const;

private:
	const RNG *r;
	const long int start, end;
	const size_t nCateg;
	const double pmf;
};

} /* namespace StatisticalModel */

#endif /* DISCRETEUNIFORM_H_ */
