//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BranchSiteRELHlp.cpp
 *
 * @date May 15, 2015
 * @author meyerx
 * @brief
 */
#include "BranchSiteRELHlp.h"

#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <sys/types.h>

#include "Model/Likelihood/Helper/../BranchSiteREL/BranchSiteRELik.h"
#include "Model/Parameter/ParamDblDef.h"
#include "Model/Parameter/ParameterType.h"
#include "Model/Parameter/Parameters.h"
#include "Parallel/Manager/MpiManager.h"
#include "Parallel/RNG/RNG.h"
#include "Utils/MolecularEvolution/MatrixFactory/MatrixFactory.h"
#include "cmath"

namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

BranchSiteRELHlp::BranchSiteRELHlp(BranchSiteREL::BranchSiteRELik *ptrREL) : ptrLik(ptrREL) {
}

BranchSiteRELHlp::~BranchSiteRELHlp() {
}

void BranchSiteRELHlp::defineParameters(Parameters &params) const {

	const bool reflect = false;
	const double epsilon = 5e-4;//1e-9;

	if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::GTR) {
		for(size_t iT=0; iT<5; iT++) {
			std::stringstream ssName;
			ssName << "Theta_" << iT;
			ParamDblDef pTheta(ssName.str(), PriorInterface::createNoPrior(), reflect);
			pTheta.setMin(0.+epsilon);
			pTheta.setMax(40.);
			pTheta.setType(StatisticalModel::RATE_PARAM);
			params.addBaseParameter(pTheta);
		}
	} else if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::K80) {
		ParamDblDef pKappa("Kappa", PriorInterface::createNoPrior(), reflect);
		pKappa.setMin(0+epsilon);
		pKappa.setMax(40.);
		pKappa.setType(StatisticalModel::RATE_PARAM);
		params.addBaseParameter(pKappa);
	} else {
		std::cerr << "Error : void BranchSiteRELHlp::defineParameters(Parameters &params) const;" << std::endl;
		std::cerr << "Nucleotide model not supported by BrachSiteRELHlp"<< std::endl;
		abort();
	}

	for(size_t iB=0; iB<ptrLik->getNBranch(); iB++) {
		std::stringstream ss;
		ss << "B" << iB << ".";
		std::string baseName = ss.str();

		ss.str(""); ss << baseName << "t";
		ParamDblDef pBL(ss.str(), PriorInterface::createNoPrior(), reflect);
		pBL.setMin(0.+epsilon);
		pBL.setMax(20.);
		pBL.setType(StatisticalModel::BRANCH_LENGTH);
		params.addBaseParameter(pBL);

		ss.str(""); ss << baseName << "wNeg";
		ParamDblDef pWNeg(ss.str(), PriorInterface::createNoPrior(), reflect);
		pWNeg.setMin(0.+epsilon);
		pWNeg.setMax(1.);
		pWNeg.setType(StatisticalModel::RATE_PARAM);
		params.addBaseParameter(pWNeg);

		ss.str(""); ss << baseName << "wNeut";
		ParamDblDef pWNeutral(ss.str(), PriorInterface::createNoPrior(), reflect);
		pWNeutral.setMin(0.+epsilon);
		pWNeutral.setMax(1.);
		pWNeutral.setType(StatisticalModel::RATE_PARAM);
		params.addBaseParameter(pWNeutral);

		ss.str(""); ss << baseName << "wPos";
		ParamDblDef pWPos(ss.str(), PriorInterface::createNoPrior(), reflect);
		if(ptrLik->getFGBranch() == (int)iB) {
			pWPos.setMin(1.);
			pWPos.setMax(1.);
		} else {
			pWPos.setMin(1.);
			pWPos.setMax(100.);
		}
		pWPos.setType(StatisticalModel::RATE_PARAM);
		params.addBaseParameter(pWPos);

		ss.str(""); ss << baseName << "P0";
		ParamDblDef pP0(ss.str(), PriorInterface::createNoPrior(), reflect);
		pP0.setMin(0.+epsilon);
		pP0.setMax(1.-epsilon);
		pP0.setType(StatisticalModel::RATE_PARAM);
		params.addBaseParameter(pP0);

		ss.str(""); ss << baseName << "P1";
		ParamDblDef pP1(ss.str(), PriorInterface::createNoPrior(), reflect);
		pP1.setMin(0.+epsilon);
		pP1.setMax(1.-epsilon);
		pP1.setType(StatisticalModel::RATE_PARAM);
		params.addBaseParameter(pP1);
	}
}

Sample BranchSiteRELHlp::defineInitSample(const Parameters &params) const {
	//const double epsilon = 1e-9;

	size_t nPPerBranch;
	nPPerBranch = 6;

	//Sampler::Sample sample = params.generateRandomSample();
	Sampler::Sample sample;

	const RNG* rng = Parallel::mpiMgr().getPRNG();

	size_t nSubParams;
	if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::GTR) {
		nSubParams = 5;
		for(size_t iT=0; iT<nSubParams; ++iT) {
			sample.getDblValues().push_back(0.5+rng->genUniformDbl());		// [0->4] Rand Theta {1..5}
		}
	} else if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::K80) {
		nSubParams = 1;
		sample.getDblValues().push_back(0.5+rng->genUniformDbl());		// [0] Rand Kappa
	} else {
		std::cerr << "Error : void BranchSiteRELHlp::defineInitSample(const Parameters &params) const;" << std::endl;
		std::cerr << "Nucleotide model not supported by BrachSiteRELHlp"<< std::endl;
		abort();
	}


	for(size_t iB=0; iB<((params.getNBaseParameters()-nSubParams)/nPPerBranch); ++iB) {
		double x0 =  exp(0.5 + rng->genUniformDbl());
		double x1 =  exp(0.5 + rng->genUniformDbl());
		double tot = x0 + x1 + 1.0;
		double p0 = x0/tot;
		double p1 = x1/tot;
		if(p0+p1 < 1e-15) {p0 = 1e-6; p1 = 1e-6;}


		size_t cnt = 0;
		size_t offset = 5+iB*nPPerBranch;

		sample.getDblValues().push_back(0.1 + 0.5 * rng->genUniformDbl()); ++cnt; 	// t = 0.1 + 0.5*rnd(0.,1.)

		sample.getDblValues().push_back(rng->genUniformDbl()); ++cnt; 				// wNeg = rnd(0.,1.)
		sample.getDblValues().push_back(rng->genUniformDbl()); ++cnt; 				// wNeut  = rnd(0.,1.)
		if(params.getBoundMin(offset+cnt) == params.getBoundMax(offset+cnt)) {
			sample.getDblValues().push_back(params.getBoundMin(offset+cnt)); ++cnt;	// wPos = 1.0
		} else {
			sample.getDblValues().push_back(1.0+0.5*rng->genUniformDbl()); ++cnt;		// wPos > 1.0 = 1+0.5*rnd(0., 1.)
		}

		sample.getDblValues().push_back(p0+p1); ++cnt; 								// wP0
		sample.getDblValues().push_back(p0/(p0+p1)); ++cnt; 							// wP1
	}

	for(size_t i=0; i < params.getNBaseParameters(); ++i)
	{
		if(sample.getDblValues()[i] < params.getBoundMin(i) * 1.05) sample.getDblValues()[i] = params.getBoundMin(i) * 1.05;
		if(sample.getDblValues()[i] > params.getBoundMax(i) / 1.05) sample.getDblValues()[i] = params.getBoundMax(i) / 1.05;
	}

	for(size_t i=0; i < params.getNBaseParameters(); ++i)
	{
		if(sample.getDblValues()[i] < params.getBoundMin(i)) sample.getDblValues()[i] = params.getBoundMin(i) * 1.2;
		if(sample.getDblValues()[i] > params.getBoundMax(i)) sample.getDblValues()[i] = params.getBoundMax(i) * 0.8;
		if(params.getBoundMin(i) == params.getBoundMax(i)) sample.getDblValues()[i] = params.getBoundMin(i);
	}

	//printSample(params, sample);

	return sample;
}


void BranchSiteRELHlp::printSample(const Parameters &params, const Sample &sample, const char sep) const {

	const std::vector<double> &values = sample.getDblValues();

	size_t nSubParams;
	if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::GTR) {
		nSubParams = 5;
		std::cout << "Theta :";
		for(size_t iT=0; iT<5; ++iT) {
			 std::cout << std::scientific << std::setprecision(3) << "\t" << sample.getDblValues()[iT];
		}
		std::cout << std::endl;
	} else if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::K80) {
		nSubParams = 1;
		std::cout << "Kappa :\t" << std::scientific << std::setprecision(3) << sample.getDblValues()[0] << std::endl;
	} else {
		std::cerr << "Error : void BranchSiteRELHlp::printSample(const Parameters &params, const Sample &sample, const char sep) const;" << std::endl;
		std::cerr << "Nucleotide model not supported by BrachSiteRELHlp"<< std::endl;
		abort();
	}

	for(size_t iB=0; iB< ptrLik->getNBranch(); ++iB){
		size_t cnt = 0;
		size_t offset = nSubParams+iB*6;
		double t = values[offset+cnt]; ++cnt;
		double wNeg = values[offset+cnt]; ++cnt;
		double wNeut = values[offset+cnt]; ++cnt;
		double wPos = values[offset+cnt]; ++cnt;
		double p0 = values[offset+cnt]; ++cnt;
		double p1 = values[offset+cnt]; ++cnt;
		double pNeg = p0;
		double pNeut = p1*(1-p0);
		double pPos = (1.-p0)*(1.-p1);

		std::cout << "Branch [" << std::setw(2) << iB << "] : t=";
		std::cout << std::scientific << std::setprecision(3) << t << "\tpNeg=" << pNeg << "\tpNeut=" << pNeut << "\tpPos=" << pPos;
		std::cout << "\twNeg=" << wNeg << "\twNeut=" << wNeut << "\twPos=" << wPos << std::endl;
	}
}

std::string BranchSiteRELHlp::sampleToString(const Parameters &params, const Sample &sample, const char sep) const {

	std::stringstream ss;

	const std::vector<double> &values = sample.getDblValues();

	size_t nSubParams;
	if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::GTR) {
		nSubParams = 5;
		ss << "Theta :";
		for(size_t iT=0; iT<5; ++iT) {
			 ss << std::scientific << std::setprecision(3) << "\t" << sample.getDblValues()[iT];
		}
		ss << std::endl;
	} else if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::K80) {
		nSubParams = 1;
		ss << "Kappa :\t" << std::scientific << std::setprecision(3) << sample.getDblValues()[0] << std::endl;
	} else {
		std::cerr << "Error : void BranchSiteRELHlp::printSample(const Parameters &params, const Sample &sample, const char sep) const;" << std::endl;
		std::cerr << "Nucleotide model not supported by BrachSiteRELHlp"<< std::endl;
		abort();
	}

	for(size_t iB=0; iB< ptrLik->getNBranch(); ++iB){
		size_t cnt = 0;
		size_t offset = nSubParams+iB*6;
		double t = values[offset+cnt]; ++cnt;
		double wNeg = values[offset+cnt]; ++cnt;
		double wNeut = values[offset+cnt]; ++cnt;
		double wPos = values[offset+cnt]; ++cnt;
		double p0 = values[offset+cnt]; ++cnt;
		double p1 = values[offset+cnt]; ++cnt;
		double pNeg = p0;
		double pNeut = p1*(1-p0);
		double pPos = (1.-p0)*(1.-p1);

		ss << "Branch [" << std::setw(2) << iB << "] : t=";
		ss << std::scientific << std::setprecision(3) << t << "\tpNeg=" << pNeg << "\tpNeut=" << pNeut << "\tpPos=" << pPos;
		ss << "\twNeg=" << wNeg << "\twNeut=" << wNeut << "\twPos=" << wPos << std::endl;
	}

	return ss.str();
}


} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
