//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * HelperInterface.cpp
 *
 *  Created on: May 12, 2015
 *      Author: meyerx
 */

#include "HelperInterface.h"

#include <assert.h>
#include <stdlib.h>

#include "Helpers.h"

namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

HelperInterface::HelperInterface() {
}

HelperInterface::~HelperInterface() {
}

HelperInterface::sharedPtr_t HelperInterface::createHelper(LikelihoodInterface *likPtr) {
	 if(PositiveSelection::Base::SelPositive *ptr = dynamic_cast<PositiveSelection::Base::SelPositive*>(likPtr)) {
		return sharedPtr_t(new PositiveSelectionHlp<PositiveSelection::Base::SelPositive>(ptr));
	} else if(PositiveSelection::Light::SelPositive *ptr = dynamic_cast<PositiveSelection::Light::SelPositive*>(likPtr)) {
			return sharedPtr_t(new PositiveSelectionHlp<PositiveSelection::Light::SelPositive>(ptr));
	} else if(BranchSiteREL::BranchSiteRELik *ptr = dynamic_cast<BranchSiteREL::BranchSiteRELik*>(likPtr)) {
			return sharedPtr_t(new BranchSiteRELHlp(ptr));
	} else if(TreeInference::Base *ptr = dynamic_cast<TreeInference::Base *>(likPtr)) {
			return sharedPtr_t(new TreeInferenceHlp(ptr));
	} else {
		std::cerr << "Helper::sharedPtr_t Helper::createHelper(LikelihoodInterface *likPtr);" << std::endl;
		std::cerr << "No Likelihood::Helper available for likelihood '" << likPtr->getName() << "'" << std::endl;
		abort();
	}
	return sharedPtr_t();
}

void HelperInterface::printSample(const Parameters &params, const Sample &sample, const char sep) const {

	if(sep == '\n') {
		for(size_t iP=0; iP<params.getNBaseParameters(); ++iP) {
			std::cout << params.getName(iP) << " =\t";
			std::cout << std::scientific << std::setprecision(3) << sample.getDblValues()[iP] << std::endl;
		}
	} else {
		// Print params names
		for(size_t iP=0; iP<params.getNBaseParameters(); ++iP) {
			std::cout << params.getName(iP) << sep;
		}
		std::cout << std::endl;
		// Print sample values
		for(size_t iP=0; iP<params.getNBaseParameters(); ++iP) {
			std::cout << std::scientific << std::setprecision(3) << sample.getDblValues()[iP] << sep;
		}
		std::cout << std::endl;
	}
}

std::string HelperInterface::sampleToString(const Parameters &params, const Sample &sample, const char sep) const {
	std::stringstream ss;
	if(sep == '\n') {
		for(size_t iP=0; iP<params.getNBaseParameters(); ++iP) {
			ss << params.getName(iP) << " =\t";
			ss << std::scientific << std::setprecision(3) << sample.getDblValues()[iP] << std::endl;
		}
	} else {
		// Print params names
		for(size_t iP=0; iP<params.getNBaseParameters(); ++iP) {
			ss << params.getName(iP) << sep;
		}
		ss << std::endl;
		// Print sample values
		for(size_t iP=0; iP<params.getNBaseParameters(); ++iP) {
			ss << std::scientific << std::setprecision(3) << sample.getDblValues()[iP] << sep;
		}
		ss << std::endl;
	}
	return ss.str();
}


} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
