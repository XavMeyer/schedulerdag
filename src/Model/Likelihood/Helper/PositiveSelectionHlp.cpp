//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PositiveSelectionHlp.cpp
 *
 * @date May 15, 2015
 * @author meyerx
 * @brief
 */
#include "PositiveSelectionHlp.h"

#include "Model/Parameter/ParamDblDef.h"
#include "Model/Parameter/ParameterType.h"
#include "Model/Parameter/Parameters.h"
#include "Model/Prior/PriorInterface.h"

namespace StatisticalModel { namespace Likelihood { namespace PositiveSelection { namespace Light { class SelPositive; } } } }

namespace StatisticalModel {
namespace Likelihood {
namespace Helper {

template<typename SelPositive>
PositiveSelectionHlp<SelPositive>::PositiveSelectionHlp(SelPositive *ptrSP) : ptrLik(ptrSP) {

}

template<typename SelPositive>
PositiveSelectionHlp<SelPositive>::~PositiveSelectionHlp() {
}


template<typename SelPositive>
void PositiveSelectionHlp<SelPositive>::defineParameters(Parameters &params) const {

	const bool reflect = false;
	const double epsilon = 1e-5;

	/*ParamDblDef pKappa("Kappa", PriorInterface::createNoPrior(), reflect);
	pKappa.setMin(0);
	pKappa.setMax(50.);
	pKappa.setType(StatisticalModel::RATE_PARAM);
	params.addParameter(pKappa);*/

	defineNucleotideSubstParameters(params, reflect);

	ParamDblDef pP0("P0", PriorInterface::createNoPrior(), reflect);
	pP0.setMin(0+epsilon);
	pP0.setMax(1.-epsilon);
	pP0.setType(StatisticalModel::RATE_PARAM);
	params.addBaseParameter(pP0);

	ParamDblDef pP1("P1", PriorInterface::createNoPrior(), reflect);
	pP1.setMin(0+epsilon);
	pP1.setMax(1.-epsilon);
	pP1.setType(StatisticalModel::RATE_PARAM);
	params.addBaseParameter(pP1);

	//ParamDblDef pW0("W0", PriorInterface::createBetaBoost(Parallel::mpiMgr().getPRNG(), 1.4, 1.4), reflect);
	ParamDblDef pW0("W0", PriorInterface::createNoPrior(), reflect);
	pW0.setMin(0.+epsilon);
	pW0.setMax(1.-epsilon);
	pW0.setType(StatisticalModel::RATE_PARAM);
	params.addBaseParameter(pW0);

	//ParamDblDef pW2("W2", PriorInterface::createGammaBoost(Parallel::mpiMgr().getPRNG(), 1.5, 1./0.005), reflect);
	double minW2, maxW2;
	PriorInterface::sharedPtr_t priorW2;
	if(ptrLik->isH1()) {
		minW2 = 1.+epsilon;
		maxW2 = 1.*1.e2;
		priorW2 = PriorInterface::createNoPrior();
	} else {
		minW2 = 1.;
		maxW2 = 1.;
		priorW2 = PriorInterface::createNoPrior();
	}
	ParamDblDef pW2("W2", priorW2, reflect);
	pW2.setMin(minW2);
	pW2.setMax(maxW2);
	pW2.setType(StatisticalModel::RATE_PARAM);
	params.addBaseParameter(pW2);

	for(size_t iB=0; iB<ptrLik->getNBranch(); ++iB) {
		std::stringstream ssBranch;
		ssBranch << "BL_" << iB;
		//ParamDblDef pBL(ssBranch.str(),PriorInterface::createGammaBoost(Parallel::mpiMgr().getPRNG(), 1., 1./0.005), reflect);
		ParamDblDef pBL(ssBranch.str(), PriorInterface::createNoPrior(), reflect);
		pBL.setMin(0.+epsilon);
		pBL.setMax(20.);
		pBL.setType(StatisticalModel::BRANCH_LENGTH);
		params.addBaseParameter(pBL);
	}

	/*for(size_t i=0; i<params.size(); ++i) {
		cout << "[" << i << "] :: " << params.getName(i) << endl;
	}*/
}

template<typename SelPositive>
void PositiveSelectionHlp<SelPositive>::defineNucleotideSubstParameters(Parameters &params, const bool reflect) const {
	ParamDblDef pKappa("Kappa", PriorInterface::createNoPrior(), reflect);
	pKappa.setMin(1.e-5);
	pKappa.setMax(20.);
	pKappa.setType(StatisticalModel::RATE_PARAM);
	params.addBaseParameter(pKappa);
}

template <>
void PositiveSelectionHlp<StatisticalModel::Likelihood::PositiveSelection::Light::SelPositive>::defineNucleotideSubstParameters(Parameters &params, const bool reflect) const {

	if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::GTR) {
		for(size_t iT=0; iT<5; iT++) {
			std::stringstream ssName;
			ssName << "Theta_" << iT;
			ParamDblDef pTheta(ssName.str(), PriorInterface::createNoPrior(), reflect);
			pTheta.setMin(1.e-5);
			pTheta.setMax(40.);
			pTheta.setType(StatisticalModel::RATE_PARAM);
			params.addBaseParameter(pTheta);
		}
	} else if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::K80) {
		ParamDblDef pKappa("Kappa", PriorInterface::createNoPrior(), reflect);
		pKappa.setMin(1.e-5);
		pKappa.setMax(20);
		pKappa.setType(StatisticalModel::RATE_PARAM);
		params.addBaseParameter(pKappa);
	} else {
		std::cerr << "Error : void PositiveSelectionHlp<>::defineParameters(Parameters &params, const bool reflect) const;" << std::endl;
		std::cerr << "Nucleotide model not supported by PositiveSelectionHlp"<< std::endl;
		abort();
	}
}

template<typename SelPositive>
void PositiveSelectionHlp<SelPositive>::defineParametersWide(Parameters &params) const {

	const bool reflect = false;
	const double epsilon = 1e-7;

	//ParamDblDef pKappa("Kappa", PriorInterface::createGammaBoost(Parallel::mpiMgr().getPRNG(), 1.1, 1./0.1), reflect);
	/*ParamDblDef pKappa("Kappa", PriorInterface::createUniformBoost(Parallel::mpiMgr().getPRNG(), 0.+epsilon, 500.), reflect);
	pKappa.setMin(0);
	pKappa.setMax(std::numeric_limits<double>::max());
	params.addParameter(pKappa);*/
	defineNucleotideSubstParametersWide(params, reflect);

	ParamDblDef pP0("P0", PriorInterface::createNoPrior(), reflect);
	pP0.setMin(0);
	pP0.setMax(1.);
	params.addBaseParameter(pP0);

	ParamDblDef pP1("P1", PriorInterface::createNoPrior(), reflect);
	pP1.setMin(0);
	pP1.setMax(1.);
	params.addBaseParameter(pP1);

	ParamDblDef pW0("W0", PriorInterface::createBetaBoost(Parallel::mpiMgr().getPRNG(), 1.4, 1.4), reflect);
	pW0.setMin(0);
	pW0.setMax(1.);
	params.addBaseParameter(pW0);

	//ParamDblDef pW2("W2", PriorInterface::createGammaBoost(Parallel::mpiMgr().getPRNG(), 1.5, 1./0.005), reflect);
	double minW2, maxW2;
	PriorInterface::sharedPtr_t priorW2;
	if(ptrLik->isH1()) {
		minW2 = 1.;
		maxW2 = std::numeric_limits<double>::max();
		priorW2 = PriorInterface::createUniformBoost(Parallel::mpiMgr().getPRNG(), 1.0+epsilon, 1000.);
	} else {
		minW2 = 1.;
		maxW2 = 1.;
		priorW2 = PriorInterface::createNoPrior();
	}
	ParamDblDef pW2("W2", priorW2, reflect);
	pW2.setMin(minW2);
	pW2.setMax(maxW2);
	params.addBaseParameter(pW2);

	for(size_t iB=0; iB<ptrLik->getNBranch(); ++iB) {
		std::stringstream ssBranch;
		ssBranch << "BL_" << iB;
		//ParamDblDef pBL(ssBranch.str(),PriorInterface::createGammaBoost(Parallel::mpiMgr().getPRNG(), 1., 1./0.005), reflect);
		ParamDblDef pBL(ssBranch.str(),PriorInterface::createUniformBoost(Parallel::mpiMgr().getPRNG(), 0.+epsilon, 500.), reflect);
		pBL.setMin(0.+epsilon);
		pBL.setMax(std::numeric_limits<double>::max());
		params.addBaseParameter(pBL);
	}
}

template<typename SelPositive>
void PositiveSelectionHlp<SelPositive>::defineNucleotideSubstParametersWide(Parameters &params, const bool reflect) const {
	ParamDblDef pKappa("Kappa", PriorInterface::createUniformBoost(Parallel::mpiMgr().getPRNG(), 1.e-9, 500.), reflect);
	pKappa.setMin(0);
	pKappa.setMax(std::numeric_limits<double>::max());
	params.addBaseParameter(pKappa);
}

template <>
void PositiveSelectionHlp<StatisticalModel::Likelihood::PositiveSelection::Light::SelPositive>::defineNucleotideSubstParametersWide(Parameters &params, const bool reflect) const {

	if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::GTR) {
		for(size_t iT=0; iT<5; iT++) {
			std::stringstream ssName;
			ssName << "Theta_" << iT;
			ParamDblDef pTheta(ssName.str(), PriorInterface::createUniformBoost(Parallel::mpiMgr().getPRNG(), 1.e-9, 500.), reflect);
			pTheta.setMin(0);
			pTheta.setMax(40.);
			params.addBaseParameter(pTheta);
		}
	} else if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::K80) {
		ParamDblDef pKappa("Kappa", PriorInterface::createUniformBoost(Parallel::mpiMgr().getPRNG(), 1.e-9, 500.), reflect);
		pKappa.setMin(0);
		pKappa.setMax(std::numeric_limits<double>::max());
		params.addBaseParameter(pKappa);
	} else {
		std::cerr << "Error : void PositiveSelectionHlp<>::defineParameters(Parameters &params, const bool reflect) const;" << std::endl;
		std::cerr << "Nucleotide model not supported by PositiveSelectionHlp"<< std::endl;
		abort();
	}
}


template<typename SelPositive>
Sample PositiveSelectionHlp<SelPositive>::defineInitSample(const Parameters &params) const {
	const double epsilon = 1e-4;

	//Sampler::Sample sample = params.generateRandomSample();
	Sampler::Sample sample;

	const RNG* rng = Parallel::mpiMgr().getPRNG();
	sample.getDblValues().clear(); 									// Clear vector

	defineInitNuclSubstSample(rng, sample);
	size_t nNucParam = sample.getDblValues().size();

	double x0 =  exp(0.5 + rng->genUniformDbl());
	double x1 =  exp(0.5 + rng->genUniformDbl());
	double tot = x0 + x1 + 1.0;
	double p0 = x0/tot;
	double p1 = x1/tot;
	if(p0+p1 < 1e-15) {p0 = 1e-6; p1 = 1e-6;}

	sample.getDblValues().push_back(p0+p1);							// [1] Rand P0
	sample.getDblValues().push_back(p0/(p0+p1));						// [2] Rand P1
	sample.getDblValues().push_back(0.5+rng->genUniformDbl());		// [3] Rand W0

	if(params.getBoundMin(4) == params.getBoundMax(4)) {
		sample.getDblValues().push_back(params.getBoundMin(4));		// [4] Add W2 = 1.0
	} else {
		double rand = rng->genUniformDbl();
		sample.getDblValues().push_back(1.0+0.5*rand);				// [4] Rand W2
	}

	for(size_t i=4+nNucParam; i < params.getNBaseParameters(); ++i) {
		double rand = rng->genUniformDbl();
		sample.getDblValues().push_back(0.1 + 0.2 * rand);			// [5->end] Rand T
	}

	for(size_t i=0; i < params.getNBaseParameters(); ++i)
	{
		if(sample.getDblValues()[i] < params.getBoundMin(i) * 1.05) sample.getDblValues()[i] = params.getBoundMin(i) * 1.05;
		if(sample.getDblValues()[i] > params.getBoundMax(i) / 1.05) sample.getDblValues()[i] = params.getBoundMax(i) / 1.05;
	}

	for(size_t i=0; i < params.getNBaseParameters(); ++i)
	{
		if(sample.getDblValues()[i] < params.getBoundMin(i)) sample.getDblValues()[i] = params.getBoundMin(i) * 1.2;
		if(sample.getDblValues()[i] > params.getBoundMax(i)) sample.getDblValues()[i] = params.getBoundMax(i) * 0.8;
		if(params.getBoundMin(i) == params.getBoundMax(i)) sample.getDblValues()[i] = params.getBoundMin(i);
	}


	//sample.getDblValues().push_back(rng->genUniformDbl(0.5, 1.5)); // Kappa between 0.5 and 1.5
	//defineInitNuclSubstSample(rng, sample);
	//sample.getDblValues().push_back(rng->genUniformDbl(0.0+epsilon, 1.-epsilon)); // P0 between 0. and 1.
	//sample.getDblValues().push_back(rng->genUniformDbl(0.0+epsilon, 1.-epsilon)); // P1 between 0. and 1.
	//sample.getDblValues().push_back(rng->genUniformDbl(0.0+epsilon, 1.-epsilon)); // W0 between 0. and 1.
	//double w2 = ptrLik->isH1() ? rng->genUniformDbl(1.0+epsilon, 50.) : 1.0;
	//sample.getDblValues().push_back(w2); // W2 1.0 if H0, between 1 and 10 else

	//for(size_t iBL=0; iBL<ptrLik->getNBranch(); ++iBL){
	//	sample.getDblValues().push_back(rng->genUniformDbl(0.0+epsilon, 1.5)); // Branches between 0.5 and 25.
	//}

	return sample;
}

template<typename SelPositive>
void PositiveSelectionHlp<SelPositive>::defineInitNuclSubstSample(const RNG* rng, Sampler::Sample &sample) const {
	sample.getDblValues().push_back(rng->genUniformDbl(0.5, 1.5)); // Kappa between 0.5 and 1.5
}

template<>
void PositiveSelectionHlp<StatisticalModel::Likelihood::PositiveSelection::Light::SelPositive>::defineInitNuclSubstSample(const RNG* rng, Sampler::Sample &sample) const {
	if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::GTR) {
		for(size_t iT=0; iT<5; iT++) {
			sample.getDblValues().push_back(rng->genUniformDbl(0.5, 1.5)); // Thetas_iT between 0.5 and 1.5
		}
	} else if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::K80) {
		sample.getDblValues().push_back(rng->genUniformDbl(0.5, 1.5)); // Kappa between 0.5 and 1.5
	} else {
		std::cerr << "Error : void PositiveSelectionHlp<>::defineInitNuclSubstSample(const RNG* rng, Sampler::Sample &sample) const;" << std::endl;
		std::cerr << "Nucleotide model not supported by PositiveSelectionHlp"<< std::endl;
		abort();
	}
}

template<typename SelPositive>
void PositiveSelectionHlp<SelPositive>::printSample(const Parameters &params, const Sample &sample, const char sep) const {

	const std::vector<double> &values = sample.getDblValues();

	double p0 = values[1]*values[2];
	double p1 = values[1]*(1.-values[2]);
	double p2a = (1.-values[1])*values[2];
	double p2b = (1.-values[1])*(1.-values[2]);

	std::cout << "Kappa=" << values[0] << "\tP0=" << values[1] << "\tP1=" << values[2] << "\tW0=" << values[3] << "\tW2=" << values[4] << std::endl;
	std::cout << "Prop_0=" << p0 << "\tProp_1=" << p1 << "\tProp_2a=" << p2a << "\tProp_2b=" << p2b << std::endl;
	for(size_t i=0; i<values.size()-5; ++i) {
		std::cout << values[i] << "\t";
	}
	std::cout << endl;
}

template<>
void PositiveSelectionHlp<StatisticalModel::Likelihood::PositiveSelection::Light::SelPositive>::printSample(const Parameters &params, const Sample &sample, const char sep) const {

	const std::vector<double> &values = sample.getDblValues();

	size_t nNuclParams = 0;
	if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::GTR) {
		nNuclParams = 5;
		std::cout << "Thetas :\t";
		for(size_t iT=0; iT<5; iT++) {
			std::cout << values[iT] << "\t";
		}
		std::cout << std::endl;
	} else if(ptrLik->getNucleotideModel() == MolecularEvolution::MatrixUtils::K80) {
		nNuclParams = 1;
		std::cout << "Kappa :\t" << values[0] << std::endl;
	} else {
		std::cerr << "Error : void PositiveSelectionHlp<>::defineInitNuclSubstSample(const RNG* rng, Sampler::Sample &sample) const;" << std::endl;
		std::cerr << "Nucleotide model not supported by PositiveSelectionHlp"<< std::endl;
		abort();
	}

	double p0 = values[nNuclParams]*values[nNuclParams+1];
	double p1 = values[nNuclParams]*(1.-values[nNuclParams+1]);
	double p2a = (1.-values[nNuclParams])*values[nNuclParams+1];
	double p2b = (1.-values[nNuclParams])*(1.-values[nNuclParams+1]);

	std::cout << "P0=" << values[nNuclParams] << "\tP1=" << values[nNuclParams+1] << "\tW0=" << values[nNuclParams+2] << "\tW2=" << values[nNuclParams+3] << std::endl;
	std::cout << "Prop_0=" << p0 << "\tProp_1=" << p1 << "\tProp_2a=" << p2a << "\tProp_2b=" << p2b << std::endl;
	std::cout << "Branch lenghts :\t";
	for(size_t i=nNuclParams+4; i<values.size(); ++i) {
		std::cout << values[i] << "\t";
	}
	std::cout << endl;
}



template class PositiveSelectionHlp<StatisticalModel::Likelihood::PositiveSelection::Base::SelPositive>;
template class PositiveSelectionHlp<StatisticalModel::Likelihood::PositiveSelection::Light::SelPositive>;

} /* namespace Helper */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
