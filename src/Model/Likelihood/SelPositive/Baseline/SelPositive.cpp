//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SelPositive.cpp
 *
 * @date Feb 16, 2015
 * @author meyerx
 * @brief
 */
#include "SelPositive.h"

#include <assert.h>
#include <stdlib.h>

#include "Model/Likelihood/SelPositive/Baseline/../SampleWrapper.h"
#include "Model/Likelihood/SelPositive/Baseline/Nodes/CombineSiteNode.h"
#include "Model/Likelihood/SelPositive/Baseline/Nodes/InternalNode.h"
#include "Model/Likelihood/SelPositive/Baseline/Nodes/MatrixNode.h"
#include "Model/Likelihood/SelPositive/Baseline/Nodes/MatrixScalingNode.h"
#include "Model/Likelihood/SelPositive/Baseline/TreeNodePS.h"
#include "DAG/Node/Base/BaseNode.h"
#include "DAG/Scheduler/Sequential/../BaseScheduler.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/TreeNode.h"
#include "new"

namespace Sampler { class Sample; }

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Base {

class LeafNode;
class RootNode;

SelPositive::SelPositive(const bool aH1, const size_t aFGBranch, const size_t aNThread,
		const std::string &aFileAlign, const std::string &aFileTree) :
	H1(aH1), fgBranch(aFGBranch), nThread(aNThread), fileAlign(aFileAlign), fileTree(aFileTree),
	fr(fileAlign), msa(DL::MSA::CODON, fr.getAlignments(), false),
	np(fileTree), newickRoot(np.getRoot()), rootPS(newickRoot),
	frequencies(MD::Codons::NB_CODONS_WO_STOP, DL::CodonFrequencies(msa).getCodonF61()) {

	first = true;
	isLogLH = true;
	isUpdatableLH = true;

	fgInternalNodeId = 0;
	fgNodePS = NULL;
	nSite = msa.getNValidSite();

	prepareDAG();
	createTreeAndDAG();

	if(nThread > 1) {
		//scheduler = new DAG::Scheduler::Dynamic::ThreadSafeScheduler(nThread, rootDAG);
		Eigen::initParallel();
		scheduler = new DAG::Scheduler::PriorityList::ThreadSafeScheduler(nThread, rootDAG);
		//scheduler = new DAG::Scheduler::Dynamic::ThreadSafeScheduler(nThread, rootDAG);
	} else {
		scheduler = new DAG::Scheduler::Sequential::SequentialScheduler(rootDAG);
	}

	markerNames.push_back("MRKR_Prop0");
	markerNames.push_back("MRKR_Prop1");
	markerNames.push_back("MRKR_Prop2a");
	markerNames.push_back("MRKR_Prop2b");
	markers.resize(markerNames.size());

}

SelPositive::~SelPositive() {
	rootPS.deleteChildren();
	rootDAG->deleteChildren();
	delete rootDAG;
	delete scheduler;

}

double SelPositive::processLikelihood(const Sampler::Sample &sample) {

	// Update the values
	SampleWrapper sw(isH1(), sample.getDblValues());
	setSample(sw);

	// Reset the DAG (could use partial result instead
	scheduler->resetDAG();
	// Process the tree
	scheduler->process();

	markers[0] = scalingMat->getProportions().getProportion(0);
	markers[1] = scalingMat->getProportions().getProportion(1);
	markers[2] = scalingMat->getProportions().getProportion(2);
	markers[3] = scalingMat->getProportions().getProportion(3);

	// return the likelihood
	return rootDAG->getLikelihood();
}

std::string SelPositive::getName(char sep) const {
	return std::string("SelectionPositiveBase");
}

size_t SelPositive::getNBranch() const {
	return branchPtr.size();
}

DAG::Scheduler::BaseScheduler* SelPositive::getScheduler() const {
	return scheduler;
}

size_t SelPositive::SelPositive::stateSize() const {
	return Q0->serializedSize() + Q1->serializedSize() + Q2->serializedSize();
}

void SelPositive::setStateLH(const char* aState) {
	Q0->serializeFromBuffer(aState);
	Q1->serializeFromBuffer(aState+Q0->serializedSize());
	Q2->serializeFromBuffer(aState+Q0->serializedSize()+Q1->serializedSize());
}

void SelPositive::getStateLH(char* aState) const {
	Q0->serializeToBuffer(aState);
	Q1->serializeToBuffer(aState+Q0->serializedSize());
	Q2->serializeToBuffer(aState+Q0->serializedSize()+Q1->serializedSize());
}

double SelPositive::update(const vector<size_t>& pIndices, const Sample& sample) {

	SampleWrapper sw(isH1(), sample.getDblValues());
	setSample(sw);

	// Reset the DAG
	if(!first) { // Partial
		scheduler->resetPartial();
	} else { // Full the first time
		scheduler->resetDAG();
		first = false;
	}

	// Process the tree
	scheduler->process();

	markers[0] = scalingMat->getProportions().getProportion(0);
	markers[1] = scalingMat->getProportions().getProportion(1);
	markers[2] = scalingMat->getProportions().getProportion(2);
	markers[3] = scalingMat->getProportions().getProportion(3);

	return rootDAG->getLikelihood();
}



size_t SelPositive::getNThread() const {
	return nThread;
}

size_t SelPositive::getFGBranch() const {
	return fgBranch;
}

std::string SelPositive::getAlignFile() const {
	return fileAlign;
}

std::string SelPositive::getTreeFile() const {
	return fileTree;
}

bool SelPositive::isH1() const {
	return H1;
}

void SelPositive::prepareDAG() {

	// Create Matrix nodes
	Q0 = new MatrixNode(MatrixNode::Q0, frequencies);
	Q1 = new MatrixNode(MatrixNode::Q1, frequencies);
	Q1->setOmega(1.);
	Q2 = new MatrixNode(MatrixNode::Q2, frequencies);
	if(!isH1()) {
		Q2->setOmega(1.);
	}

	// Create Matrix Scaling and proportions node
	scalingMat = new MatrixScalingNode();
	scalingMat->addChild(Q0);
	scalingMat->addChild(Q1);
	scalingMat->addChild(Q2);

	// Root of the DAG
	rootDAG = new CombineSiteNode(nSite);

}

void SelPositive::createTreeAndDAG() {

	// Labelize internal branches
	labelizeInternalBranches(newickRoot);
	if(fgBranch >= internalBranches.size()) {
		std::cerr << "[ERROR] There are " << internalBranches.size() << " therefore the branch (" << fgBranch << ") is invalid." << std::endl;
		abort();
	}
	fgInternalNodeId = internalBranches[fgBranch];

	// Root
	bool isAfterFG = createTree(newickRoot, &rootPS);
	assert(isAfterFG == true);
	rootPS.setAfterFG(isAfterFG);

	// Initialise combine class
	for(size_t iS=0; iS<nSite; ++iS) { // For each size
		// Create combine class
		combClassNodes.push_back(new CombineClassNode());

		combClassNodes.back()->addChild(scalingMat);
		rootDAG->addChild(combClassNodes[iS]);

		// Create trees
		for(size_t iC=0; iC<TreeNode::N_CLASS; ++iC) { // For each class
			siteClassPS_t siteClass = static_cast<siteClassPS_t>(iC);
			// Init root
			RootNode *rootSubDAG = new RootNode(siteClass, frequencies);

			rootPS.addRootDAG(rootSubDAG);
			combClassNodes.back()->addChild(rootSubDAG);

			// Create internals and leaves
			createSubDAG(iS, siteClass, &rootPS, rootSubDAG);
		}
	}
}


/**
 * @param newickNode a new from DataLoader::TreeNode. i.e. the ones returned from NewickParser
 * @param node a node in the internal Positive Selection tree
 * @return If the current node is the FG node or after the FG node
 */
bool SelPositive::createTree(const DL::TreeNode &newickNode, TreeNodePS *node) {

	node->setFGBranch(newickNode.getId() == fgInternalNodeId);
	node->setAfterFG(false);

	if(newickNode.children.size() > 0) { // If node has children, process
		// For all children in newick, create in PosSel
		bool isAfterFG = false;
		for(size_t i=0; i<newickNode.getChildren().size(); ++i){
			TreeNodePS *childNode = new TreeNodePS(newickNode.getChildren()[i]);
			branchPtr.push_back(childNode); // Keep pointer to branch for t update

			bool childIsAfterFG = createTree(newickNode.getChildren()[i], childNode);
			isAfterFG = isAfterFG || childIsAfterFG;
			node->addChild(childNode);
			if(childNode->isFGBranch()) { // Keep pointer to FG node -> branch
				fgNodePS = childNode;
			}
		}

		// Updage if it is after FG branch)
		node->setAfterFG(isAfterFG);

	}

	//DBG std::cout << node->toString() << std::endl << "-----------------------" << std::endl;

	return node->isFGBranch() || node->isAfterFG();
}

/**
 * Define the internal branch number for each branch of the tree.
 * Branch number are mapped using the internalBranches vectors
 *
 * @param newickNode node of the newick tree
 */
void SelPositive::labelizeInternalBranches(const DL::TreeNode &newickNode) {
	if(newickNode.children.size() > 0 && &newickNode != &newickRoot) {
		internalBranches.push_back(newickNode.getId());
	}

	for(size_t i=0; i<newickNode.children.size(); ++i) {
		labelizeInternalBranches(newickNode.children[i]);
	}
}


/**
 * Define the right matrix in function of the class of the site and the type of branch (FG, BG)
 *
 * @param siteClass the class of the site.
 * @param node the node that start the transition.
 * @return
 */
DAG::BaseNode* SelPositive::defineMatrixNode(siteClassPS_t siteClass, TreeNodePS *node) const {
	if(siteClass == TreeNode::C_0){ // Class 0 is always Q0
		return Q0;
	}

	if(siteClass == TreeNode::C_1){ // Class 1 is always Q1
		return Q1;
	}

	if(siteClass == TreeNode::C_2a){
		if(node->isFGBranch()) { // class 2a and foreground branch is Q2
			return Q2;
		} else { // otherwise Q0
			return Q0;
		}
	}

	if(siteClass == TreeNode::C_2b){
		if(node->isFGBranch()) { // class 2a and foreground branch is Q2
			return Q2;
		} else { // otherwise Q1
			return Q1;
		}
	}

	std::cerr << "DAG::BaseNode* SelPositive::defineMatrixNode(siteClassPS_t siteClass, TreeNodePS &node) const;" << std::endl;
	std::cerr << "Error : No matrix found for the current DAG node." << std::endl;
	abort();

	return NULL;
}



/**
 * This method create one sub DAG per site and class. For each node we create its son nodes in the DAG by doing
 * the following steps
 * 1) Create the right child DAG node
 * 2) Register the newly created DAG node to its parent(s) (a) and to the node of the internal Pos. Sel. tree (b).
 * 3) Add the correct DAG dependencies to the child node (MatrixNode, MatrixScalingNode)
 *
 * @param iSite the site position in the alignment.
 * @param siteClass the class currently processed.
 * @param node the "parent" node in the internal positive sel. tree.
 * @param nodeDAG the "parent" node in the DAG.
 */
void SelPositive::createSubDAG(const size_t iSite, const siteClassPS_t siteClass, TreeNodePS *node, DAG::BaseNode *nodeDAG) {

	for(size_t i=0; i<node->getChildren().size(); ++i) {
		// Create DAG node (1) and register in the positive sel. tree (2b)
		DAG::BaseNode *childNodeDAG;
		TreeNodePS *childNode = node->getChild(i);
		if(childNode->isLeaf()) { // Create leaf element
			DL::Alignment::vecCode_t cdnVals = msa.getValidSiteCode(childNode->getName(), iSite);
			LeafNode *newNode = new LeafNode(siteClass, frequencies, cdnVals);

			childNode->addLeafDAG(newNode); // Add the DAG node to PS tree
			childNodeDAG = newNode;
		} else { // create internal element
			InternalNode *newNode = new InternalNode(siteClass, frequencies);

			childNode->addInteralDAG(newNode); // Add the DAG node to PS tree
			childNodeDAG = newNode;
			createSubDAG(iSite, siteClass, childNode, childNodeDAG);

			// Check if FG branch (4)
			if(childNode->isFGBranch()) {
				newNode->setIsFgBranch(true);
			}

		}

		// Add the children node to the DAG (register to its father) (2a)
		nodeDAG->addChild(childNodeDAG);

		// Add the dependecies (3)
		childNodeDAG->addChild(scalingMat);
		childNodeDAG->addChild(defineMatrixNode(siteClass, childNode));
	}

	//DBG std::cout << node->toString() << std::endl << "-----------------------" << std::endl;
}

void SelPositive::setSample(const SampleWrapper &sw) {
	setKappa(sw);
	setOmega0(sw);
	if(isH1()) {
		setOmega2(sw);
	}
	setProportions(sw);
	setBranchLength(sw);
}

void SelPositive::setKappa(const SampleWrapper &sw) {
	Q0->setKappa(sw.K);
	Q1->setKappa(sw.K);
	Q2->setKappa(sw.K);
}

void SelPositive::setOmega0(const SampleWrapper &sw) {
	Q0->setOmega(sw.W0);
}

void SelPositive::setOmega2(const SampleWrapper &sw) {
	Q2->setOmega(sw.W2);
}

void SelPositive::setProportions(const SampleWrapper &sw) {
	scalingMat->setP0(sw.P0);
	scalingMat->setP1(sw.P1);
}

void SelPositive::setBranchLength(const SampleWrapper &sw) {
	for(size_t i=0; i<sw.N_BL; ++i) {
		branchPtr[i]->setBranchLength(sw.branchLength[i]);
	}
}

} /* namespace Base */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
