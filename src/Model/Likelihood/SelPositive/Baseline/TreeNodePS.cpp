//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeNodePS.cpp
 *
 * @date Feb 16, 2015
 * @author meyerx
 * @brief
 */
#include "TreeNodePS.h"

#include <stdlib.h>
#include <sys/types.h>

#include "Utils/MolecularEvolution/DataLoader/Tree/TreeNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Base {

class InternalNode;
class LeafNode;
class RootNode;

TreeNodePS::TreeNodePS(const MolecularEvolution::DataLoader::TreeNode &aNewickNode, bool aIsFGBranch) {
	id = aNewickNode.getId();
	name = aNewickNode.getName();
	branchLength = aNewickNode.getLength();

	fgBranch = aIsFGBranch;
	afterFG = false;
	leaf = aNewickNode.isLeaf();

}

TreeNodePS::~TreeNodePS() {
}


void TreeNodePS::addChild(TreeNodePS *child) {
	children.push_back(child);
}

TreeNodePS* TreeNodePS::getChild(size_t id) {
	return children[id];
}


const treeNodePS_children& TreeNodePS::getChildren() const {
	return children;
}

const std::string& TreeNodePS::getName() const {
	return name;
}

void TreeNodePS::addLeafDAG(LeafNode *node) {
	leafDAG.push_back(node);
}

void TreeNodePS::addInteralDAG(InternalNode *node) {
	internalDAG.push_back(node);
}

void TreeNodePS::addRootDAG(RootNode *node) {
	rootDAG.push_back(node);
}

void TreeNodePS::setBranchLength(const double aBL) {
	if(branchLength != aBL) {

		branchLength = aBL;

		if(isLeaf()) {
			for(size_t i=0; i<leafDAG.size(); ++i){
				leafDAG[i]->setBranchLength(branchLength);
			}
		} else {
			for(size_t i=0; i<internalDAG.size(); ++i){
				internalDAG[i]->setBranchLength(branchLength);
			}
		}
	}
}

void TreeNodePS::setFGBranch(const bool aFGBranch) {
	fgBranch = aFGBranch;

	if(id==0 && fgBranch) {
		std::cerr << "void TreeNodePS::setFGBranch(const bool aFGBranch);" << std::endl;
		std::cerr << "ERROR : TreeNode [" << id << "] cannot be FG branch since its the root." << std::endl;
		abort();
	}

	if(leaf && fgBranch) {
		std::cerr << "void TreeNodePS::setFGBranch(const bool aFGBranch);" << std::endl;
		std::cerr << "ERROR : TreeNode [" << id << "] cannot be FG branch since its a leaf." << std::endl;
		abort();
	}


	if(isLeaf()) {
		for(size_t i=0; i<leafDAG.size(); ++i){
			leafDAG[i]->setIsFgBranch(fgBranch);
		}
	} else {
		for(size_t i=0; i<internalDAG.size(); ++i){
			internalDAG[i]->setIsFgBranch(fgBranch);
		}
	}
}

void TreeNodePS::setAfterFG(const bool aAfterFG) {
	afterFG = aAfterFG;
}

void TreeNodePS::setLeaf(const bool aLeaf) {
	leaf = aLeaf;
}

bool TreeNodePS::isFGBranch() const {
	return fgBranch;
}

bool TreeNodePS::isAfterFG() const {
	return afterFG;
}

bool TreeNodePS::isLeaf() const {
	return leaf;
}

const std::string TreeNodePS::toString() const {
	using std::stringstream;

	stringstream ss;
	ss << "Node [" << id << "]" << name << " - " << branchLength;
	if(leaf) ss << " - isLeaf";
	if(afterFG) ss << " - isAfterFG";
	if(fgBranch) ss << " - isFGBranch";
	ss << std::endl;
	for(uint i=0; i<children.size(); ++i){
		ss << "[" << children[i]->id << "]" << children[i]->name;
		if(i < children.size()-1) ss << " :: ";
	}

	return ss.str();
}

void TreeNodePS::deleteChildren() {
	while(!children.empty()) {
		TreeNodePS *child = children.back();
		child->deleteChildren();
		children.pop_back();
		delete child;
	}
}

} /* namespace Base */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
