//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SelPositive.h
 *
 * @date Feb 16, 2015
 * @author meyerx
 * @brief This class process the likelihood for the Positive Selection model.
 *
 * This class process the likelihood for the Positive Selection model. It begins by
 * creating an internal tree that will be used to be linked with the DAG nodes. Then
 * the DAG is created and the DAG scheduler is initialised.
 *
 * A sample is formed of the following components :
 * [ k, p0, p1, w0, w1, t(1..nBranch) ]
 *
 */
#ifndef SELPOSITIVEBASE_H_
#define SELPOSITIVEBASE_H_

#include <stddef.h>
#include <fstream>
#include <iostream>
#include <vector>

#include "../SampleWrapper.h"
#include "Model/Likelihood/SelPositive/Baseline/Nodes/TreeNode.h"
#include "DAG/Scheduler/Sequential/SequentialScheduler.h"
#include "DAG/Scheduler/SharedMemory/Dynamic/ThreadSafeScheduler.h"
#include "DAG/Scheduler/SharedMemory/SimplePriority/ThreadSafeScheduler.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "Nodes/IncNodes.h"
#include "TreeNodePS.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/FastaReader.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/Frequencies/CodonFrequencies.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/MSA.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/NewickTree/NewickParser.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/TreeNode.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"
#include "Utils/MolecularEvolution/Definitions/Types.h"

namespace DAG { class BaseNode; }
namespace DAG { namespace Scheduler { class BaseScheduler; } }
namespace MolecularEvolution { namespace DataLoader { class TreeNode; } }
namespace Sampler { class Sample; }
namespace StatisticalModel { namespace Likelihood { namespace PositiveSelection { class SampleWrapper; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Base {

class CombineSiteNode;
class MatrixNode;
class MatrixScalingNode;

namespace DL = ::MolecularEvolution::DataLoader;
namespace MD = ::MolecularEvolution::Definition;

class SelPositive : public LikelihoodInterface {
public:
	SelPositive(const bool aH1, const size_t aFGBranch, const size_t aNThread,
			const std::string &aFileAlign, const std::string &aFileTree);
	~SelPositive();

	//! Process the likelihood
	double processLikelihood(const Sampler::Sample &sample);
	std::string getName(char sep) const;
	size_t getNBranch() const;

	size_t stateSize() const;
	void setStateLH(const char* aState);
	void getStateLH(char* aState) const;
	double update(const vector<size_t>& pIndices, const Sample& sample);

	size_t getFGBranch() const;
	size_t getNThread() const;
	std::string getAlignFile() const;
	std::string getTreeFile() const;
	bool isH1() const;

	DAG::Scheduler::BaseScheduler* getScheduler() const;

private:

	/* Input data */
	const bool H1;
	size_t fgBranch, fgInternalNodeId, nThread, nSite;
	std::string fileAlign, fileTree;
	DL::FastaReader fr;
	DL::MSA msa;
	DL::NewickParser np;
	const DL::TreeNode &newickRoot;

	/* Internal data */
	TreeNodePS rootPS;
	const TreeNodePS* fgNodePS;
	std::vector<TreeNodePS*> branchPtr;
	DL::Utils::Frequencies frequencies;
	std::vector<size_t> internalBranches;

	/* DAG data */
	MatrixNode *Q0, *Q1, *Q2;
	MatrixScalingNode *scalingMat;
	CombineSiteNode *rootDAG;

	typedef std::vector<CombineClassNode*> cClassVec_t;
	cClassVec_t combClassNodes;

	/* DAG Scheduler */
	bool first;
	DAG::Scheduler::BaseScheduler *scheduler;


	//! Prepare DAG elemens (matrices, scaling matrix
	void prepareDAG();
	//! Build the internal tree and the DAG
	void createTreeAndDAG();

	//! Create recursively the internal tree based on the newick one
	bool createTree(const DL::TreeNode &newickNode, TreeNodePS *node);

	//! Label internal branches
	void labelizeInternalBranches(const DL::TreeNode &newickNode);

	typedef TreeNode::siteClass_t siteClassPS_t;
	//! Create recursively a sub-DAG (based on the internal Tree)
	void createSubDAG(const size_t iSite, const siteClassPS_t siteClass, TreeNodePS *node, DAG::BaseNode *nodeDAG);
	//! Define the right matrix for the current DAG node
	DAG::BaseNode* defineMatrixNode(siteClassPS_t siteClass, TreeNodePS *node) const;

	// Setters for the samples
	void setSample(const SampleWrapper &sw);
	void setKappa(const SampleWrapper &sw);
	void setProportions(const SampleWrapper &sw);
	void setOmega0(const SampleWrapper &sw);
	void setOmega2(const SampleWrapper &sw);
	void setBranchLength(const SampleWrapper &sw);

};

} /* namespace Base */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* SELPOSITIVEBASE_H_ */
