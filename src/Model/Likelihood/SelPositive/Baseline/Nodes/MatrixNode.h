//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MatrixNode.h
 *
 * @date Feb 10, 2015
 * @author meyerx
 * @brief
 */
#ifndef MATRIXNODE_H_
#define MATRIXNODE_H_

#include <stddef.h>

#include "DAG/Node/Base/BaseNode.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "Eigen/Eigenvalues"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"
#include "Utils/MolecularEvolution/MatrixFactory/CodonModels/CodeMLMF.h"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Base {

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class MatrixNode: public DAG::BaseNode {

public:

	enum OMEGA_CLASS {Q0=0, Q1=1, Q2=2};
	typedef OMEGA_CLASS omega_class_t;

public:
	MatrixNode(omega_class_t aOmegaClass, DL_Utils::Frequencies &aFrequencies);
	~MatrixNode();

	void setKappa(const double aK);
	void setOmega(const double aW);

	const omega_class_t getOmegaClass() const;
	const double getMatrixQScaling() const;
	const Eigen::MatrixXd& getMatrixQ() const;
	const Eigen::MatrixXd& getMatrixA() const ;
	const Eigen::MatrixXd& getEigenVectors() const;
	const Eigen::MatrixXd& getScaledEigenVectors() const;
	const Eigen::VectorXd& getEigenValues() const;

	size_t serializedSize() const;
	void serializeToBuffer(char *buffer) const;
	void serializeFromBuffer(const char *buffer);

private:

	// Coefficients are [kappa, omega]
	omega_class_t omegaClass;
	std::vector<double> coefficients;
	DL_Utils::Frequencies &frequencies;

	double matrixScaling;
	MolecularEvolution::MatrixUtils::CodeML_MF cmlMF;
	Eigen::VectorXd D;
	Eigen::MatrixXd A, Q, V, scaledV;

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	// DEBUG: void eigenLapack(const Eigen::MatrixXd& mat, Eigen::VectorXd &eigenD, Eigen::MatrixXd &eigenV);

};

} /* namespace Base */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* MATRIXNODE_H_ */
