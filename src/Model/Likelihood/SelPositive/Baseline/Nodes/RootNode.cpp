//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RootNode.cpp
 *
 * @date Feb 13, 2015
 * @author meyerx
 * @brief
 */
#include "RootNode.h"

#include "Model/Likelihood/SelPositive/Baseline/Nodes/TreeNode.h"

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Base {

RootNode::RootNode(TreeNode::siteClass_t aSClass, DL_Utils::Frequencies &aFrequencies) :
		DAG::BaseNode(), TreeNode(aSClass), frequencies(aFrequencies) {
	likelihood = 0;
}

RootNode::~RootNode() {
}

double RootNode::getLikelihood() const {
	return likelihood;
}

void RootNode::doProcessing() {

	// Processing g
	Eigen::VectorXd g = *hChildren[0];
	for(size_t i=1; i<hChildren.size(); ++i) {
		g = g.cwiseProduct(*hChildren[i]);
	}
	/*for(size_t i=0; i<childrenNode.size(); ++i){
		const std::type_info &childtype = typeid(*(childrenNode[i]));
		if(childtype == typeid(LeafNode)){
			LeafNode *tmp = dynamic_cast<LeafNode*>(childrenNode[i]);
			if(i==0) g = tmp->getH();
			else g = g.cwiseProduct(tmp->getH());
		} else if(childtype == typeid(InternalNode)){
			InternalNode *tmp = dynamic_cast<InternalNode*>(childrenNode[i]);
			if(i==0) g = tmp->getH();
			else g = g.cwiseProduct(tmp->getH());
		}
	}*/

	likelihood = g.dot(frequencies.getEigen());
}

bool RootNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void RootNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(LeafNode)){
		//childrenNode.push_back(aChild);
		LeafNode *tmp = dynamic_cast<LeafNode*>(aChild);
		hChildren.push_back(tmp->getH());
	} else if(childtype == typeid(InternalNode)){
		//childrenNode.push_back(aChild);
		InternalNode *tmp = dynamic_cast<InternalNode*>(aChild);
		hChildren.push_back(tmp->getH());
	}
}

} /* namespace Base */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
