//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LeafNode.h
 *
 * @date Nov 26, 2014
 * @author meyerx
 * @brief
 */
#ifndef LEAFNODE_H_
#define LEAFNODE_H_

#include <stddef.h>

#include "DAG/Node/Base/BaseNode.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "Eigen/Eigenvalues"
#include "MatrixNode.h"
#include "MatrixScalingNode.h"
#include "TreeNode.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/Alignment.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Base {

class MatrixNode;
class MatrixScalingNode;

namespace DL = ::MolecularEvolution::DataLoader;
namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class LeafNode: public DAG::BaseNode, public TreeNode {
public:
	LeafNode(TreeNode::siteClass_t aSClass, DL_Utils::Frequencies &aFrequencies, const DL::Alignment::vecCode_t& observedCodons);
	~LeafNode();

	void setBranchLength(const double aBL);
	void setIsFgBranch(const bool aIsFgBranch);

	const Eigen::VectorXd* getH() const;

private:

	bool isFgBranch;
	double branchLength;
	DL_Utils::Frequencies &frequencies;
	Eigen::VectorXd g, h;

	MatrixNode* matrixNode;
	MatrixScalingNode* scalingNode;

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);

};

} /* namespace Base */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* LEAFNODE_H_ */
