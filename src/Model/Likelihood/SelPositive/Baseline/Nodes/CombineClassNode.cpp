//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombineClassNode.cpp
 *
 * @date Feb 12, 2015
 * @author meyerx
 * @brief
 */
#include "CombineClassNode.h"

#include "Model/Likelihood/SelPositive/Baseline/Nodes/MatrixScalingNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Base {

class Proportions;

CombineClassNode::CombineClassNode() : DAG::BaseNode(), liksRoot(TreeNode::N_CLASS, 0.) {
	likelihood = 0.;
	mScaling = NULL;

}

double CombineClassNode::getLikelihood() const {
	return likelihood;
}

CombineClassNode::~CombineClassNode() {
}

void CombineClassNode::doProcessing() {

	const Proportions &prop = mScaling->getProportions();

	likelihood = 0.;
	for(size_t i=0; i<liksRoot.size(); ++i) {
		//DEBUG:
		//std::cout << liksRoot[i] << " -- " << prop.getProportion(i) << " -- " << prop.getProportion(i) * liksRoot[i] << std::endl;
		likelihood += prop.getProportion(i) * liksRoot[i];
	}
}

bool CombineClassNode::processSignal(DAG::BaseNode* aChild) {

	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(RootNode)){
		RootNode *tmp = dynamic_cast<RootNode*>(aChild);
		size_t classID = tmp->getIDSClass();
		liksRoot[classID] = tmp->getLikelihood();
	}

	return true;
}

void CombineClassNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(MatrixScalingNode)){
		mScaling = dynamic_cast<MatrixScalingNode*>(aChild);
	}
}

} /* namespace Base */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
