//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombineClassNode.h
 *
 * @date Feb 12, 2015
 * @author meyerx
 * @brief
 */
#ifndef COMBINECLASSNODE_H_
#define COMBINECLASSNODE_H_

#include <stddef.h>
#include <vector>

#include "DAG/Node/Base/BaseNode.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "MatrixScalingNode.h"
#include "Proportions.h"
#include "RootNode.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Base {

class MatrixScalingNode;

class CombineClassNode : public DAG::BaseNode {
public:
	CombineClassNode();
	~CombineClassNode();

	double getLikelihood() const;

private:

	double likelihood;
	std::vector<double> liksRoot;
	MatrixScalingNode *mScaling;

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);
};

} /* namespace Base */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* COMBINECLASSNODE_H_ */
