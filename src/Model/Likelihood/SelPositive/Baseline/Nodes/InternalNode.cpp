//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file InteriorNode.cpp
 *
 * @date Feb 10, 2015
 * @author meyerx
 * @brief
 */
#include "InternalNode.h"

#include "Model/Likelihood/SelPositive/Baseline/Nodes/MatrixNode.h"
#include "Model/Likelihood/SelPositive/Baseline/Nodes/MatrixScalingNode.h"
#include "Model/Likelihood/SelPositive/Baseline/Nodes/TreeNode.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Base {

InternalNode::InternalNode(TreeNode::siteClass_t aSClass, DL_Utils::Frequencies &aFrequencies) :
	DAG::BaseNode(), TreeNode(aSClass), frequencies(aFrequencies) {

	branchLength = 0;
	isFgBranch = false;
	scalingNode = NULL;
	matrixNode = NULL;

}

InternalNode::~InternalNode() {

}

void InternalNode::setBranchLength(const double aBL) {
	branchLength = aBL;
	this->updated();
}

void InternalNode::setIsFgBranch(const bool aIsFgBranch) {
	isFgBranch = aIsFgBranch;
}

const Eigen::VectorXd* InternalNode::getH() const {
	return &h;
}

void InternalNode::doProcessing() {

	// Shortcuts
	const size_t N = frequencies.size();
	const Eigen::VectorXd &D = matrixNode->getEigenValues();
	const Eigen::MatrixXd &scaledV = matrixNode->getScaledEigenVectors();
	const Eigen::VectorXd &invPi = frequencies.getInvEigen();

	// Process number of substitutions
	double t = branchLength;
	if(isFgBranch) {
		t /= scalingNode->getScalingFG();
	} else {
		t /= scalingNode->getScalingBG();
	}

	Eigen::VectorXd g = *hChildren[0];
	for(size_t i=1; i<hChildren.size(); ++i) {
		g = g.cwiseProduct(*hChildren[i]);
	}
	/*Eigen::VectorXd g;
	for(size_t i=0; i<childrenNode.size(); ++i){
		const std::type_info &childtype = typeid(*childrenNode[i]);
		if(childtype == typeid(LeafNode)){
			LeafNode *tmp = dynamic_cast<LeafNode*>(childrenNode[i]);
			if(i==0) g = tmp->getH();
			else g = g.cwiseProduct(tmp->getH());
		} else if(childtype == typeid(InternalNode)){
			InternalNode *tmp = dynamic_cast<InternalNode*>(childrenNode[i]);
			if(i==0) g = tmp->getH();
			else g = g.cwiseProduct(tmp->getH());
		}
	}*/

	// Processing Y
	Eigen::ArrayXd dt = (t/2.0) * D.array();
	dt = dt.exp();
	Eigen::VectorXd dt2 = dt.matrix();

	Eigen::MatrixXd Y = scaledV * dt2.asDiagonal();

	Eigen::MatrixXd Z(N, N);
	Z.setZero();
	Z.triangularView<Eigen::Lower>() = Y*Y.transpose();

	// Multiplication
	Eigen::VectorXd tmpH = Z.selfadjointView<Eigen::Lower>() * g;
	h = invPi.cwiseProduct(tmpH);

	/*if(isFgBranch) std::cout << "F->";
	cout << "I[" << getId() << "] Class = " << getIDSClass() << " - G Res = " << g.sum() << std::endl;

	if(isFgBranch) std::cout << "F->";
	cout << "I[" << getId() << "] Class = " << getIDSClass() << " - H Res = " << tmpH.sum() << std::endl;*/

}

bool InternalNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void InternalNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(MatrixNode)){
		matrixNode = dynamic_cast<MatrixNode*>(aChild);
	} else if(childtype == typeid(MatrixScalingNode)){
		scalingNode = dynamic_cast<MatrixScalingNode*>(aChild);
	} else if(childtype == typeid(LeafNode)){
		LeafNode *tmp = dynamic_cast<LeafNode*>(aChild);
		hChildren.push_back(tmp->getH());
		//childrenNode.push_back(aChild);
	} else if(childtype == typeid(InternalNode)){
		InternalNode *tmp = dynamic_cast<InternalNode*>(aChild);
		hChildren.push_back(tmp->getH());
		//childrenNode.push_back(aChild);
	}
}

} /* namespace Base */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
