//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombineSiteNode.cpp
 *
 * @date Feb 10, 2015
 * @author meyerx
 * @brief
 */
#include "CombineSiteNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Base {

CombineSiteNode::CombineSiteNode(const size_t aN_SITES) :
		DAG::BaseNode(), DAG::LikelihoodNode(), N_SITES(aN_SITES), likSites(N_SITES, 0.) {
}

CombineSiteNode::~CombineSiteNode() {
}

void CombineSiteNode::doProcessing() {

	// LOG_LIKELIHOOD
	likelihood = 0.;
	for(size_t i=0; i<likSites.size(); ++i) {
		likelihood += log(likSites[i]);
	}

}

bool CombineSiteNode::processSignal(DAG::BaseNode* aChild) {

	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CombineClassNode)){
		CombineClassNode *tmp = dynamic_cast<CombineClassNode*>(aChild);
		likSites[DAG::BaseNode::getChildrenRow(aChild)] = tmp->getLikelihood();
	}

	return true;
}

void CombineSiteNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
}


} /* namespace Base */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
