//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeNode.h
 *
 * @date Feb 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef TREENODEDAGPS_H_
#define TREENODEDAGPS_H_

#include <stddef.h>

#include "stddef.h"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Base {

class TreeNode {
public:

	static const size_t N_CLASS;
	enum SiteClass { C_0=0, C_1=1, C_2a=2, C_2b=3 };
    typedef SiteClass siteClass_t;

public:
	TreeNode(siteClass_t aSClass);
	virtual ~TreeNode();

	siteClass_t getSClass() const;
	size_t getIDSClass() const;

protected:
	siteClass_t sClass;
};

} /* namespace Base */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* TREENODEDAGPS_H_ */
