//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file RootNode.h
 *
 * @date Feb 13, 2015
 * @author meyerx
 * @brief
 */
#ifndef ROOTNODE_H_
#define ROOTNODE_H_

#include <stddef.h>

#include "DAG/Node/Base/BaseNode.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "InternalNode.h"
#include "LeafNode.h"
#include "MatrixNode.h"
#include "MatrixScalingNode.h"
#include "TreeNode.h"

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }


namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Base {

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class RootNode: public DAG::BaseNode, public TreeNode {
public:
	RootNode(TreeNode::siteClass_t aSClass, DL_Utils::Frequencies &aFrequencies);
	~RootNode();

	double getLikelihood() const;

private:

	double likelihood;
	DL_Utils::Frequencies &frequencies;
	std::vector<const Eigen::VectorXd*> hChildren;
	//std::vector<BaseNode*> childrenNode;

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);
};

} /* namespace Base */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* ROOTNODE_H_ */
