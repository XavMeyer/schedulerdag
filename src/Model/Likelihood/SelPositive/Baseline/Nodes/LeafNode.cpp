//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LeafNode.cpp
 *
 * @date Nov 26, 2014
 * @author meyerx
 * @brief
 */
#include "LeafNode.h"

#include "Model/Likelihood/SelPositive/Baseline/Nodes/MatrixNode.h"
#include "Model/Likelihood/SelPositive/Baseline/Nodes/MatrixScalingNode.h"
#include "Model/Likelihood/SelPositive/Baseline/Nodes/TreeNode.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Base {

LeafNode::LeafNode(TreeNode::siteClass_t aSClass, DL_Utils::Frequencies &aFrequencies, const DL::Alignment::vecCode_t& observedCodons) :
		DAG::BaseNode(), TreeNode(aSClass), frequencies(aFrequencies), g(frequencies.size()), h(frequencies.size()) {

	isFgBranch = false;
	branchLength = 0;
	scalingNode = NULL;
	matrixNode = NULL;

	// init g - should use eigen::Zero but its messy with eclipse
	g.setZero();

	for(size_t i=0; i<observedCodons.size(); ++i) {
		g(observedCodons[i]) = 1.0;
	}
}

LeafNode::~LeafNode() {
}

void LeafNode::setBranchLength(const double aBL) {
	branchLength = aBL;
	this->updated();
}

void LeafNode::setIsFgBranch(const bool aIsFgBranch) {
	isFgBranch = aIsFgBranch;
}

const Eigen::VectorXd* LeafNode::getH() const {
	return &h;
}

void LeafNode::doProcessing() {

	// Shortcuts
	const size_t N = frequencies.size();
	const Eigen::VectorXd &D = matrixNode->getEigenValues();
	const Eigen::MatrixXd &scaledV = matrixNode->getScaledEigenVectors();
	const Eigen::VectorXd &invPi = frequencies.getInvEigen();

	// Process number of substitutions
	double t = branchLength;
	if(isFgBranch) {
		t /= scalingNode->getScalingFG();
	} else {
		t /= scalingNode->getScalingBG();
	}

	// Processing Y
	Eigen::ArrayXd dt = D.array() * (t/2.0);
	dt = dt.exp();
	Eigen::VectorXd dt2 = dt.matrix();

	Eigen::MatrixXd Y = scaledV * dt2.asDiagonal();

	Eigen::MatrixXd Z(N, N);
	Z.setZero();
	Z.triangularView<Eigen::Lower>() = Y*Y.transpose();
	//Z = Y * Y.transpose();

	// Multiplication
	Eigen::VectorXd tmpH = Z.selfadjointView<Eigen::Lower>() * g;
	//Eigen::VectorXd tmpH = Z * g;

	h = invPi.cwiseProduct(tmpH);

	/*std::cout << "----------------------------------------------------------"<< std::endl;
	std::cout << "L[" << getId() << "] (  bl = " << branchLength << " t = " << t << " Class = " << getIDSClass() << " - Res = " << tmpH.sum() << ") " << std::endl;
	for(size_t i=0; i<N;++i) {
		if(g(i) > 1e-20) {
			std::cout << std::scientific << "[" << i << "] = " << g(i) << "\t";
		}
	}
	std::cout << std::endl;
	for(size_t i=0; i<N;++i) {
		if(tmpH[i] > 1e-20) {
			std::cout << std::scientific << "[" << i << "] = " << tmpH[i] << "\t";
		}
	}
	std::cout << std::endl;*/
}

bool LeafNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void LeafNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(MatrixNode)){
		matrixNode = dynamic_cast<MatrixNode*>(aChild);
	} else if(childtype == typeid(MatrixScalingNode)){
		scalingNode = dynamic_cast<MatrixScalingNode*>(aChild);
	}
}

} /* namespace Base */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
