//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MatrixNode.cpp
 *
 * @date Feb 10, 2015
 * @author meyerx
 * @brief
 */
#include "MatrixNode.h"

extern "C" int dsyevr_(char *jobz, char *range, char *uplo, int *n,
					double *a, int *lda, double *vl, double *vu,
					int *il, int *iu, double *abstol, int *m, double *w,
					double *z__, int *ldz, int *isuppz, double *work,
					int *lwork, int *iwork, int *liwork, int *info);


namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Base {

MatrixNode::MatrixNode(omega_class_t aOmegaClass, DL_Utils::Frequencies &aFrequencies) :
		DAG::BaseNode(), omegaClass(aOmegaClass), frequencies(aFrequencies), cmlMF(false),
		D(frequencies.size()), V(frequencies.size(),frequencies.size()), scaledV(frequencies.size(), frequencies.size()) {

	coefficients.assign(cmlMF.getNBCoefficients(), 0.);
	cmlMF.setFrequencies(frequencies.getStd());
	matrixScaling = 0.;
}

MatrixNode::~MatrixNode() {
}

void MatrixNode::setKappa(const double aK) {
	if(coefficients[0] != aK) {
		coefficients[0] = aK;
		DAG::BaseNode::updated(); // signal that the node must be recomputed
	}
}

void MatrixNode::setOmega(const double aW) {
	if(coefficients[1] != aW) {
		coefficients[1] = aW;
		DAG::BaseNode::updated(); // signal that the node must be recomputed
	}
}

const MatrixNode::omega_class_t MatrixNode::getOmegaClass() const {
	return omegaClass;
}

const double MatrixNode::getMatrixQScaling() const {
	return matrixScaling;
}

const Eigen::MatrixXd& MatrixNode::getMatrixQ() const {
	return Q;
}

const Eigen::MatrixXd& MatrixNode::getMatrixA() const {
	return A;
}

const Eigen::MatrixXd& MatrixNode::getEigenVectors() const {
	return V;
}

const Eigen::MatrixXd& MatrixNode::getScaledEigenVectors() const {
	return scaledV;
}


const Eigen::VectorXd& MatrixNode::getEigenValues() const {
	return D;
}

size_t MatrixNode::serializedSize() const {
	const size_t N = cmlMF.getMatrixDimension();
	return (3+N+N*N)*sizeof(double);
}

void MatrixNode::serializeToBuffer(char *buffer) const {
	double *tmp = reinterpret_cast<double *>(buffer);

	size_t cnt = 0;
	// Copy Q scaling
	tmp[cnt] = matrixScaling; cnt++;
	tmp[cnt] = coefficients[0]; cnt++;
	tmp[cnt] = coefficients[1]; cnt++;


	// Copy EigenValues
	for(size_t i=0; i<(size_t)D.size(); ++i) {
		tmp[cnt+i] = *(D.data() + i);
	}
	cnt += D.size();

	// Copy EigenVectors
	for(size_t i=0; i<(size_t)scaledV.size(); ++i) {
		tmp[cnt+i] = *(scaledV.data() + i);
	}
	cnt += scaledV.size();
}

void MatrixNode::serializeFromBuffer(const char *buffer) {
	const double *tmp = reinterpret_cast<const double *>(buffer);

	bool hasChanged = false;

	size_t cnt = 0;
	// Copy Q scaling
	matrixScaling = tmp[cnt]; cnt++;
	hasChanged = hasChanged || coefficients[0] != tmp[cnt];
	coefficients[0] = tmp[cnt]; cnt++;
	hasChanged = hasChanged || coefficients[1] != tmp[cnt];
	coefficients[1] = tmp[cnt]; cnt++;


	// Copy EigenValues
	for(size_t i=0; i<(size_t)D.size(); ++i) {
		*(D.data() + i) = tmp[cnt+i];
	}
	cnt += D.size();

	// Copy EigenVectors
	for(size_t i=0; i<(size_t)scaledV.size(); ++i) {
		*(scaledV.data() + i) = tmp[cnt+i];
	}
	cnt += scaledV.size();

	if(hasChanged) {
		DAG::BaseNode::hasBeenSerialized();
	}

}

/* DEBUG
void MatrixNode::eigenLapack(const Eigen::MatrixXd& mat, Eigen::VectorXd &eigenD, Eigen::MatrixXd &eigenV) {

	// Compute eigen valuers and vector
	const size_t N = 61;
	char cU = 'U', cV = 'V', cA = 'A';
	double tmpWORK, dummy, *WORK;
	int LWORK=-1, LIWORK=-1, INFO=0, DIM=N, LD=N;
	int m, tmpIWORK, *IWORK, ISUPPZ[2*N], dummyI;

	double tmpA[N*N], tmpV[N*N], tmpD[N];

	// Copy A;
	for(size_t i=0; i<N;++i){
		for(size_t j=0; j<N; ++j){
		tmpA[i*N+j] = mat(j, i);
		}
	}

	dsyevr_(&cV, &cA, &cU, &DIM, tmpA, &DIM, &dummy, &dummy, &dummyI, &dummyI, &dummy,
			&m, tmpD, tmpV, &LD, ISUPPZ, &tmpWORK, &LWORK, &tmpIWORK, &LIWORK, &INFO);

	// Compute eigenvalues and eigenvectors for the full symmetric matrix
	LWORK = (int) tmpWORK;
	WORK = new double[LWORK];

	LIWORK = tmpIWORK;
	IWORK = new int[LIWORK];


	// Compute eigenvalues and eigenvectors for the full symmetric matrix
	//dsyevr_("V", "A", "U", &aDim, aU, &aDim, &D0, &D0, &I0, &I0, &D0, &m, aR, tmp_u, &N64, isuppz, work, &lwork, iwork, &liwork, &info)
	dsyevr_(&cV, &cA, &cU, &DIM, tmpA, &DIM, &dummy, &dummy, &dummyI, &dummyI, &dummy,
			&m, tmpD, tmpV, &LD, ISUPPZ, WORK, &LWORK, IWORK, &LIWORK, &INFO);

	delete [] WORK;
	delete [] IWORK;

	Eigen::Map<Eigen::MatrixXd> myMapV(tmpV, N, N);
	Eigen::Map<Eigen::VectorXd> myMapD(tmpD, N);

	eigenV = myMapV;
	eigenD = myMapD;

}
*/

void MatrixNode::doProcessing() {

	// shortcut to sqrt frequencies
	const Eigen::VectorXd &sqrtPi = frequencies.getSqrtEigen();
	const Eigen::VectorXd &sqrtInvPi = frequencies.getSqrtInvEigen();

	// Fill the matrix given the new coefficient
	cmlMF.fillMatrix(coefficients);
	matrixScaling = cmlMF.getScalingFactor();

	// Compute
	// Prepare S
	const size_t N = cmlMF.getMatrixDimension();
	Eigen::Map< Eigen::MatrixXd > mappedMat(cmlMF.getMatrix().data(), N, N);
	Q = mappedMat;

	// Compute
	/*
	 * Q = S * PI
	 * S = Q*PI^(-1)
	 *
	 * A = PI^(1/2)*S*PI^(1/2) = PI^(1/2)*Q*PI^(-1)*PI^(1/2) = PI^(1/2)*Q*PI^(-1/2)
	 */
	/*A.resize(N,N);
	A.setZero();
	A.triangularView<Eigen::Lower>() = sqrtInvPi.asDiagonal()*Q*sqrtPi.asDiagonal();*/
	A = sqrtPi.asDiagonal()*Q*sqrtInvPi.asDiagonal();

	// Eigen decomposition : OLD METHOD
	Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> es;
	es.compute(A);

	D = es.eigenvalues();
	V = es.eigenvectors();

	scaledV = sqrtPi.asDiagonal() * V;

}

bool MatrixNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

} /* namespace Base */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
