//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MatrixType.h
 *
 * @date Mar 2, 2015
 * @author meyerx
 * @brief
 */
#ifndef MATRIXTYPE_H_
#define MATRIXTYPE_H_

#include <stddef.h>
#include <assert.h>

#include "Eigen/Core"
#include "Eigen/Dense"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

	static const double LOG_OF_2 = log(2.);

	enum OMEGA_CLASS {OMEGA_0=0, OMEGA_1=1, OMEGA_2=2};
	typedef OMEGA_CLASS omegaClass_t;

	static const size_t N_CLASS = 4;
	enum SiteClass { CLASS_0=0, CLASS_1=1, CLASS_2a=2, CLASS_2b=3 };
    typedef SiteClass siteClass_t;

    enum scalingType_t {NONE=0, LOCAL=1, GLOBAL=2};

    bool isOmegaValidForClass(const omegaClass_t omegaClass, const siteClass_t siteClass);
    bool isOmegaValidForClass(const omegaClass_t omegaClass, const siteClass_t siteClass, const bool isFGBranch);

} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
#endif /* TYPES_H_ */
