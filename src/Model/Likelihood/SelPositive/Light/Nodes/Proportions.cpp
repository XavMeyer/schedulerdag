//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Proportions.cpp
 *
 * @date Feb 11, 2015
 * @author meyerx
 * @brief
 */
#include "Proportions.h"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

const Proportions::proportions_t Proportions::PROP_TYPE = NEW ;

Proportions::Proportions() : proportions(4, 0.) {
	p0 = p1 = 0.;
}

Proportions::Proportions(const double aP0, const double aP1) {
	set(aP0, aP1);
}

Proportions::~Proportions() {
}

void Proportions::set(const double aP0, const double aP1) {
	p0 = aP0;
	p1 = aP1;

	if(PROP_TYPE == NEW) {
		proportions[CLASS_0] = p0*p1;
		proportions[CLASS_1] = p0*(1.-p1);
		proportions[CLASS_2a] = (1.-p0)*p1;
		proportions[CLASS_2b] = (1.-p0)*(1.-p1);
	} else if(PROP_TYPE == ORIGINAL) {
		// ORIGINAL PROPORTIONS
		// Rescale from [0..1]->[-99..99]
		double eV0 = exp((p0-0.5)*2.*99.);
		double eV1 = exp((p1-0.5)*2.*99.);
		// Compute proportions
		proportions[CLASS_0] = eV0/(1.+eV0+eV1);
		proportions[CLASS_1] = eV1/(1.+eV0+eV1);
		double sum = proportions[CLASS_0] + proportions[CLASS_1];
		proportions[CLASS_2a] = (1.-sum)*proportions[CLASS_0]/sum;
		proportions[CLASS_2b] = (1.-sum)*proportions[CLASS_1]/sum;
	}

}

double Proportions::getP0() const {
	return p0;
}

double Proportions::getP1() const {
	return p1;
}

const std::vector<double>& Proportions::getProportions() const {
	return proportions;
}

double Proportions::getProportion(const size_t idx) const {
	return proportions[idx];
}

} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
