//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LeafNode.cpp
 *
 * @date Nov 26, 2014
 * @author meyerx
 * @brief
 */
#include "LeafNode.h"

#include "Model/Likelihood/SelPositive/Light/Nodes/BranchMatrixNode.h"
#include "Model/Likelihood/SelPositive/Light/Nodes/EdgeNode.h"
#include "Model/Likelihood/SelPositive/Light/Nodes/Types.h"

namespace DAG { class BaseNode; }
namespace MolecularEvolution { namespace DataLoader { class MSA; } }

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

LeafNode::LeafNode(const bool aFgBranch, const bool aAfterFgBranch,
		const omegaClass_t aOmegaClass, const std::vector<size_t> &aSitePos,
		const std::string &aSpecieName, const DL::MSA& aMSA,
		const DL::CompressedAlignements *aPtrCA,
		DL_Utils::Frequencies &aFrequencies, bool compress) :
				EdgeNode(aFgBranch, aAfterFgBranch, aOmegaClass, aSitePos, aFrequencies), ptrCA(aPtrCA) {

	if(!compress) {
		init(aSpecieName, aMSA);
	} else {
		createSignatureCPV(aSpecieName, aMSA);
		size_t nCompressed = compressVecCPV();
		if(nCompressed == 0) {
			init(aSpecieName, aMSA);
		} else {
			initCompressed(nCompressed, aSpecieName, aMSA);
		}
	}
}

LeafNode::~LeafNode() {
}

void LeafNode::createSignatureCPV(const std::string &aSpecieName, const DL::MSA& aMSA) {

	if(ptrCA == NULL) {
		for(size_t iCPV=0; iCPV<vecCPV.size(); ++iCPV) {
			DL::Alignment::vecCode_t cdnVals = aMSA.getValidSiteCode(aSpecieName, vecCPV[iCPV].getSitePos());
			vecCPV[iCPV].addToSignature(frequencies.size(), cdnVals);
		}
	} else {
		for(size_t iCPV=0; iCPV<vecCPV.size(); ++iCPV) {
			DL::Alignment::vecCode_t cdnVals = ptrCA->getSiteCode(aSpecieName, vecCPV[iCPV].getSitePos());
			vecCPV[iCPV].addToSignature(frequencies.size(), cdnVals);
		}
	}

}

void LeafNode::init(const std::string &aSpecieName, const DL::MSA& aMSA) {
	Eigen::MatrixXd G;
	G.resize(frequencies.size(), vecCPV.size());
	G.setZero();
	if(ptrCA == NULL) {
		for(size_t iCPV=0; iCPV<vecCPV.size(); ++iCPV) {
			DL::Alignment::vecCode_t cdnVals = aMSA.getValidSiteCode(aSpecieName, vecCPV[iCPV].getSitePos());
			for(size_t iCod=0; iCod < cdnVals.size(); ++iCod) {
				G(cdnVals[iCod], iCPV) =  1.0;
			}
		}
	} else {
		for(size_t iCPV=0; iCPV<vecCPV.size(); ++iCPV) {
			DL::Alignment::vecCode_t cdnVals = ptrCA->getSiteCode(aSpecieName, vecCPV[iCPV].getSitePos());
			for(size_t iCod=0; iCod < cdnVals.size(); ++iCod) {
				G(cdnVals[iCod], iCPV) =  1.0;
			}
		}
	}
	sparseG = G.sparseView();
}

void LeafNode::initCompressed(const size_t nCompressed, const std::string &aSpecieName, const DL::MSA& aMSA) {
	Eigen::MatrixXd G;
	// resize to take account of compression
	G.resize(frequencies.size(), vecCPV.size()-nCompressed);
	H.resize(frequencies.size(), vecCPV.size()-nCompressed);

	if(SCALING_TYPE == NORMAL || SCALING_TYPE == LOG || SCALING_TYPE == LOG_ACCURATE) {
		nrm.resize(vecCPV.size()-nCompressed);
	}

	// Then we initialize G
	G.setZero();

	//std::cout << "Start init compressed" << std::endl;
	for(size_t iCPV=0; iCPV<vecCPV.size(); ++iCPV) {
		if(vecCPV[iCPV].isCompressed()) continue; // Skipping compressed CPV

		if(ptrCA == NULL) {
			DL::Alignment::vecCode_t cdnVals = aMSA.getValidSiteCode(aSpecieName, vecCPV[iCPV].getSitePos());
			for(size_t iCod=0; iCod < cdnVals.size(); ++iCod) {
				G(cdnVals[iCod], vecCPV[iCPV].getIndex()) = 1.0;
			}
		} else {
			DL::Alignment::vecCode_t cdnVals = ptrCA->getSiteCode(aSpecieName, vecCPV[iCPV].getSitePos());
			for(size_t iCod=0; iCod < cdnVals.size(); ++iCod) {
				G(cdnVals[iCod], vecCPV[iCPV].getIndex()) = 1.0;
			}
		}
	}
	sparseG = G.sparseView();
	//std::cout << "end init compressed" << std::endl;
}

void LeafNode::doProcessing() {

	const Eigen::MatrixXd &Z = bmNode->getZ();
	const Eigen::VectorXd &invPi = frequencies.getInvEigen();

	if(SCALING_TYPE == LOG_ACCURATE) {
		//nrm.setZero();
		nrm.resize(0);
	}

	// Multiplication
	Eigen::MatrixXd tmp = Z.selfadjointView<Eigen::Lower>();
	H = (invPi.asDiagonal() * tmp) * sparseG;
	//H = invPi.asDiagonal() * (Z.selfadjointView<Eigen::Lower>() * G);

	if(SCALING_TYPE == NORMAL) {
		nrm.setOnes();
		for(size_t iC=0; iC<(size_t)H.cols(); ++iC){
			double tmpNrm = H.col(iC).norm();
			if(tmpNrm > 0.) {
				nrm(iC) = tmpNrm;
				H.col(iC) = H.col(iC) / tmpNrm;
			}
		}
	} else if(SCALING_TYPE == LOG) {
		nrm.setZero();
		for(size_t iC=0; iC<(size_t)H.cols(); ++iC){
			double tmpNrm = H.col(iC).norm();
			if(tmpNrm > 0.) {
				nrm(iC) = log(tmpNrm);
				H.col(iC) = H.col(iC) / tmpNrm;
			}
		}
	}
}

bool LeafNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void LeafNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(BranchMatrixNode)){
		bmNode = dynamic_cast<BranchMatrixNode*>(aChild);
		if(bmNode->getOmegaClass() != omegaClass){
			std::cerr << "void CPVNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild);" << std::endl;
			std::cerr << "Error : Invalid BranchMatrixNode children. Found Omega_" << bmNode->getOmegaClass();
			std::cerr << " while waiting for omega_" <<  omegaClass << std::endl;
			abort();
		}
	}
}


} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
