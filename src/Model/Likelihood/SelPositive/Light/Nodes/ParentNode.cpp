//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ParentNode.cpp
 *
 * @date Mar 3, 2015
 * @author meyerx
 * @brief
 */
#include "ParentNode.h"

#include <stddef.h>

#include "Model/Likelihood/SelPositive/Light/Nodes/EdgeNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

ParentNode::ParentNode(const bool aFGBranch, const bool aAfterFgBranch, const omegaClass_t aOmegaClass,
					   const std::vector<size_t> &aSitePos, DL_Utils::Frequencies &aFrequencies, const bool aCompress) :
							   EdgeNode(aFGBranch, aAfterFgBranch, aOmegaClass, aSitePos, aFrequencies),
							   compress(aCompress), hChildrenPos(vecCPV.size()) {

}

ParentNode::~ParentNode() {
}

void ParentNode::linkChildren(EdgeNode* childrenNode) {

	if(this->hasEqualVecCPV(*childrenNode)) {
		// Keep the block
		hChildrenBlock.push_back(childrenNode);

		if(compress) { // Update the signature
			for(size_t i=0; i<vecCPV.size(); ++i) {
				// Check if children has this CPV
				const CPV *cpv = childrenNode->getPtrCPV(vecCPV[i].getSitePos(), vecCPV[i].getSiteClass());
				if(cpv != NULL) {
					vecCPV[i].addToSignature(*cpv);
				}
			}
		}
	} else {
		// For each CPV, we ask children node if he have a children CPV
		for(size_t i=0; i<vecCPV.size(); ++i) {
			// Check if children has this CPV
			const CPV *cpv = childrenNode->getPtrCPV(vecCPV[i].getSitePos(), vecCPV[i].getSiteClass());
			if(cpv != NULL) {
				hChildrenPos[vecCPV[i].getIndex()].push_back(hChildrenPos_t(childrenNode, cpv));
				if(compress) { // update the signature
					vecCPV[i].addToSignature(*cpv);
				}
			}
		}
	}
}

void ParentNode::compressNode(){
	if(!compress) {
		std::cerr << "void ParentNode::compressNode();" << std::endl;
		std::cerr << "Error : This parent node cannot be compressed because it wasn't build we the parameter aCompress=true." << std::endl;
		abort();
	}

	// Compress the vecCPV if possible
	size_t nCompressed = compressVecCPV();
	if(nCompressed == 0) return; // No compression possible

	// Resize the matrix G and H
	H.resize(frequencies.size(), vecCPV.size()-nCompressed);

	if(SCALING_TYPE == NORMAL || SCALING_TYPE == LOG || SCALING_TYPE == LOG_ACCURATE) {
		nrm.resize(vecCPV.size()-nCompressed);
	}

	// Adapt the hChildrenPos vector
	vecChildrenPos_t hChildrenPosTmp;
	for(size_t iCPV=0; iCPV<vecCPV.size(); ++iCPV) {
		if(vecCPV[iCPV].isCompressed()) continue; // Skipping compressed CPV

		// not compressed, we store it at the right column
		hChildrenPosTmp.push_back(hChildrenPos[iCPV]);
	}
	hChildrenPos = hChildrenPosTmp;
}

} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
