//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file EdgeNode.cpp
 *
 * @date Mar 3, 2015
 * @author meyerx
 * @brief
 */
#include "EdgeNode.h"

#include "Model/Likelihood/SelPositive/Light/Nodes/CPV.h"
#include "Model/Likelihood/SelPositive/Light/Nodes/Types.h"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

const EdgeNode::scaling_t EdgeNode::SCALING_TYPE = LOG_ACCURATE;
const float EdgeNode::SCALING_THRESHOLD = 1.; // Scale all with 1.

EdgeNode::EdgeNode(const bool aFgBranch, const bool aAfterFgBranch, const omegaClass_t aOmegaClass,
				   const std::vector<size_t> &aSitePos, DL_Utils::Frequencies &aFrequencies) :
		DAG::BaseNode(), fgBranch(aFgBranch), afterFgBranch(aAfterFgBranch), omegaClass(aOmegaClass),
		frequencies(aFrequencies)  {

	bmNode = NULL;
	init(aSitePos);
}

EdgeNode::~EdgeNode() {
}


void EdgeNode::init(const std::vector<size_t> &sitePos) {

	size_t cnt = 0;

	for(size_t iC=0; iC<N_CLASS; ++iC) {
		siteClass_t siteClass = static_cast<siteClass_t>(iC);
		if(isOmegaValidForClass(omegaClass, siteClass)){
			bool doAddW0 = (omegaClass == OMEGA_0) && ((siteClass == CLASS_0) || (isAfterFGBranch() && (siteClass == CLASS_2a)));
			bool doAddW1 = (omegaClass == OMEGA_1) && ((siteClass == CLASS_1) || (isAfterFGBranch() && (siteClass == CLASS_2b)));
			bool doAddW2 = (omegaClass == OMEGA_2) && (isFGBranch());

			if(doAddW0 || doAddW1 || doAddW2) {
				for(size_t iP=0; iP<sitePos.size(); ++iP) {
					// If we are before FG branch, there is no distinction between class {0 + 2a} or {1 + 2b}
					// Thus we remove 2a and 2b class for now
					CPV tmp(sitePos[iP], siteClass, cnt);
					vecCPV.push_back(tmp);
					++cnt;
				}
			}
		}
	}

	// Create vectors
	H.resize(frequencies.size(), vecCPV.size());

	if(SCALING_TYPE == NORMAL || SCALING_TYPE == LOG) {
		nrm.resize(vecCPV.size());
	} else if (SCALING_TYPE == LOG_ACCURATE) {
		nrm.resize(0);
	}
}

const CPV* EdgeNode::getPtrCPV(const size_t aSitePos, const siteClass_t aSiteClass) const {
	// If we are before the FG branch, we do not have any class 2a or 2b vectors (removed in init).
	// Thus if we get asked for it, we have to return class 0 or 1 vectors
	siteClass_t siteClass = aSiteClass;
	bool doTranslate = !isFGBranch() && !isAfterFGBranch();
	if(doTranslate) {
		if(siteClass == CLASS_2a) {
			siteClass = CLASS_0;
		} else if(siteClass == CLASS_2b) {
			siteClass = CLASS_1;
		}
	}

	//CPV toFindCPV(aSitePos, siteClass, 0);
	cstItVecCPV_t it;
	it = std::find_if(vecCPV.begin(), vecCPV.end(), HasSameSitePositionAndClass(aSitePos, siteClass));
	// If the vector is not find there, return NULL
	if(it == vecCPV.end()) {
		return NULL;
	} else {
		return &(*it);
	}
}

const Eigen::MatrixXd& EdgeNode::getH() const {
	return H;
}

const Eigen::VectorXd& EdgeNode::getNorm() const {
	return nrm;
}

omegaClass_t EdgeNode::getOmegaClass() const {
	return omegaClass;
}

bool EdgeNode::isFGBranch() const {
	return fgBranch;
}

bool EdgeNode::isAfterFGBranch() const {
	return afterFgBranch;
}

bool EdgeNode::hasEqualVecCPV(const EdgeNode &other) const {
	if(this->vecCPV.size() != other.vecCPV.size()) {
		return false;
	}

	for(size_t i=0; i<this->vecCPV.size(); ++i) {
		bool isEqual = this->vecCPV[i] == other.vecCPV[i];
		if(!isEqual) {
			return false;
		}
	}

	return true;
}

size_t EdgeNode::compressVecCPV() {

	size_t iBase = 0, nCompressed = 0;
	for(size_t iCPV=0; iCPV<vecCPV.size(); ++iCPV) { // For each CPVs
		// If this element iCPV is already compressed, continue
		if(vecCPV[iCPV].isCompressed()) continue;

		// Set the new position in G and G
		vecCPV[iCPV].setIndex(iBase);
		++iBase;

		for(size_t jCPV=iCPV+1; jCPV<vecCPV.size(); ++jCPV) { // For next CPVs
			// If this element jCPV is already compressed, continue
			if(vecCPV[jCPV].isCompressed()) continue;

			if(vecCPV[iCPV].canCompress(vecCPV[jCPV])){
				// Compress cVal2 -> cVal1
				vecCPV[jCPV].setIndex(vecCPV[iCPV].getIndex());
				vecCPV[jCPV].setCompressed(true);
				nCompressed++;
			}
		}
	}
	return nCompressed;
}

size_t EdgeNode::getNCompressed() const {
	size_t nCompressed = 0;
	for(size_t iCPV=0; iCPV<vecCPV.size(); ++iCPV) {
		if(vecCPV[iCPV].isCompressed()) {
			nCompressed++;
		}
	}
	return nCompressed;
}

} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
