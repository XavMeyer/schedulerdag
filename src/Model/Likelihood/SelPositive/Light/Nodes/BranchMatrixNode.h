//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BranchMatrixNode.h
 *
 * @date Mar 2, 2015
 * @author meyerx
 * @brief
 */
#ifndef BRANCHMATRIXNODELIGHT_H_
#define BRANCHMATRIXNODELIGHT_H_

#include <stddef.h>

#include "DAG/Node/Base/BaseNode.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "MatrixNode.h"
#include "MatrixScalingNode.h"
#include "Types.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

class MatrixNode;
class MatrixScalingNode;

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class BranchMatrixNode: public DAG::BaseNode {
public:
	BranchMatrixNode(const scalingType_t aScalingType, const bool aFGBranch, const omegaClass_t aOmegaClass, DL_Utils::Frequencies &aFrequencies);
	~BranchMatrixNode();

	void setBranchLength(const double aBL);
	void setFgBranch(const bool aFgBranch);
	void setAfterFgBranch(const bool aAfterFgBranch);

	omegaClass_t getOmegaClass() const;

	bool isFGBranch() const {return fgBranch;}
	bool isAfterFGBranch() const {return afterFgBranch;}

	const Eigen::MatrixXd& getZ() const;

private:

	scalingType_t scalingType;
	bool fgBranch, afterFgBranch;
	double branchLength;
	omegaClass_t omegaClass;
	DL_Utils::Frequencies &frequencies;

	MatrixNode *matrixNode;
	MatrixScalingNode *scalingNode;

	Eigen::MatrixXd Z;


	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);

};

} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* BRANCHMATRIXNODELIGHT_H_ */
