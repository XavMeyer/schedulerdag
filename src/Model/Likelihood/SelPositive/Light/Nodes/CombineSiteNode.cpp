//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombineSiteNode.cpp
 *
 * @date Feb 10, 2015
 * @author meyerx
 * @brief
 */
#include "CombineSiteNode.h"

#include "Model/Likelihood/SelPositive/Light/Nodes/EdgeNode.h"
#include "Model/Likelihood/SelPositive/Light/Nodes/MatrixScalingNode.h"
#include "Types.h"

namespace StatisticalModel { namespace Likelihood { namespace PositiveSelection { namespace Light { class Proportions; } } } }
namespace StatisticalModel { namespace Likelihood { namespace PositiveSelection { namespace Light { class RootNode; } } } }

#define LOG_PLUS(x,y) (x>y ? x+log(1.+exp(y-x)) : y+log(1.+exp(x-y)))

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

CombineSiteNode::CombineSiteNode(const std::vector<size_t> &aSites, const DL::CompressedAlignements *aPtrCA) :
		DAG::BaseNode(), ptrCA(aPtrCA) {

	mScaling = NULL;

	// init ptr to likelihoods
	for(size_t i=0; i<aSites.size(); ++i) {
		vecSSLik.push_back(SiteSubLikelihood(aSites[i]));
	}

}

CombineSiteNode::~CombineSiteNode() {
}

void CombineSiteNode::getLikelihoodsForBEB(size_t iClass, std::vector<size_t> &iSite, std::vector<double> &lik, std::vector<double> &nrm) {

	for(size_t iS=0; iS<vecSSLik.size(); ++iS) {
		int idx = vecSSLik[iS].idxSLik[iClass];

		// Site ID
		iSite.push_back(vecSSLik[iS].site);

		// Lik
		const Eigen::VectorXd &subLik = vecSSLik[iS].ptrNode[iClass]->getLikelihood();
		lik.push_back(subLik(idx));

		// Norm
		if(EdgeNode::SCALING_TYPE == EdgeNode::LOG || EdgeNode::SCALING_TYPE == EdgeNode::LOG_ACCURATE) {
			const Eigen::VectorXd &logNorm = vecSSLik[iS].ptrNode[iClass]->getNorm();
			nrm.push_back(logNorm(idx));
		}
	}
}

void CombineSiteNode::doProcessing() {

	const Proportions &prop = mScaling->getProportions();

	if(EdgeNode::SCALING_TYPE == EdgeNode::LOG ||
	   EdgeNode::SCALING_TYPE == EdgeNode::LOG_ACCURATE) {
		// LOG_LIKELIHOOD
		likelihood = 0.;
		for(size_t iS=0; iS<vecSSLik.size(); ++iS) {
			// Combine sub-likelihood at class level
			double logLikSite = 0;
			for(size_t iC=0; iC<N_CLASS; ++iC) {
				int idx = vecSSLik[iS].idxSLik[iC];
				const Eigen::VectorXd &logNorm = vecSSLik[iS].ptrNode[iC]->getNorm();
				const Eigen::VectorXd &subLik = vecSSLik[iS].ptrNode[iC]->getLikelihood();

				double tmpSublik = subLik(idx) >= 0. ? subLik(idx) : 0.;
				double tmpLogLik = log(prop.getProportion(iC) * tmpSublik) + logNorm(idx);

				if(tmpLogLik == -std::numeric_limits<double>::infinity()) tmpLogLik = -std::numeric_limits<double>::max();

				if(iC == 0) {
					logLikSite = tmpLogLik;
				} else {
					logLikSite = LOG_PLUS(logLikSite, tmpLogLik);
				}
			}

			// Combine likelihood at site level
			if(ptrCA != NULL) {
				likelihood += ptrCA->getSiteFactor(vecSSLik[iS].site)*logLikSite;
			} else {
				likelihood += logLikSite;
			}
		}
	} else {
		// LOG_LIKELIHOOD
		likelihood = 0.;
		for(size_t iS=0; iS<vecSSLik.size(); ++iS) {
			// Combine sub-likelihood at class level
			double likSite = 0;
			for(size_t iC=0; iC<N_CLASS; ++iC) {
				int idx = vecSSLik[iS].idxSLik[iC];
				const Eigen::VectorXd &subLik = vecSSLik[iS].ptrNode[iC]->getLikelihood();
				likSite += prop.getProportion(iC) * subLik(idx);
			}

			// Combine likelihood at site level
			if(ptrCA != NULL) {
				likelihood += ptrCA->getSiteFactor(vecSSLik[iS].site)*log(likSite);
			} else {
				likelihood += log(likSite);
			}
		}
	}
}

bool CombineSiteNode::processSignal(BaseNode* aChild) {

	return true;
}

void CombineSiteNode::doAddChild(const size_t rowId, BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(RootNode)){
		RootNode *node = dynamic_cast<RootNode*>(aChild);
		linkLikSite(node);
	} else if(childtype == typeid(MatrixScalingNode)){
		mScaling = dynamic_cast<MatrixScalingNode*>(aChild);
	}
}

void CombineSiteNode::linkLikSite(RootNode *node) {

	// Get node omegaClass
	const omegaClass_t oc = node->getOmegaClass();

	for(size_t iC=0; iC<N_CLASS; ++iC) { // For each site class
		siteClass_t siteClass = static_cast<siteClass_t>(iC);

		// Check if this node could be valid for the current site class
		if(isOmegaValidForClass(oc, siteClass, false)) {
			// If so memorize sub likelihood value location (vectorXd(idx))
			for(size_t iS=0; iS<vecSSLik.size(); ++iS){
				int idxSLik = node->getLikelihoodIndex(vecSSLik[iS].site, siteClass);
				if(idxSLik >= 0) {
					vecSSLik[iS].idxSLik[iC] = idxSLik;
					vecSSLik[iS].ptrNode[iC] = node;
				}
			}
		}
	}
}


} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
