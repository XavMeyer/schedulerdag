//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CPVNode.h
 *
 * @date Mar 3, 2015
 * @author meyerx
 * @brief
 */
#ifndef CPVNODELIGHT_H_
#define CPVNODELIGHT_H_

#include <stddef.h>

#include "Model/Likelihood/SelPositive/Light/Nodes/Types.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "LeafNode.h"
#include "ParentNode.h"

namespace DAG { class BaseNode; }

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class CPVNode : public ParentNode {
public:

	CPVNode(const bool aFgBranch, const bool aAfterFgBranch, const omegaClass_t aOmegaClass,
			const std::vector<size_t> &aSitePos, DL_Utils::Frequencies &aFrequencies, const bool compress = false);
	~CPVNode();

private: // Methods

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);

};

} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* CPVNODELIGHT_H_ */
