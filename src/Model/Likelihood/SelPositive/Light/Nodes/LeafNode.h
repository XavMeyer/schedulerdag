//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LeafNode.h
 *
 * @date Nov 26, 2014
 * @author meyerx
 * @brief
 */
#ifndef LEAFNODELIGHT_H_
#define LEAFNODELIGHT_H_

#include <stddef.h>
#include <string>
#include <vector>

#include "BranchMatrixNode.h"
#include "CPV.h"
#include "DAG/Node/Base/BaseNode.h"
#include "EdgeNode.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "Eigen/Eigenvalues"
#include "Eigen/Sparse"
#include "MatrixScalingNode.h"
#include "Types.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/CompressedAlignements.h"
#include "Utils/MolecularEvolution/DataLoader/Alignment/MSA.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

namespace DAG { class BaseNode; }
namespace MolecularEvolution { namespace DataLoader { class CompressedAlignements; } }
namespace MolecularEvolution { namespace DataLoader { class MSA; } }

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

namespace DL = ::MolecularEvolution::DataLoader;
namespace DL_Utils = ::MolecularEvolution::DataLoader::Utils;

class LeafNode : public EdgeNode {
public:

	LeafNode(const bool aFgBranch, const bool aAfterFgBranch,
			const omegaClass_t aOmegaClass, const std::vector<size_t> &aSitePos,
			const std::string &aSpecieName, const DL::MSA& aMSA,
			const DL::CompressedAlignements *aPtrCA,
			DL_Utils::Frequencies &aFrequencies, bool compress = false);
	~LeafNode();

private:

	const DL::CompressedAlignements *ptrCA;
	Eigen::SparseMatrix<double> sparseG;

	void createSignatureCPV(const std::string &aSpecieName, const DL::MSA& aMSA);
	void init(const std::string &aSpecieName, const DL::MSA& aMSA);
	void initCompressed(const size_t nCompressed, const std::string &aSpecieName, const DL::MSA& aMSA);

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);

};

} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* LEAFNODELIGHT_H_ */
