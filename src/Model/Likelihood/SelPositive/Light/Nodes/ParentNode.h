//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file ParentNode.h
 *
 * @date Mar 3, 2015
 * @author meyerx
 * @brief
 */
#ifndef PARENTNODELIGHT_H_
#define PARENTNODELIGHT_H_

#include "Model/Likelihood/SelPositive/Light/Nodes/Types.h"
#include "EdgeNode.h"

namespace StatisticalModel {
namespace Likelihood {
namespace PositiveSelection {
namespace Light {

class CPV;

class ParentNode : public EdgeNode {
public:
	ParentNode(const bool aFGBranch, const bool aAfterFgBranch, const omegaClass_t aOmegaClass,
			   const std::vector<size_t> &aSitePos, DL_Utils::Frequencies &aFrequencies,
			   const bool aCompress = false);
	virtual ~ParentNode();

	void compressNode();

protected:
	// Define on which CPV node and at which position we find a children's H
	struct hChildrenPos_t {
		EdgeNode *node;
		const CPV *cpv;

		hChildrenPos_t(EdgeNode *aNode, const CPV *aCPV) :
					node(aNode), cpv(aCPV){};
		~hChildrenPos_t(){};
	};
	typedef std::vector<std::vector<hChildrenPos_t> > vecChildrenPos_t; // Associated 2D vector

protected: // Variables

	const bool compress;
	std::vector<EdgeNode*> hChildrenBlock;
	vecChildrenPos_t hChildrenPos;

protected: // Methods
	void linkChildren(EdgeNode* childrenNode);
};

} /* namespace Light */
} /* namespace PositiveSelection */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* PARENTNODELIGHT_H_ */
