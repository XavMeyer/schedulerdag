//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombinedBMNode.cpp
 *
 * @date Jun 30, 2015
 * @author meyerx
 * @brief
 */
#include "CombinedBMNode.h"

#include "Model/Likelihood/BranchSiteREL/Nodes/Types.h"

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

CombinedBMNode::CombinedBMNode(DL_Utils::Frequencies &aFrequencies) :
	BaseNode(), frequencies(aFrequencies), Pbs(frequencies.size(), frequencies.size()), BMNodes(3, NULL) {
	//p0 = p1 = pNegative = pNeutral = pPositive = -1.;
	scalingNode = NULL;
}

CombinedBMNode::~CombinedBMNode() {
}

/*void CombinedBMNode::setProportions(const double aP0, const double aP1) {
	if(aP0 != p0 || aP1 != p1) {

		p0 = aP0;
		p1 = aP1;
		pNegative = p0;
		pNeutral = p1*(1.-p0);
		pPositive = (1.-p0)*(1.-p1);

		BaseNode::updated();
	}
}*/

const Eigen::MatrixXd& CombinedBMNode::getPbs() const {
	return Pbs;
}

/*double CombinedBMNode::getPNegative() const {
	return pNegative;
}

double CombinedBMNode::getPNeutral() const {
	return pNeutral;
}

double CombinedBMNode::getPPositive() const {
	return pPositive;
}*/

size_t CombinedBMNode::serializedSize() const {
	const size_t nParams = 0;
	const size_t sizeMatrix = Pbs.size();
	return (nParams+sizeMatrix)*sizeof(double);
}

void CombinedBMNode::serializeToBuffer(char *buffer) const {
	double *tmp = reinterpret_cast<double *>(buffer);

	size_t cnt = 0;
	// Copy proportions scaling
	//tmp[cnt] = p0; cnt++;
	//tmp[cnt] = p1; cnt++;

	// Copy Pbs
	for(size_t i=0; i<(size_t)Pbs.size(); ++i) {
		tmp[cnt+i] = *(Pbs.data() + i);
	}
	cnt += Pbs.size();
}

void CombinedBMNode::serializeFromBuffer(const char *buffer) {
	const double *tmp = reinterpret_cast<const double *>(buffer);

	bool hasChanged = false;

	size_t cnt = 0;

	/*if(p0 != tmp[0] || p1 != tmp[1]) {
		hasChanged = true;
		p0 = tmp[cnt];
		p1 = tmp[cnt+1];

		pNegative = p0;
		pNeutral = p1*(1.-p0);
		pPositive = (1.-p0)*(1.-p1);

	}
	cnt += 2;*/

	// Copy EigenValues
	for(size_t i=0; i<(size_t)Pbs.size(); ++i) {
		hasChanged = hasChanged || (*(Pbs.data() + i) != tmp[cnt+i]);
		*(Pbs.data() + i) = tmp[cnt+i];
	}
	cnt += Pbs.size();

	if(hasChanged) {
		DAG::BaseNode::hasBeenSerialized();
	}
}

void CombinedBMNode::doProcessing() {

	Pbs = scalingNode->getProportions().getProportion(0)*BMNodes[0]->getZ();
	for(size_t iClass=1; iClass<N_CLASS; ++iClass) {
		Pbs += scalingNode->getProportions().getProportion(iClass)*BMNodes[iClass]->getZ();
	}

	/*Pbs = pNegative*BMNodes[OMEGA_NEGATIVE]->getZ() +
		  pNeutral*BMNodes[OMEGA_NEUTRAL]->getZ() +
		  pPositive*BMNodes[OMEGA_POSITIVE]->getZ();*/

}

bool CombinedBMNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void CombinedBMNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(BranchMatrixNode)){
		BranchMatrixNode *BMNode = dynamic_cast<BranchMatrixNode*>(aChild);
		BMNodes[BMNode->getOmegaClass()] = BMNode;
	} else if(childtype == typeid(MatrixScalingNode)) {
		scalingNode = dynamic_cast<MatrixScalingNode*>(aChild);
	}
}

} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
