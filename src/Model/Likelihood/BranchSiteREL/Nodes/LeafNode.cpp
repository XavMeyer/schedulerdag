//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LeafNode.cpp
 *
 * @date Nov 26, 2014
 * @author meyerx
 * @brief
 */
#include "LeafNode.h"

#include "Model/Likelihood/BranchSiteREL/Nodes/CombinedBMNode.h"
#include "Model/Likelihood/BranchSiteREL/Nodes/EdgeNode.h"

namespace DAG { class BaseNode; }
namespace MolecularEvolution { namespace DataLoader { class MSA; } }

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

LeafNode::LeafNode(const std::vector<size_t> &aSitePos, const std::string &aSpecieName,
				   const DL::MSA& aMSA, DL_Utils::Frequencies &aFrequencies, bool compress) :
						   EdgeNode(aSitePos, aFrequencies) {

	if(!compress) {
		init(aSpecieName, aMSA);
	} else {
		createSignatureCPV(aSpecieName, aMSA);
		size_t nCompressed = compressVecCPV();
		if(nCompressed == 0) {
			init(aSpecieName, aMSA);
		} else {
			initCompressed(nCompressed, aSpecieName, aMSA);
		}
	}
}

LeafNode::~LeafNode() {
}

void LeafNode::createSignatureCPV(const std::string &aSpecieName, const DL::MSA& aMSA) {

	for(size_t iCPV=0; iCPV<vecCPV.size(); ++iCPV) {
		DL::Alignment::vecCode_t cdnVals = aMSA.getValidSiteCode(aSpecieName, vecCPV[iCPV].getSitePos());
		vecCPV[iCPV].addToSignature(frequencies.size(), cdnVals);
	}

}

void LeafNode::init(const std::string &aSpecieName, const DL::MSA& aMSA) {
	Eigen::MatrixXd G;
	G.resize(H.rows(), H.cols());
	G.setZero();
	for(size_t iCPV=0; iCPV<vecCPV.size(); ++iCPV) {
		DL::Alignment::vecCode_t cdnVals = aMSA.getValidSiteCode(aSpecieName, vecCPV[iCPV].getSitePos());
		for(size_t iCod=0; iCod < cdnVals.size(); ++iCod) {
			G(cdnVals[iCod], iCPV) =  1.0;
		}
	}
	sparseG = G.sparseView();
}

void LeafNode::initCompressed(const size_t nCompressed, const std::string &aSpecieName, const DL::MSA& aMSA) {
	Eigen::MatrixXd G;
	// resize to take account of compression
	G.resize(frequencies.size(), vecCPV.size()-nCompressed);
	H.resize(frequencies.size(), vecCPV.size()-nCompressed);

#if NORM
	nrm.resize(vecCPV.size()-nCompressed);
#endif

	// Then we initialize G
	G.setZero();

	//std::cout << "Start init compressed" << std::endl;
	for(size_t iCPV=0; iCPV<vecCPV.size(); ++iCPV) {
		if(vecCPV[iCPV].isCompressed()) continue; // Skipping compressed CPV

		DL::Alignment::vecCode_t cdnVals = aMSA.getValidSiteCode(aSpecieName, vecCPV[iCPV].getSitePos());
		for(size_t iCod=0; iCod < cdnVals.size(); ++iCod) {
			G(cdnVals[iCod], vecCPV[iCPV].getIndex()) = 1.0;
		}
	}

	sparseG = G.sparseView();
	//std::cout << "end init compressed" << std::endl;
}

void LeafNode::doProcessing() {

	const Eigen::MatrixXd &Pbs = cbmNode->getPbs();
	const Eigen::VectorXd &invPi = frequencies.getInvEigen();
	Eigen::MatrixXd tmp = Pbs.selfadjointView<Eigen::Lower>();

	// Multiplication
	//H = invPi.asDiagonal() * (Pbs.selfadjointView<Eigen::Lower>() * G);
	//H = invPi.asDiagonal() * ((Pbs.selfadjointView<Eigen::Lower>()*Eigen::MatrixXd::Identity(61,61)) * sparseG);
	H = (invPi.asDiagonal() * tmp) * sparseG;

#if NORM
	for(size_t iC=0; iC<(size_t)H.cols(); ++iC){
		nrm(iC) = H.col(iC).norm();
		H.col(iC) = H.col(iC) / nrm(iC);
	}
#endif

}

bool LeafNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void LeafNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CombinedBMNode)){
		cbmNode = dynamic_cast<CombinedBMNode*>(aChild);
	}
}


} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
