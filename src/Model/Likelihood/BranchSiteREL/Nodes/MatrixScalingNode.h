//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MatrixScalingNode.h
 *
 * @date July 27, 2015
 * @author meyerx
 * @brief
 */
#ifndef MATRIXSCALINGNODE_BS_REL_H_
#define MATRIXSCALINGNODE_BS_REL_H_

#include <stddef.h>

#include "DAG/Node/Base/BaseNode.h"
#include "MatrixNode.h"
#include "Proportions.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

class MatrixScalingNode : public DAG::BaseNode {
public:
	MatrixScalingNode();
	~MatrixScalingNode();

	double getAverageScaling() const;
	const Proportions& getProportions() const;

	void setP0(const double aP0);
	void setP1(const double aP1);

private:

	double p0, p1;
	double averageScaling;
	Proportions proportions;
	std::vector<MatrixNode*> Qs;

	void doProcessing();
	bool processSignal(DAG::BaseNode* aChild);
	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);
};

} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* MATRIXSCALINGNODE_BS_REL_H_ */
