//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MatrixScalingNode.cpp
 *
 * @date July 27, 2015
 * @author meyerx
 * @brief
 */
#include "MatrixScalingNode.h"

#include "Model/Likelihood/BranchSiteREL/Nodes/Proportions.h"
#include "Model/Likelihood/BranchSiteREL/Nodes/Types.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

MatrixScalingNode::MatrixScalingNode() : DAG::BaseNode(), proportions(), Qs(N_CLASS, NULL) {

	p0 = p1 = 0.;
	averageScaling = 0.;

}

MatrixScalingNode::~MatrixScalingNode() {
}

double MatrixScalingNode::getAverageScaling() const {
	return averageScaling;
}

const Proportions& MatrixScalingNode::getProportions() const {
	return proportions;
}

void MatrixScalingNode::setP0(const double aP0) {
	if(p0 != aP0) {
		p0 = aP0;
		DAG::BaseNode::updated();
	}
}

void MatrixScalingNode::setP1(const double aP1) {
	if(p1 != aP1) {
		p1 = aP1;
		DAG::BaseNode::updated();
	}
}


void MatrixScalingNode::doProcessing() {
	proportions.set(p0, p1);

	averageScaling = 0.;
	for(size_t iClass=0; iClass<N_CLASS; ++iClass) {
		//std::cout << "Class=" << iClass << "\t proportions : " << proportions.getProportion(iClass) << "\tMatrixScaling : " << Qs[iClass]->getMatrixQScaling() << std::endl;
		averageScaling += proportions.getProportion(iClass) * Qs[iClass]->getMatrixQScaling(); // To get same result as hyphy : /3.
	}
	//std::cout << "Average scaling : " << averageScaling << std::endl;
	//getchar();
}

bool MatrixScalingNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void MatrixScalingNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(MatrixNode)){
		MatrixNode* matrixNode = dynamic_cast<MatrixNode*>(aChild);
		Qs[matrixNode->getOmegaClass()] = matrixNode;
	} else {
		std::cerr << "Child nodes should be MatrixNode only." << std::endl;
		abort();
	}
}

} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
