//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file BranchMatrixNode.cpp
 *
 * @date June 30, 2015
 * @author meyerx
 * @brief
 */
#include "BranchMatrixNode.h"

#include <assert.h>

#include "Model/Likelihood/BranchSiteREL/Nodes/MatrixNode.h"
#include "Model/Likelihood/BranchSiteREL/Nodes/MatrixScalingNode.h"
#include "Model/Likelihood/BranchSiteREL/Nodes/Types.h"

namespace MolecularEvolution { namespace DataLoader { namespace Utils { class Frequencies; } } }

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

BranchMatrixNode::BranchMatrixNode(omegaClass_t aOmegaClass, DL_Utils::Frequencies &aFrequencies) :
		BaseNode(), omegaClass(aOmegaClass), frequencies(aFrequencies), Z(frequencies.size(), frequencies.size()) {

	branchLength = 0.;
	matrixNode = NULL;
	scalingNode = NULL;
}

BranchMatrixNode::~BranchMatrixNode() {
}

omegaClass_t BranchMatrixNode::getOmegaClass() const {
	return omegaClass;
}

void BranchMatrixNode::setBranchLength(const double aBL) {
	branchLength = aBL;
	BaseNode::updated();
}

const Eigen::MatrixXd& BranchMatrixNode::getZ() const {
	return Z;
}

void BranchMatrixNode::doProcessing() {

	// Shortcuts
	const Eigen::VectorXd &D = matrixNode->getEigenValues();
	const Eigen::MatrixXd &scaledV = matrixNode->getScaledEigenVectors();

	// Process number of substitutions
	double t = branchLength;
	//t /= matrixNode->getMatrixQScaling();
	t /= scalingNode->getAverageScaling();

	// Processing Y
	Eigen::ArrayXd dt = (t/2.0) * D.array();
	dt = dt.exp();
	Eigen::VectorXd dt2 = dt.matrix();

	Eigen::MatrixXd Y = scaledV * dt2.asDiagonal();
	//Z.setZero();
	Z.triangularView<Eigen::Lower>() = Y*Y.transpose();

}

bool BranchMatrixNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void BranchMatrixNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(MatrixNode)){
		matrixNode = dynamic_cast<MatrixNode*>(aChild);
		assert(matrixNode->getOmegaClass() == omegaClass);
	} else if(childtype == typeid(MatrixScalingNode)) {
		scalingNode = dynamic_cast<MatrixScalingNode*>(aChild);
	}
}

} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
