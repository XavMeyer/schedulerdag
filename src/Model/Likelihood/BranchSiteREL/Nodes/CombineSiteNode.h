//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CombineSiteNode.h
 *
 * @date Feb 10, 2015
 * @author meyerx
 * @brief
 */
#ifndef COMBINESITENODE_BS_REL_H_
#define COMBINESITENODE_BS_REL_H_

#include <stddef.h>
#include <vector>

#include "DAG/Node/Base/BaseNode.h"
#include "DAG/Node/Base/LikelihoodNode.h"
#include "RootNode.h"
#include "Types.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

class RootNode;

class CombineSiteNode: public DAG::BaseNode, public DAG::LikelihoodNode {
public:
	CombineSiteNode(const std::vector<size_t> &aSites);
	~CombineSiteNode();

private:

	RootNode *rootNode;

	const std::vector<size_t> sites;
	std::vector<size_t> index;

	void doProcessing();
	bool processSignal(BaseNode* aChild);

	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);

	void linkLikSites(RootNode *node);

};

} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* COMBINESITENODE_BS_REL_H_ */
