//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Proportions.cpp
 *
 * @date July 27, 2015
 * @author meyerx
 * @brief
 */
#include "Proportions.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

Proportions::Proportions() : proportions(N_CLASS, 0.) {
	p0 = p1 = 0.;
}

Proportions::Proportions(const double aP0, const double aP1) {
	set(aP0, aP1);
}

Proportions::~Proportions() {
}

void Proportions::set(const double aP0, const double aP1) {
	p0 = aP0;
	p1 = aP1;

	proportions[OMEGA_NEGATIVE] = p0;
	proportions[OMEGA_NEUTRAL] = p1*(1.-p0);
	proportions[OMEGA_POSITIVE] = (1.-p0)*(1.-p1);
}

double Proportions::getP0() const {
	return p0;
}

double Proportions::getP1() const {
	return p1;
}

const std::vector<double>& Proportions::getProportions() const {
	return proportions;
}

double Proportions::getProportion(const size_t idx) const {
	return proportions[idx];
}

} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
