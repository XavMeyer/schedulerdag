//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CPVNode.cpp
 *
 * @date Mar 3, 2015
 * @author meyerx
 * @brief
 */
#include "CPVNode.h"

#include "Model/Likelihood/BranchSiteREL/Nodes/CombinedBMNode.h"
#include "Model/Likelihood/BranchSiteREL/Nodes/EdgeNode.h"
#include "Utils/MolecularEvolution/DataLoader/Utils/Frequencies.h"

namespace DAG { class BaseNode; }

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

CPVNode::CPVNode(const std::vector<size_t> &aSitePos, DL_Utils::Frequencies &aFrequencies, const bool compress) :
		        		 ParentNode(aSitePos, aFrequencies, compress) {

}

CPVNode::~CPVNode() {
}

void CPVNode::doProcessing() {

	const Eigen::MatrixXd &Pbs = cbmNode->getPbs();
	const Eigen::VectorXd &invPi = frequencies.getInvEigen();

	Eigen::MatrixXd G(H.rows(), H.cols());

	// Prepare G
	bool first = true;
	// Check block at first
	for(size_t iE=0; iE<hChildrenBlock.size(); ++iE) {
		if(iE == 0) {
			G = hChildrenBlock[iE]->getH();
			first = false;
#if NORM
			nrm = hChildrenBlock[iE]->getNorm();
#endif
		} else {
			G = G.cwiseProduct(hChildrenBlock[iE]->getH());
#if NORM
			nrm = nrm.cwiseProduct(hChildrenBlock[iE]->getNorm());
#endif
		}
	}

	// Check column wise
	for(size_t iH=0; iH<hChildrenPos.size(); ++iH) {
		for(size_t iC=0; iC<hChildrenPos[iH].size(); ++iC) {
			const Eigen::MatrixXd &tmpH = hChildrenPos[iH][iC].node->getH();
			const Eigen::VectorXd &tmpV = tmpH.col(hChildrenPos[iH][iC].cpv->getIndex());
#if NORM
			const Eigen::VectorXd &tmpNrm= hChildrenPos[iH][iC].node->getNorm();
#endif
			if(first && iC==0) { // first element : copy
				G.col(iH) = tmpV;
#if NORM
				nrm(iH) = tmpNrm(hChildrenPos[iH][iC].cpv->getIndex());
#endif
			} else { // then : cwiseProd
				G.col(iH) = G.col(iH).cwiseProduct(tmpV);
#if NORM
				nrm(iH) = nrm(iH)*tmpNrm(hChildrenPos[iH][iC].cpv->getIndex());
#endif
			}
		}
	}

	// Multiplication
	H = invPi.asDiagonal() * (Pbs.selfadjointView<Eigen::Lower>() * G);

#if NORM
	for(size_t iC=0; iC< (size_t)H.cols(); ++iC){
		double tmpNrm = H.col(iC).norm();
		nrm(iC) = nrm(iC)*tmpNrm;
		H.col(iC) = H.col(iC) / tmpNrm;
	}
#endif
}

bool CPVNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void CPVNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	const std::type_info &childtype = typeid(*aChild);
	if(childtype == typeid(CombinedBMNode)){
		cbmNode = dynamic_cast<CombinedBMNode*>(aChild);
	} else if(childtype == typeid(CPVNode)){
		CPVNode* cpvNode = dynamic_cast<CPVNode*>(aChild);
		linkChildren(cpvNode);
	} else if(childtype == typeid(LeafNode)){
		LeafNode* leafNode = dynamic_cast<LeafNode*>(aChild);
		linkChildren(leafNode);
	}
}


} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
