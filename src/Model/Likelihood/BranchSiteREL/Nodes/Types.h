//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Types.h
 *
 * @date Jun 30, 2015
 * @author meyerx
 * @brief
 */
#ifndef TYPES_BRANCHSITE_REL_H_
#define TYPES_BRANCHSITE_REL_H_

#include <stddef.h>

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

enum NUMBER_OF_CLASS {N_CLASS=3};
enum OMEGA_CLASS {OMEGA_NEGATIVE=0, OMEGA_NEUTRAL=1, OMEGA_POSITIVE=2};
typedef OMEGA_CLASS omegaClass_t;

} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* TYPES_BRANCHSITE_REL_H_  */
