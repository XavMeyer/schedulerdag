//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Proportions.h
 *
 * @date July 27, 2015
 * @author meyerx
 * @brief
 */
#ifndef PROPORTIONS_BS_REL_H_
#define PROPORTIONS_BS_REL_H_

#include <vector>

#include "Types.h"
#include "stddef.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

class Proportions {
public:
	Proportions();
	Proportions(const double aP0, const double aP1);
	~Proportions();

	void set(const double aP0, const double aP1);
	double getP0() const;
	double getP1() const;
	double getProportion(const size_t idx) const;
	const std::vector<double>& getProportions() const;

private:
	double p0, p1;
	std::vector<double> proportions;


};

} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* PROPORTIONS_BS_REL_H_ */
