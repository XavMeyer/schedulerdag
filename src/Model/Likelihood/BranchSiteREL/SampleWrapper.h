//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file SampleWrapper.h
 *
 * @date Jun 29, 2015
 * @author meyerx
 * @brief
 */
#ifndef SAMPLEWRAPPER_BS_REL_H_
#define SAMPLEWRAPPER_BS_REL_H_

#include <stddef.h>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "Utils/MolecularEvolution/MatrixFactory/MatrixFactory.h"
#include "Utils/MolecularEvolution/MatrixFactory/Operations/Instructions/MultStore.h"

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

namespace MU = ::MolecularEvolution::MatrixUtils;

class BranchSiteRELik;

class SampleWrapper {
public:
	friend class BranchSiteRELik;
public:

	SampleWrapper(const MU::nuclModel_t aNuclModel, const size_t aN_BRANCH, const std::vector<double> &sample);
	~SampleWrapper();

	std::string toString() const;

private:

	static const size_t N_KAPPA, N_THETA, K, N_PARAM_PER_BRANCH;

	struct BranchSiteParams {
	public:
		double t, wNeg, wNeutral, wPos, p0, p1;
	};

	const size_t N_BRANCH;
	double kappa;
	std::vector<double> thetas;
	std::vector<BranchSiteParams> branchParams;

};

} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* SAMPLEWRAPPER_BS_REL_H_ */
