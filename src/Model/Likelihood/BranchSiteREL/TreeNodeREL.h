//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeNodeREL.h
 *
 * @date Jun 29, 2015
 * @author meyerx
 * @brief
 */
#ifndef TREENODEREL_H_
#define TREENODEREL_H_

#include <stddef.h>
#include <sstream>
#include <vector>

#include "Model/Likelihood/BranchSiteREL/Nodes/Types.h"
#include "Nodes/IncNodes.h"
#include "Utils/MolecularEvolution/DataLoader/Tree/TreeNode.h"

namespace MolecularEvolution { namespace DataLoader { class TreeNode; } }

namespace StatisticalModel {
namespace Likelihood {
namespace BranchSiteREL {

class TreeNodeREL;
class BranchMatrixNode;
class CombinedBMNode;
class MatrixNode;
class MatrixScalingNode;

typedef std::vector<TreeNodeREL*> TreeNodeREL_children;

class TreeNodeREL {
public:
	TreeNodeREL(const MolecularEvolution::DataLoader::TreeNode &aNewickNode);
	~TreeNodeREL();

	void addChild(TreeNodeREL *child);

	TreeNodeREL* getChild(size_t id);
	const TreeNodeREL_children& getChildren() const;
	const std::string& getName() const;

	void setLeaf(const bool aLeaf);
	bool isLeaf() const;

	void setOmegaNegative(const double aWNegative);
	void setOmegaNeutral(const double aWNeutral);
	void setOmegaPositive(const double aWPositive);
	void setKappa(const double aKappa);
	void setThetas(const std::vector<double> &aThetas);
	void setBranchLength(const double aBL);
	void setP0(const double aP0);
	void setP1(const double aP1);

	const std::string toString() const;

	void deleteChildren();

	std::string buildNewick() const;
	void addNodeToNewick(std::string &nwk) const;

	void addMatrixNode(MatrixNode *node);
	MatrixNode* getMatrixNode(omegaClass_t aOmegaClass);

	void addBranchMatrixNode(BranchMatrixNode *node);
	BranchMatrixNode* getBranchMatrixNode(omegaClass_t aOmegaClass);

	void setCombineBranchMatrixNode(CombinedBMNode *node);
	CombinedBMNode* getCombineBranchMatrixNode();

	void setMatrixScalingNode(MatrixScalingNode *node);
	MatrixScalingNode* getMatrixScalingNode();

private:
	/* Default data */
	size_t id;
	bool leaf;
	std::string name;
	double branchLength;
	TreeNodeREL_children children;

	std::vector<MatrixNode*> matrixNodes;
	std::vector<BranchMatrixNode*> bmNodes;
	MatrixScalingNode* scalingNode;
	CombinedBMNode* cbmNode;

};

} /* namespace BranchSiteREL */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* TREENODEREL_H_ */
