//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GraphLik.h
 *
 * @date Apr 18, 2016
 * @author meyerx
 * @brief
 */
#ifndef GRAPHLIK_H_
#define GRAPHLIK_H_

#include <Model/Likelihood/LikelihoodInterface.h>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/topological_sort.hpp>
#include <stddef.h>

#include "DAG/Scheduler/BaseScheduler.h"
#include "DAG/Scheduler/Sequential/SequentialScheduler.h"
#include "DAG/Scheduler/SharedMemory/Dynamic/ThreadSafeScheduler.h"
#include "GraphNode.h"
#include <boost/graph/properties.hpp>
#include <boost/pending/property.hpp>

namespace Sampler { class Sample; }
namespace boost { struct dynamic_properties; }



namespace StatisticalModel {
namespace Likelihood {
namespace Graph {

class GraphNode;

class GraphLik: public LikelihoodInterface {
public:
	typedef boost::shared_ptr< GraphLik > sharedPtr_t;

	enum nodeTime_t {UNIT=0, NORMAL=1, EXPONENTIAL=2, BIMODAL=3, COSTLY=4, VERY_COSTLY=5};

public:
	GraphLik(const nodeTime_t aNodeTime, const std::string &aInFile);
	~GraphLik();

	double processLikelihood(const Sampler::Sample &sample);
	std::string getName(char sep=' ') const;

	size_t stateSize() const;
	void setStateLH(const char* aState);
	void getStateLH(char* aState) const;

	double update(const vector<size_t>& pIndices, const Sample& sample);

	DAG::Scheduler::BaseScheduler* getScheduler() const;
	std::vector<DAG::BaseNode*> defineDAGNodeToCompute(const Sample& sample);

	double getMaxTime() const;
	double getTotalTime() const;
	double getExecTime() const;

	std::vector<size_t> getLeavesID() const;
	size_t getNbNodes() const;

	void setNodeRunning(bool doRun);

private:

	const nodeTime_t nodeTimeType;
	double executionTime;

	// Vertex properties
	typedef boost::property < boost::vertex_name_t, std::string,
			boost::property < boost::vertex_color_t, float > > vertex_p;
	// Edge properties
	typedef boost::property < boost::edge_weight_t, double > edge_p;
	// Graph properties
	typedef boost::property < boost::graph_name_t, std::string > graph_p;
	// adjacency_list-based type
	typedef boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS,
	  vertex_p, edge_p, graph_p > graph_t;


	const std::string inFileName;

	/* DAG Scheduler */
	GraphNode *rootDAG;
	std::vector<GraphNode *> gNodes;

	DAG::Scheduler::BaseScheduler *scheduler;

	void initDAG();
	void initGraph(graph_t &graph, boost::dynamic_properties &dp);

};

} /* namespace Graph */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* GRAPHLIK_H_ */
