//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GraphLik.cpp
 *
 * @date Apr 18, 2016
 * @author meyerx
 * @brief
 */
#include "GraphLik.h"

#include "Parallel/Parallel.h" // IWYU pragma: keep

#include <assert.h>

#include "Model/Likelihood/Graph/GraphNode.h"
#include "Model/Likelihood/LikelihoodInterface.h"
#include "Parallel/Manager/../RNG/RNG.h"
#include "Parallel/Manager/MpiManager.h"
#include <boost/graph/graph_traits.hpp>
#include <boost/property_map/dynamic_property_map.hpp>
#include "new"

namespace Sampler { class Sample; }

namespace StatisticalModel {
namespace Likelihood {
namespace Graph {

GraphLik::GraphLik(const nodeTime_t aNodeTime, const std::string &aInFile) :
		LikelihoodInterface(), nodeTimeType(aNodeTime), inFileName(aInFile) {
	isLogLH = true;
	isUpdatableLH = true;

	/*if(Parallel::mpiMgr().isMainProcessor()) {
		if(nodeTimeType == UNIT) {
			std::cout << "UNIT TIME" << std::endl;
		} else if(nodeTimeType == NORMAL) {
			std::cout << "NORMAL TIME" << std::endl;
		} else if(nodeTimeType == EXPONENTIAL) {
			std::cout << "EXPONENTIAL TIME" << std::endl;
		} else if(nodeTimeType == BIMODAL) {
			std::cout << "BIMODAL TIME" << std::endl;
		} else if(nodeTimeType == COSTLY) {
			std::cout << "COSTLY TIME" << std::endl;
		} else if(nodeTimeType == VERY_COSTLY) {
			std::cout << "VERY COSTLY TIME" << std::endl;
		}
	}*/

	initDAG();

	scheduler = new DAG::Scheduler::Sequential::SequentialScheduler(rootDAG);

	executionTime = 0.;
}

GraphLik::~GraphLik() {

	rootDAG->deleteChildren();
	delete rootDAG;
}



void GraphLik::initDAG() {

	bool doRun = true;

	// Construct an empty graph and prepare the dynamic_property_maps.
	graph_t graph(0);
	boost::dynamic_properties dp;
	// Init graph
	initGraph(graph, dp);

	// Create DAG
    typedef boost::property_map<graph_t, boost::vertex_index_t>::type IndexMap;
    IndexMap index = get(boost::vertex_index, graph);

    // Create node vector
    gNodes.resize(graph.m_vertices.size(), NULL);
    //std::cout << graph.m_vertices.size() << std::endl;

    double minTime=std::numeric_limits<double>::max(), maxTime=0, meanTime=0;

    // Create vertex
    size_t cnt = 0;
	typedef boost::graph_traits<graph_t>::vertex_iterator vertex_iter;
	std::pair<vertex_iter, vertex_iter> vp;
	for (vp = vertices(graph); vp.first != vp.second; ++vp.first) {
		//std::cout << "Vertex : " << index[*vp.first] << "\t" << get(boost::vertex_name, graph)[*vp.first];
		//std::cout << std::endl;
		// Create DAG NODE
		//gNodes[index[*vp.first]] = new GraphNode(index[*vp.first], executionTime, doRun, myTime);

		double time = 0.;
		if(nodeTimeType == UNIT) {
			time = 1.0;
		} else if(nodeTimeType == NORMAL) {
			time = fabs(10. + Parallel::mpiMgr().getPRNG()->genGaussian(3));
		} else if(nodeTimeType == EXPONENTIAL) {
			time = 1.0 + Parallel::mpiMgr().getPRNG()->genExponential(50);
		} else if(nodeTimeType == BIMODAL) {
			if(Parallel::mpiMgr().getPRNG()->genUniformDbl() > 0.5) {
				time = fabs(2. + Parallel::mpiMgr().getPRNG()->genGaussian(0.1));
			} else {
				time = fabs(25. + Parallel::mpiMgr().getPRNG()->genGaussian(2.));
			}
		} else if(nodeTimeType == COSTLY) {
			if(Parallel::mpiMgr().getPRNG()->genUniformDbl() < 0.05) {
				time = fabs(150. + Parallel::mpiMgr().getPRNG()->genGaussian(5.));
			} else {
				time = fabs(1. + Parallel::mpiMgr().getPRNG()->genGaussian(0.01));
			}
		} else if(nodeTimeType == VERY_COSTLY) {
			if(Parallel::mpiMgr().getPRNG()->genUniformDbl() < 0.05) {
				time = fabs(500. + Parallel::mpiMgr().getPRNG()->genGaussian(5.));
			} else {
				time = fabs(1. + Parallel::mpiMgr().getPRNG()->genGaussian(0.01));
			}
		} else {
			assert(false);
		}
		minTime = std::min(minTime, time);
		maxTime = std::max(maxTime, time);
		meanTime += time;
		cnt++;

		if(get(boost::vertex_name, graph)[*vp.first] == "source") {
			gNodes[graph.m_vertices.size()-1] = new GraphNode(graph.m_vertices.size()-1, executionTime, doRun, time);
			rootDAG = gNodes[graph.m_vertices.size()-1];
			//std::cout << "Source :  " << graph.m_vertices.size()-1 << std::endl;
		} else {
			size_t vInd = atoi(get(boost::vertex_name, graph)[*vp.first].c_str());
			gNodes[vInd] = new GraphNode(vInd, executionTime, doRun, time);
			//std::cout << "Vertex : " <<  vInd << std::endl;
		}
	}
	// if(Parallel::mpiMgr().isMainProcessor()) std::cout << "Node times ==> " << minTime << "  || " << meanTime/cnt << " || " << maxTime << std::endl;

    // Create edges
	boost::graph_traits<graph_t>::edge_iterator ei, ei_end;
	for (tie(ei, ei_end) = edges(graph); ei != ei_end; ++ei) {
		size_t iFrom=0, iTo=0;
		if(get(boost::vertex_name, graph)[source(*ei, graph)] != "source") {
			iFrom = atoi(get(boost::vertex_name, graph)[source(*ei, graph)].c_str());//index[source(*ei, graph)];
		} else {
			//std::cout << "[S]";
			iFrom = graph.m_vertices.size()-1;
		}
		if(get(boost::vertex_name, graph)[target(*ei, graph)] != "source") {
			iTo = atoi(get(boost::vertex_name, graph)[target(*ei, graph)].c_str());//index[target(*ei, graph)];
		} else {
			//std::cout << "[S]";
			iTo =  graph.m_vertices.size()-1;
		}
		//std::cout << "Edge : " << get(boost::vertex_name, graph)[source(*ei, graph)] << " -- " << get(boost::vertex_name, graph)[target(*ei, graph)];
		//std::cout << "Edge : " << iFrom << " -- " << iTo;
		//std::cout << std::endl;
		// Add relation
		gNodes[iFrom]->addChild(gNodes[iTo]);
	}

	//std::cout << rootDAG->subtreeToString() << std::endl;

}

void GraphLik::initGraph(graph_t &graph, boost::dynamic_properties &dp) {

	boost::property_map<graph_t, boost::vertex_name_t>::type name =
	  get(boost::vertex_name, graph);
	dp.property("node_id",name);

	boost::property_map<graph_t, boost::vertex_color_t>::type mass =
	  get(boost::vertex_color, graph);
	dp.property("mass", mass);

	boost::property_map<graph_t, boost::edge_weight_t>::type weight =
	  get(boost::edge_weight, graph);
	dp.property("weight", weight);

	// Use ref_property_map to turn a graph property into a property map
	boost::ref_property_map<graph_t*,std::string>
	  gname(get_property(graph,boost::graph_name));
	dp.property("name", gname);

	// Sample graph as an std::istream;
	/*std::istringstream
	  gvgraph("digraph { graph [name=\"graphname\"]  a  c e [mass = 6.66] }");*/

	std::ifstream iFile(inFileName.c_str());
	read_graphviz(iFile,graph,dp,"node_id");
}

double GraphLik::processLikelihood(const Sampler::Sample &sample) {

	executionTime = 0.;

	// Update the values
	//SampleWrapper sw(nuclModel, branchPtr.size(), sample.getDblValues());
	//setSample(sw);

	// Reset the DAG (could use partial result instead
	scheduler->resetDAG();

	// Process the tree
	scheduler->process(false);

	// return the likelihood
	return executionTime;
}

std::string GraphLik::getName(char sep) const {
	return "Graph";
}


size_t GraphLik::stateSize() const {
	return 0;
}
void GraphLik::setStateLH(const char* aState) {

}

void GraphLik::getStateLH(char* aState) const {

}

double GraphLik::update(const vector<size_t>& pIndices, const Sample& sample) {

	executionTime = 0.;

	//rootDAG->resetInternals();
	for(size_t iN=0; iN<gNodes.size(); ++iN) {
		gNodes[iN]->resetInternals();
	}


	for(size_t iP=0; iP<pIndices.size(); ++iP) {
		gNodes[pIndices[iP]]->doUpdate();
	}

	// Reset the DAG
	if(!scheduler->resetPartial()) {
		std::cerr << "Reset partial did not found any change for parameters : " << std::endl;
		for(size_t i=0; i< pIndices.size(); ++i) {
			std::cerr << "[" << pIndices[i] << "] = "  << sample.getDoubleParameter(i) << "\t";
		}
		std::cerr << std::endl;
	}

	// Process the tree
	scheduler->process(false);

	return executionTime;
}


DAG::Scheduler::BaseScheduler* GraphLik::getScheduler() const {
	return scheduler;
}
std::vector<DAG::BaseNode*> GraphLik::defineDAGNodeToCompute(const Sample& sample) {
	std::vector<DAG::BaseNode*> mustBeProcessed;

	for(size_t i=0; i<gNodes.size(); ++i) {
		if(sample.getDblValues()[i] > 0) {
			gNodes[i]->doUpdate();
		}
	}

	// Find the nodes to process
	scheduler->findNodeToProcess(rootDAG, mustBeProcessed);
	// Reinit the dag
	scheduler->resetPartial();
	// Fake the computations
	bool doFakeCompute = true;
	scheduler->process(doFakeCompute);

	return mustBeProcessed;
}

double GraphLik::getMaxTime() const {
	return rootDAG->getMaxTime();
}

double GraphLik::getTotalTime() const {
	return rootDAG->getSumTime();
}

double GraphLik::getExecTime() const {
	return executionTime;
}


std::vector<size_t> GraphLik::getLeavesID() const {

	std::vector<size_t> leavesID;

	for(size_t iN=0; iN<gNodes.size(); ++iN) {
		if(gNodes[iN]->isLeaf()) {
			leavesID.push_back(iN);
		}
	}

	return leavesID;
}

size_t GraphLik::getNbNodes() const {
	return gNodes.size();
}

void GraphLik::setNodeRunning(bool doRun) {
	for(size_t i=0; i<gNodes.size(); ++i) {
		gNodes[i]->setDoRun(doRun);
	}
}


} /* namespace Graph */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
