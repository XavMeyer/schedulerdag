//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GraphNode.h
 *
 * @date Apr 18, 2016
 * @author meyerx
 * @brief
 */
#ifndef GRAPHNODE_H_
#define GRAPHNODE_H_

#include <DAG/Node/Base/BaseNode.h>
#include <stddef.h>
#include <time.h>

namespace StatisticalModel {
namespace Likelihood {
namespace Graph {

class GraphNode: public DAG::BaseNode {
public:
	GraphNode(size_t aID, double &aExecutionTime, bool aDoRun, double aTime);
	~GraphNode();

	void doUpdate();

	double getTime() const;
	double getMaxTime() const;
	double getSumTime() const;

	void resetInternals();

	void setDoRun(bool aDoRun);

private:

	size_t ID;
	bool doRun;
	double time, sumTime, maxTime;
	double &executionTime;

	std::vector<GraphNode*> childGNode;

	void doProcessing();
	bool processSignal(BaseNode* aChild);
	void doAddChild(const size_t rowId, DAG::BaseNode* aChild);

};

} /* namespace Graph */
} /* namespace Likelihood */
} /* namespace StatisticalModel */

#endif /* GRAPHNODE_H_ */
