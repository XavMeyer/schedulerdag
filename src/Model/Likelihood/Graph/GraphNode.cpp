//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file GraphNode.cpp
 *
 * @date Apr 18, 2016
 * @author meyerx
 * @brief
 */
#include <Model/Likelihood/Graph/GraphNode.h>

#include "mpi.h"

namespace DAG { class BaseNode; }

namespace StatisticalModel {
namespace Likelihood {
namespace Graph {

GraphNode::GraphNode(size_t aID, double &aExecutionTime, bool aDoRun, double aTime) :
		ID(aID), doRun(aDoRun), time(aTime), executionTime(aExecutionTime) {
	sumTime = maxTime = 0;
	for(size_t i=0; i<100000; ++i) {
		accTime(time);
	}
}

GraphNode::~GraphNode() {
}

void GraphNode::doUpdate() {
	BaseNode::updated();
}

void GraphNode::resetInternals() {
	maxTime = 0;
	sumTime = 0;
	/*for(size_t iC=0; iC<childGNode.size(); ++iC) {
		childGNode[iC]->resetInternals();
	}*/
}

void GraphNode::setDoRun(bool aDoRun) {
	doRun = aDoRun;
}

void GraphNode::doProcessing() {

	//std::cout << "Processing node :: " << ID << " -- with execution time : " << time << std::endl;

	if(doRun) {
		double start = MPI_Wtime();
		double now = MPI_Wtime();
		double done = now + time;
		while(now < done) {
			now = MPI_Wtime();
		}
		std::cout << "compute time : " << MPI_Wtime() - start << std::endl;
	}

	maxTime = time;
	sumTime = time;
	for(size_t iC=0; iC<childGNode.size(); ++iC) {
		maxTime = std::max(childGNode[iC]->getMaxTime(), maxTime);
		sumTime += childGNode[iC]->getSumTime();
	}

	executionTime += time;

}

double GraphNode::getTime() const {
	return time;
}

double GraphNode::getMaxTime() const {
	return maxTime;
}

double GraphNode::getSumTime() const {
	return sumTime;
}

bool GraphNode::processSignal(DAG::BaseNode* aChild) {
	return true;
}

void GraphNode::doAddChild(const size_t rowId, DAG::BaseNode* aChild) {
	childGNode.push_back(static_cast<GraphNode*>(aChild));
}


} /* namespace Graph */
} /* namespace Likelihood */
} /* namespace StatisticalModel */
