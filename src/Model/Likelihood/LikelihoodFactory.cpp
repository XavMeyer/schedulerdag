//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file LikelilhoodFactory.cpp
 *
 * @date May 13, 2015
 * @author meyerx
 * @brief
 */
#include "LikelihoodFactory.h"

#include "Model/Likelihood/LikelihoodInterface.h"

namespace MolecularEvolution { namespace DataLoader { class NewickParser; } }

namespace StatisticalModel {
namespace Likelihood {

LikelihoodFactory::LikelihoodFactory() {
}

LikelihoodFactory::~LikelihoodFactory() {
}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createSelPositiveBase(const bool isH1, const size_t aFGBranch, const size_t aNThread,
		 const std::string &aFileAlign, const std::string &aFileTree) {
	return LikelihoodInterface::sharedPtr_t (new PositiveSelection::Base::SelPositive(isH1, aFGBranch, aNThread, aFileAlign, aFileTree));
}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createSelPositiveLight(const bool isH1, const bool aUseCompression, const long int aFGBranch,
		const size_t aNThread, const std::string &aFileAlign, const std::string &aFileTree){
	return LikelihoodInterface::sharedPtr_t (new PositiveSelection::Light::SelPositive(isH1, aUseCompression, aFGBranch, aNThread, aFileAlign, aFileTree));
}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createSelPositiveLight(const bool isH1, const bool aUseCompression, const long int aFGBranch, const size_t aNThread,
 													    				   const PositiveSelection::Light::nuclModel_t aNuclModel, const PositiveSelection::Light::codonFreq_t aCFreqType,
 													    				   const std::string &aFileAlign, const std::string &aFileTree, const PositiveSelection::Light::scalingType_t scalingType) {
	return LikelihoodInterface::sharedPtr_t (new PositiveSelection::Light::SelPositive(isH1, aUseCompression, scalingType, aFGBranch, aNThread, aNuclModel, aCFreqType, aFileAlign, aFileTree));
}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createBranchSiteREL(const bool aUseCompression, const int aIBranch, const size_t aNThread,
		const BranchSiteREL::nuclModel_t aNuclModel, const BranchSiteREL::codonFreq_t aCFreqType,
		const std::string &aFileAlign, const std::string &aFileTree) {
	return LikelihoodInterface::sharedPtr_t (new BranchSiteREL::BranchSiteRELik(aUseCompression, aIBranch, aNThread, aNuclModel, aCFreqType, aFileAlign, aFileTree));
}

LikelihoodInterface::sharedPtr_t LikelihoodFactory::createTreeInference(const TreeInference::scalingType_t aScalingType,
															const TreeInference::modelType_t aModelType, const size_t aNThread,
		   	   	   	   	   	   	   	   	   	   	   	   	    const TreeInference::nuclModel_t aNuclModel, const TreeInference::codonFreq_t aCFreqType,
		   	   	   	   	   	   	   	   	   	   	   	   	    MolecularEvolution::DataLoader::NewickParser *aNP, const std::string &aFileAlign,
		   	   	   	   	   	   	   	   	   	   	   	   	    TreeInference::Base::treeUpdateStrategy_t aStrat) {
	return LikelihoodInterface::sharedPtr_t (new TreeInference::Base(aScalingType, aModelType, aNThread, aNuclModel, aCFreqType, aNP, aFileAlign, aStrat));
}


} /* namespace Likelihood */
} /* namespace StatisticalModel */
