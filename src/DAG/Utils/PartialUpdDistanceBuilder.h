//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PartialUpdDistanceBuilder.h
 *
 * @date Jan 17, 2016
 * @author meyerx
 * @brief
 */
#ifndef PARTIALUPDDISTANCEBUILDER_H_
#define PARTIALUPDDISTANCEBUILDER_H_

#include <assert.h>
#include <stddef.h>
#include <algorithm>
#include <map>
#include <set>

#include "DAG/Node/Base/BaseNode.h"
#include "DAG/Scheduler/BaseScheduler.h"
#include "Eigen/Core"
#include "Eigen/Dense"
#include "Model/Likelihood/Likelihoods.h"
#include "Model/Parameter/Parameters.h"
#include "Model/Prior/PriorInterface.h"
#include "Sampler/Samples/Sample.h"

namespace StatisticalModel { class Parameters; }

namespace DAG {
namespace Tools {

namespace SM = ::StatisticalModel;
namespace SM_Lik = ::StatisticalModel::Likelihood;

class PartialUpdDistanceBuilder {
public:
	//! The distance is a measure of graph difference / similarity depending on :
	//! 1) DAG metrics : A) number of vertex; B) computational time of vertices
	enum distanceType_t {NODE_METRIC=0, TIME_METRIC=1};
	//! 2) The distance between two subdag
	//! 2)->A) The union of the DAG vertices that have to be computed (ML) -> Total computing time for a ML gradient
	//! 2)->B) The difference of the DAG vertices that have to be computed (MCMC) -> Added computing time for a move
	enum setOperator_t {SUBDAG_UNION=0, SUBDAG_SYMMETRIC_DIFFERENCE=1};

public:
	PartialUpdDistanceBuilder(distanceType_t aDistanceType, setOperator_t aSetOperator,
			const std::vector<size_t> &aActiveParamsIdx,
			const SM::Parameters &aParams, const Sampler::Sample &aSample,
			SM::Likelihood::LikelihoodInterface* aPtrLik);
	~PartialUpdDistanceBuilder();

	void writeEdgeWeights(const std::string &filename);

	float getDefaultTourLength() const;
	float getRandomTourLength() const;

	const std::vector<size_t>& getActiveParams() const;
	const Eigen::MatrixXf& getEdgeMatrix() const;
	Eigen::MatrixXf getEdgeMatrixTSP();

private:

	static const size_t DEFAULT_SEED, NUMBER_ITERATION;

	const distanceType_t DISTANCE_TYPE;
	const setOperator_t SET_OPERATOR;
	std::vector<size_t> activeParams;
	const SM::Parameters &params;
	const Sampler::Sample baseSample;
	SM::Likelihood::LikelihoodInterface *ptrLik;

	typedef std::map<size_t, DAG::BaseNode*> mapNodeDAG_t;
	mapNodeDAG_t mapNodeDAG;

	typedef std::vector< std::set<size_t> > partialUpdateCost_t;
	partialUpdateCost_t partialUpdateCost;

	float minDistance;
	Eigen::MatrixXf nodeDistance;

	void checkActiveParameters();
	void definePartialUpdatesCost();

	void computeDistance();
	float computeNodeDistance(size_t iParam1, size_t iParam2);
	float computeTimeDistance(size_t iParam1, size_t iParam2);
};

} /* namespace Tools */
} /* namespace DAG */

#endif /* PARTIALUPDDISTANCEBUILDER_H_ */
