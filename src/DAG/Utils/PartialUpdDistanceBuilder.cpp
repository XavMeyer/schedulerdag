//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file PartialUpdDistanceBuilder.cpp
 *
 * @date Jan 17, 2016
 * @author meyerx
 * @brief
 */
#include "PartialUpdDistanceBuilder.h"

#include <assert.h>

namespace DAG {
namespace Tools {

const size_t PartialUpdDistanceBuilder::DEFAULT_SEED = 1234;
const size_t PartialUpdDistanceBuilder::NUMBER_ITERATION = 20;

PartialUpdDistanceBuilder::PartialUpdDistanceBuilder(
		distanceType_t aDistanceType, setOperator_t aSetOperator,
		const std::vector<size_t> &aActiveParamsIdx, const SM::Parameters &aParams,
		const Sampler::Sample &aSample, SM::Likelihood::LikelihoodInterface *aPtrLik) :
			DISTANCE_TYPE(aDistanceType), SET_OPERATOR(aSetOperator),
			activeParams(aActiveParamsIdx), params(aParams),
			baseSample(aSample), ptrLik(aPtrLik){

	assert(!activeParams.empty());
	assert(ptrLik != NULL);

	minDistance = std::numeric_limits<float>::infinity();

	// Check parameters
	checkActiveParameters();
	// Define the node implicated in partial update
	definePartialUpdatesCost();

	// Compute some insteration to get "time" information
	/*if(DISTANCE_TYPE == TIME_DISTANCE) {
		Utils::Maximizer::LikelihoodMaximizer likMax(DEFAULT_SEED, params, baseSample, ptrLik);
		likMax.maximize(NUMBER_ITERATION);
	}*/

	computeDistance();

}

PartialUpdDistanceBuilder::~PartialUpdDistanceBuilder() {
}

void PartialUpdDistanceBuilder::checkActiveParameters() {
	for(size_t i=0; i<activeParams.size(); ++i) {
		assert(params.getBoundMin(activeParams[i]) < params.getBoundMax(activeParams[i]));
	}
}

void PartialUpdDistanceBuilder::definePartialUpdatesCost() {

	partialUpdateCost.resize(activeParams.size());
	const std::list<DAG::BaseNode*> &nodes = ptrLik->getScheduler()->getTopologicalList();

	// Keep track of nodes
	for(std::list<DAG::BaseNode*>::const_iterator it=nodes.begin(); it != nodes.end(); ++it) {
		mapNodeDAG[(*it)->getId()] = (*it);
	}

	// If the sample has not changed we refresh the likelihood state
	if(!baseSample.hasChanged()) {
		std::vector<size_t> dummyParams;
		ptrLik->update(dummyParams, baseSample);
	} else {
		// Reset state
		ptrLik->defineDAGNodeToCompute(baseSample);
	}

	// For each parameters define partial update cost
	for(size_t iP=0; iP < activeParams.size(); ++iP) {

		// Change one parameter
		Sampler::Sample newSample(baseSample);
		float val = baseSample.getDoubleParameter(activeParams[iP]);
		newSample.setDoubleParameter(activeParams[iP], val+1.0);
		// Get node to compute
		std::vector<DAG::BaseNode*> costPU = ptrLik->defineDAGNodeToCompute(newSample);
		// Add them to partial Updates cost
		for(size_t iN=0; iN<costPU.size(); ++iN) {
			partialUpdateCost[iP].insert(costPU[iN]->getId());
		}
		// Reset state
		ptrLik->defineDAGNodeToCompute(baseSample);

	}


}

void PartialUpdDistanceBuilder::computeDistance() {
	const size_t N = activeParams.size();

	nodeDistance.resize(N, N);
	nodeDistance.setZero();

	for(size_t iR=0; iR<N; ++iR) {
		for(size_t iC=0; iC<iR; ++iC) {

			float dist = 0.;
			if(DISTANCE_TYPE == NODE_METRIC) {
				dist = computeNodeDistance(iR, iC);
			} else if (DISTANCE_TYPE == TIME_METRIC) {
				dist = computeTimeDistance(iR, iC);
			} else {
				std::cerr << "[Error] in Eigen::MatrixXf PartialUpdDistanceBuilder::computeDistance();" << std::endl;
				std::cerr << "Node distance method not yet implemented." << std::endl;
				assert(false);
			}

			nodeDistance(iR, iC) = dist;
			nodeDistance(iC, iR) = dist;
			if(dist!=0.) {
				minDistance = std::min(dist, minDistance);
			}

		}
	}
}

void PartialUpdDistanceBuilder::writeEdgeWeights(const std::string &filename) {
	const size_t N = activeParams.size();

	std::ofstream oFile(filename.c_str());

	oFile << "NAME: " << filename << std::endl;
	oFile << "TYPE: TSP" << std::endl;
	oFile << "DIMENSION:" << N << std::endl;
	oFile << "EDGE_WEIGHT_TYPE: EXPLICIT" << std::endl;
	oFile << "EDGE_WEIGHT_FORMAT: FULL_MATRIX" << std::endl;
	oFile << "EDGE_WEIGHT_SECTION:" << std::endl;

	for(size_t iR=0; iR<N; ++iR) {
		for(size_t iC=0; iC<N; ++iC) {
			if(DISTANCE_TYPE == NODE_METRIC) {
				oFile << nodeDistance(iR, iC) << "\t";
			} else if (DISTANCE_TYPE == TIME_METRIC) {
				oFile << (size_t)((nodeDistance(iR, iC)/minDistance)*1.e3) << "\t";
			}
		}
		oFile << std::endl;
	}
	oFile << "EOF" << std::endl;


	/*for(size_t iR=0; iR<N; ++iR) {
		for(size_t iC=iR+1; iC<N; ++iC) {
			if(DISTANCE_TYPE == NODE_DISTANCE) {
				oFile << iR << "\t" << iC << "\t" << nodeDistance(iR, iC) << std::endl;
			} else if (DISTANCE_TYPE == TIME_DISTANCE) {
				oFile << iR << "\t" << iC << "\t" << (size_t)((nodeDistance(iR, iC)/minDistance)*1.e3) << std::endl;
			}
		}
	}*/
}

float PartialUpdDistanceBuilder::getDefaultTourLength() const {
	float tour = 0.;
	const size_t N = activeParams.size();
	for(size_t iR=0; iR<N; ++iR) {
		size_t iNext = (iR+1)%N;
		if(DISTANCE_TYPE == NODE_METRIC) {
			//std::cout <<  iR << " -- " << iNext << " -- " << nodeDistance(iR, iNext) << std::endl;
			tour += nodeDistance(iR, iNext);
		} else if (DISTANCE_TYPE == TIME_METRIC) {
			tour += nodeDistance(iR, iNext);
			// tour += (size_t)((nodeDistance(iR, iNext)/minDistance)*1.e3); // FOR LK
		}
	}

	return tour;
}

float PartialUpdDistanceBuilder::getRandomTourLength() const {
	const size_t N = activeParams.size();

	std::vector<size_t> order;
	for(size_t iR=0; iR<N; ++iR) {
		order.push_back(iR);
	}
	Parallel::mpiMgr().getPRNG()->randomShuffle(order);

	float tour = 0.;
	for(size_t iR=0; iR<N; ++iR) {
		size_t iCur = order[iR];
		size_t iNext = order[(iCur+1)%N];
		if(DISTANCE_TYPE == NODE_METRIC) {
			tour += nodeDistance(iCur, iNext);
		} else if (DISTANCE_TYPE == TIME_METRIC) {
			tour += (size_t)((nodeDistance(iCur, iNext)/minDistance)*1.e3);
		}
	}

	return tour;
}

const std::vector<size_t>& PartialUpdDistanceBuilder::getActiveParams() const {
	return activeParams;
}

const Eigen::MatrixXf& PartialUpdDistanceBuilder::getEdgeMatrix() const {
	return nodeDistance;
}

Eigen::MatrixXf PartialUpdDistanceBuilder::getEdgeMatrixTSP() {
	const size_t N = activeParams.size();
	Eigen::MatrixXf tspEM;

	tspEM.resize(N+1, N+1);
	tspEM.setZero();
	tspEM.block(1,1,N,N) = nodeDistance;

	for(size_t iR=1; iR<N+1; ++iR) {
		float dist = 0.;
		if(DISTANCE_TYPE == NODE_METRIC) {
			dist = partialUpdateCost[iR-1].size();
		} else if (DISTANCE_TYPE == TIME_METRIC) {
			const std::set<size_t> &mySet = partialUpdateCost[iR-1];
			for(std::set<size_t>::const_iterator it=mySet.begin(); it != mySet.end(); ++it) {
				dist += mapNodeDAG[(*it)]->getAverageTime();
			}
		} else {
			std::cerr << "[Error] in Eigen::MatrixXf PartialUpdDistanceBuilder::computeDistance();" << std::endl;
			std::cerr << "Node distance method not yet implemented." << std::endl;
			assert(false);
		}
		tspEM(0,iR) = dist;
		tspEM(iR, 0) = dist;
	}

	return tspEM;
}

float PartialUpdDistanceBuilder::computeNodeDistance(size_t iParam1, size_t iParam2) {

	// Get sets
	std::set<size_t> &set1 = partialUpdateCost[iParam1];
	std::set<size_t> &set2 = partialUpdateCost[iParam2];

	// Create union of both
	std::set<size_t> setU;
	if(SET_OPERATOR == SUBDAG_UNION) {
		std::set_union(set1.begin(), set1.end(), set2.begin(), set2.end(), std::inserter(setU, setU.begin()));
	} else if(SET_OPERATOR == SUBDAG_SYMMETRIC_DIFFERENCE) {
		std::set_symmetric_difference(set1.begin(), set1.end(), set2.begin(), set2.end(), std::inserter(setU, setU.begin()));
	} else {
		std::cerr << "[ERROR] SET_OPERATOR unknown." << std::endl;
		assert(false);
	}

	return setU.size();

}

float PartialUpdDistanceBuilder::computeTimeDistance(size_t iParam1, size_t iParam2) {

	// Get sets
	std::set<size_t> &set1 = partialUpdateCost[iParam1];
	std::set<size_t> &set2 = partialUpdateCost[iParam2];

	// Create union of both
	std::set<size_t> setU;
	if(SET_OPERATOR == SUBDAG_UNION) {
		std::set_union(set1.begin(), set1.end(), set2.begin(), set2.end(), std::inserter(setU, setU.begin()));
	} else if(SET_OPERATOR == SUBDAG_SYMMETRIC_DIFFERENCE) {
		std::set_symmetric_difference(set1.begin(), set1.end(), set2.begin(), set2.end(), std::inserter(setU, setU.begin()));
	} else {
		std::cerr << "[ERROR] SET_OPERATOR unknown." << std::endl;
		assert(false);
	}

	// Approximate time
	float time = 0.;
	for(std::set<size_t>::iterator it=setU.begin(); it != setU.end(); ++it) {
		time += mapNodeDAG[(*it)]->getAverageTime();
	}

	return time;
}



} /* namespace Tools */
} /* namespace DAG */
