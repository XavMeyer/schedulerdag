//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file AddNode.cpp
 *
 * @date Nov 26, 2014
 * @author meyerx
 * @brief
 */
#include "AddNode.h"

#include <stddef.h>

namespace DAG {

AddNode::AddNode() : BaseNode() {
	res = 0;
}

AddNode::~AddNode() {
}

double AddNode::getRes() const {
	return res;
}

void AddNode::doProcessing() {
	res = 0;
	for(size_t i=0; i<values.size(); ++i){
		res += values[i];
	}
	//cout << "AddNode [" << getId() << "] processing -> res=" << res << endl;
}

bool AddNode::processSignal(BaseNode* aChild) {
	bool isProcessed = false;

	if(values.empty()) {
		values.assign(BaseNode::getNChildren(), 0.);
	}

	size_t rowId = BaseNode::getChildrenRow(aChild);
	const type_info &childtype = typeid(*aChild);

	if(childtype == typeid(MultNode)){
		MultNode *mn = static_cast<MultNode*>(aChild);
		values[rowId] = mn->getRes();
		isProcessed = true;
	}

	return isProcessed;
}


} /* namespace DAG */
