//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * ModelSpecificSample.cpp
 *
 *  Created on: 30 March 2017
 *      Author: meyerx
 */

#include "ModelSpecificSample.h"

#include <assert.h>

#include "Utils/Code/BinSerializationSupport.h" // IWYU pragma: keep

namespace Sampler {

ModelSpecificSample::ModelSpecificSample() : idSubSpace(-1) {
}

ModelSpecificSample::ModelSpecificSample(const ModelSpecificSample &toCopy) {
	copy(toCopy);
}

ModelSpecificSample::ModelSpecificSample(const Utils::Serialize::buffer_t &aBuffer) {
	load(aBuffer);
}

ModelSpecificSample::~ModelSpecificSample() {
	//idManager.freeId(id);
}

ModelSpecificSample& ModelSpecificSample::operator=(const ModelSpecificSample &toCopy) {
	copy(toCopy);
    return *this;
}

bool ModelSpecificSample::isInteger(int aInd) const {
	int ivSize = intValues.size();
	if(aInd < ivSize){
		return true;
	} else {
		return false;
	}
}

bool ModelSpecificSample::isDouble(int aInd) const {
	return !isInteger(aInd);
}

bool ModelSpecificSample::hasSameContinuousParameter(const ModelSpecificSample &other) const {
	for(size_t iX=0; iX<dblValues.size(); ++iX) {
		if(dblValues[iX] != other.dblValues[iX]) return false;
	}
	return true;
}


void ModelSpecificSample::incParameter(int aInd, double aIncr){
	int ivSize = intValues.size();
	if(aInd < ivSize){
		intValues[aInd] += aIncr;
	} else {
		dblValues[aInd-ivSize] += aIncr;
	}
}

void ModelSpecificSample::multParameter(int aInd, double aMult){
	int ivSize = intValues.size();
	if(aInd < ivSize){
		intValues[aInd] *= aMult;
	} else {
		dblValues[aInd-ivSize] *= aMult;
	}
}

double ModelSpecificSample::getDoubleParameter(int aInd) const {
	int ivSize = intValues.size();
	if(aInd < ivSize){
		return intValues[aInd];
	} else {
		return dblValues[aInd-ivSize];
	}
}

void ModelSpecificSample::setDoubleParameter(int aInd, double aVal){
	int ivSize = intValues.size();
	if(aInd < ivSize){
		intValues[aInd] = aVal;
	} else {
		dblValues[aInd-ivSize] = aVal;
	}
}

long int ModelSpecificSample::getIntParameter(int aInd) const {
	int ivSize = intValues.size();
	if(aInd < ivSize){
		return intValues[aInd];
	} else {
		return dblValues[aInd-ivSize];
	}
}

void ModelSpecificSample::setIntParameter(int aInd, long int aVal){
	int ivSize = intValues.size();
	if(aInd < ivSize){
		intValues[aInd] = aVal;
	} else {
		dblValues[aInd-ivSize] = aVal;
	}
}

size_t ModelSpecificSample::getNbParameters() const {
	return intValues.size() + dblValues.size();
}

size_t ModelSpecificSample::getIdSubSpace() const {
	return idSubSpace;
}

std::string ModelSpecificSample::getLabelSubSpaceUID() const {
	return labelSubSpaceUID;
}

void ModelSpecificSample::write(std::ostream &oFile, char sep) const {

	for(size_t i=0; i<markers.size(); ++i){
		oFile << sep << markers[i];
	}

	for(size_t i=0; i<intValues.size(); ++i){
		oFile << sep << intValues[i];
	}

	for(size_t i=0; i<dblValues.size(); ++i){
		oFile << sep << dblValues[i];
	}
}

void ModelSpecificSample::writeBin(std::ostream &oFile) const {
	assert(false && "NOT YET IMPLEMENTED");
	oFile.write(reinterpret_cast<const char*>(&labelSubSpaceUID), sizeof(size_t));
	// Serialize marker vector
	oFile.write(reinterpret_cast<const char*>(&markers[0]), markers.size()*sizeof(double));
	// Serialize int vector
	oFile.write(reinterpret_cast<const char*>(&intValues[0]), intValues.size()*sizeof(long int));
	// Serialize dbl vector
	oFile.write(reinterpret_cast<const char*>(&dblValues[0]), dblValues.size()*sizeof(double));
}

void ModelSpecificSample::writeBinFloat(std::ostream &oFile) const {
	assert(false && "NOT YET IMPLEMENTED");
	std::vector<float> tmpMarker(markers.begin(), markers.end());
	std::vector<float> tmpDbl(dblValues.begin(), dblValues.end());
	// Write Prior, Likelihood, Posterior
	oFile.write(reinterpret_cast<const char*>(&labelSubSpaceUID), sizeof(size_t));
	// Serialize marker vector
	oFile.write(reinterpret_cast<const char*>(&tmpMarker[0]), tmpMarker.size()*sizeof(float));
	// Serialize int vector
	oFile.write(reinterpret_cast<const char*>(&intValues[0]), intValues.size()*sizeof(long int));
	// Serialize dbl vector
	oFile.write(reinterpret_cast<const char*>(&tmpDbl[0]), tmpDbl.size()*sizeof(float));
}

std::string ModelSpecificSample::toString(char sep) const {
	std::stringstream ss;
//	ss << (changed ? "[t] " : "[f] "); // TODO REMOVE
	ss << labelSubSpaceUID << sep ;

	for(size_t i=0; i<markers.size(); ++i){
		ss << sep << markers[i];
	}

	for(size_t i=0; i<intValues.size(); ++i){
		ss << sep << intValues[i];
	}

	for(size_t i=0; i<dblValues.size(); ++i){
		ss << sep << dblValues[i];
	}
	return ss.str();
}

void ModelSpecificSample::setMarkers(const std::vector<double> &aMarkers) {
	if(!aMarkers.empty()){
		markers = aMarkers;
	}
}

void ModelSpecificSample::copy(const ModelSpecificSample &toCopy) {
	idSubSpace = toCopy.idSubSpace;
	labelSubSpaceUID = toCopy.labelSubSpaceUID;
	pInd = toCopy.pInd;
	intValues = toCopy.intValues;
	dblValues = toCopy.dblValues;
	markers = toCopy.markers;
}


Utils::Serialize::buffer_t ModelSpecificSample::save() {
	Utils::Serialize::buffer_t serializedBuffer;
	Utils::Serialize::save(*this, serializedBuffer);
	return serializedBuffer;
}

void ModelSpecificSample::load(const Utils::Serialize::buffer_t& buffer) {
	Utils::Serialize::load(buffer, *this);
}

template<class Archive>
void ModelSpecificSample::save(Archive & ar, const unsigned int version) const {
	ar << idSubSpace;
	ar << labelSubSpaceUID;
	ar << pInd;
	ar << intValues;
	ar << dblValues;
	ar << markers;
}

template<class Archive>
void ModelSpecificSample::load(Archive & ar, const unsigned int version) {
	ar >> idSubSpace;
	ar >> labelSubSpaceUID;
	ar >> pInd;
	ar >> intValues;
	ar >> dblValues;
	ar >> markers;
}

} // namespace Sampler
