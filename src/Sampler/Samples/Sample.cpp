//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Sample.cpp
 *
 *  Created on: 19 sept. 2013
 *      Author: meyerx
 */

#include "Sample.h"

#include <assert.h>

#include "Utils/Code/BinSerializationSupport.h" // IWYU pragma: keep
#include "Sampler/Samples/ModelSpecific/ModelSpecificSample.h"

namespace Sampler {

Sample::Sample() :  changed(true), evaluated(false), prior(0.0), likelihood(0.0), posterior(0.0) {
}

Sample::Sample(const Sample &toCopy) {
	copy(toCopy);
}

Sample::~Sample() {
}

Sample& Sample::operator=(const Sample &toCopy) {
	copy(toCopy);
    return *this;
}

bool Sample::isInteger(int aInd) const {
	if(aInd < (int)(intValues.size() + dblValues.size())) {
		int ivSize = intValues.size();
		if(aInd < ivSize){
			return true;
		} else {
			return false;
		}
	}
}

bool Sample::isDouble(int aInd) const {
	return !isInteger(aInd);
}

void Sample::incParameter(int aInd, double aIncr){
	if(aInd < (int)(intValues.size() + dblValues.size())) {
		int ivSize = intValues.size();
		if(aInd < ivSize){
			intValues[aInd] += aIncr;
		} else {
			dblValues[aInd-ivSize] += aIncr;
		}
	}
}

void Sample::multParameter(int aInd, double aMult){
	if(aInd < (int)(intValues.size() + dblValues.size())) {
		int ivSize = intValues.size();
		if(aInd < ivSize){
			intValues[aInd] *= aMult;
		} else {
			dblValues[aInd-ivSize] *= aMult;
		}
	}
}

double Sample::getDoubleParameter(int aInd) const {
	if(aInd < (int)(intValues.size() + dblValues.size())) {
		int ivSize = intValues.size();
		if(aInd < ivSize){
			return intValues[aInd];
		} else {
			return dblValues[aInd-ivSize];
		}
	}
}

void Sample::setDoubleParameter(int aInd, double aVal){
	if(aInd < (int)(intValues.size() + dblValues.size())) {
		int ivSize = intValues.size();
		if(aInd < ivSize){
			intValues[aInd] = aVal;
		} else {
			dblValues[aInd-ivSize] = aVal;
		}
	}
}

long int Sample::getIntParameter(int aInd) const {
	if(aInd < (int)(intValues.size() + dblValues.size())) {
		int ivSize = intValues.size();
		if(aInd < ivSize){
			return intValues[aInd];
		} else {
			return dblValues[aInd-ivSize];
		}
	}
}

void Sample::setIntParameter(int aInd, long int aVal){
	if(aInd < (int)(intValues.size() + dblValues.size())) {
		int ivSize = intValues.size();
		if(aInd < ivSize){
			intValues[aInd] = aVal;
		} else {
			dblValues[aInd-ivSize] = aVal;
		}
	}
}


size_t Sample::getNbParameters() const {
	return intValues.size() + dblValues.size();
}

void Sample::write(ostream &oFile, char sep) const {
	oFile << prior << sep;

	// Get rid of minus inf when LOG LIK
	if(likelihood == -std::numeric_limits<double>::infinity()) {
		oFile << -std::numeric_limits<double>::max() << sep;
	} else {
		oFile << likelihood << sep;
	}

	// Get rid of minus inf when LOG LIK
	if(posterior == -std::numeric_limits<double>::infinity()) {
		oFile << -std::numeric_limits<double>::max();
	} else {
		oFile << posterior;
	}

	for(size_t i=0; i<markers.size(); ++i){
		oFile << sep << markers[i];
	}

	for(size_t i=0; i<intValues.size(); ++i){
		oFile << sep << intValues[i];
	}

	for(size_t i=0; i<dblValues.size(); ++i){
		oFile << sep << dblValues[i];
	}

	oFile << endl;
}

void Sample::writeBin(ostream &oFile) const {
	// Write Prior, Likelihood, Posterior
	oFile.write(reinterpret_cast<const char*>(&prior), sizeof(double));
	oFile.write(reinterpret_cast<const char*>(&likelihood), sizeof(double));
	oFile.write(reinterpret_cast<const char*>(&posterior), sizeof(double));
	// Serialize marker vector
	oFile.write(reinterpret_cast<const char*>(&markers[0]), markers.size()*sizeof(double));
	// Serialize int vector
	oFile.write(reinterpret_cast<const char*>(&intValues[0]), intValues.size()*sizeof(long int));
	// Serialize dbl vector
	oFile.write(reinterpret_cast<const char*>(&dblValues[0]), dblValues.size()*sizeof(double));
}

void Sample::writeBinFloat(ostream &oFile) const {

	float tmp;
	vector<float> tmpMarker(markers.begin(), markers.end());
	vector<float> tmpDbl(dblValues.begin(), dblValues.end());
	// Write Prior, Likelihood, Posterior
	tmp = static_cast<float>(prior);
	oFile.write(reinterpret_cast<const char*>(&tmp), sizeof(float));
	tmp = static_cast<float>(likelihood);
	oFile.write(reinterpret_cast<const char*>(&tmp), sizeof(float));
	tmp = static_cast<float>(posterior);
	oFile.write(reinterpret_cast<const char*>(&tmp), sizeof(float));
	// Serialize marker vector
	oFile.write(reinterpret_cast<const char*>(&tmpMarker[0]), tmpMarker.size()*sizeof(float));
	// Serialize int vector
	oFile.write(reinterpret_cast<const char*>(&intValues[0]), intValues.size()*sizeof(long int));
	// Serialize dbl vector
	oFile.write(reinterpret_cast<const char*>(&tmpDbl[0]), tmpDbl.size()*sizeof(float));
}

string Sample::toString(char sep) const {
	stringstream ss;
//	ss << (changed ? "[t] " : "[f] "); // TODO REMOVE
	ss << prior << sep << likelihood << sep << posterior;

	for(size_t i=0; i<markers.size(); ++i){
		ss << sep << markers[i];
	}

	for(size_t i=0; i<intValues.size(); ++i){
		ss << sep << intValues[i];
	}

	for(size_t i=0; i<dblValues.size(); ++i){
		ss << sep << dblValues[i];
	}
	return ss.str();
}

void Sample::setMarkers(const std::vector<double> &aMarkers) {
	if(!aMarkers.empty()){
		markers = aMarkers;
	}
}

void Sample::setChanged(bool aChanged) {
	changed = aChanged;
}

bool Sample::hasChanged() const {
	return changed;
}

void Sample::setEvaluated(bool aEvaluated) {
	evaluated = aEvaluated;
}

bool Sample::isEvaluated() const {
	return evaluated;
}

bool Sample::hasSameContinuousParameters(const Sample &other) const {
	for(size_t iX=0; iX<dblValues.size(); ++iX) {
		if(dblValues[iX] != other.dblValues[iX]) return false;
	}


	return true;
}

Sample::ValueLocator Sample::getValueLocator(int aInd) const {
	mapValueLocators_t::const_iterator itFind = valueLocators.find(aInd);
	assert(itFind != valueLocators.end());									// TODO remove when tested
	size_t indMSS = itFind->second.indMSS;
	size_t indLoc = itFind->second.indLocalVal;
	return itFind->second;
}


void Sample::copy(const Sample &toCopy) {
	changed = toCopy.changed;
	evaluated = toCopy.evaluated;
	prior = toCopy.prior;
	likelihood = toCopy.likelihood;
	posterior = toCopy.posterior;
	intValues = toCopy.intValues;
	dblValues = toCopy.dblValues;
	markers = toCopy.markers;

	valueLocators = toCopy.valueLocators;
}

Utils::Serialize::buffer_t Sample::save() const {
	Utils::Serialize::buffer_t serializedBuffer;
	Utils::Serialize::save(*this, serializedBuffer);
	return serializedBuffer;
}

void Sample::load(const Utils::Serialize::buffer_t &buffer) {
	Utils::Serialize::load(buffer, *this);
}

template<class Archive>
void Sample::save(Archive & ar, const unsigned int version) const {
	ar & changed;
	ar & evaluated;

	ar & prior;
	ar & likelihood;
	ar & posterior;

	ar & intValues;
	ar & dblValues;
	ar & markers;

	ar & valueLocators;
}

template<class Archive>
void Sample::load(Archive & ar, const unsigned int version) {
	ar & changed;
	ar & evaluated;

	ar & prior;
	ar & likelihood;
	ar & posterior;

	ar & intValues;
	ar & dblValues;
	ar & markers;

	ar & valueLocators;
}

} // namespace Sampler
