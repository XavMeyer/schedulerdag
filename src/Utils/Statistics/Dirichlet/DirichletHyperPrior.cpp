//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file DirichletHyperPrior.cpp
 *
 * @date May 25, 2017
 * @author meyerx
 * @brief
 */
#include <assert.h>
#include "DirichletHyperPrior.h"

#include <boost/math/distributions/exponential.hpp>

#include "mpi.h"

namespace Utils {
namespace Statistics {

DirichletHyperPrior::DirichletHyperPrior(double aAlpha0, const std::vector<double> &aAlphas, dirichletHyperPriorType_t aType) :
		DIRICHLET_HP_TYPE(aType), alpha0(aAlpha0), alphas(aAlphas), dirichletPDF(alphas) {

	if(DIRICHLET_HP_TYPE == EXP_DIRICHLET_HP_TYPE) {
		expDistr = boost::math::exponential_distribution<>(1./alpha0);
	} else if(DIRICHLET_HP_TYPE == GAMMA_HP_TYPE) {
		for(size_t iG=0; iG<alphas.size(); ++iG) {
			gammasDistr.push_back(boost::math::gamma_distribution<>(alphas[iG], alpha0));
		}
	} else {
 		assert(false && "Unsupported 'DIRICHLET_HP_TYPE'.");
 	}
}

DirichletHyperPrior::DirichletHyperPrior(const std::vector<double> &aParameters, dirichletHyperPriorType_t aType) :
		DIRICHLET_HP_TYPE(aType), alpha0(aParameters.front()), alphas(aParameters.begin()+1, aParameters.end()),
		dirichletPDF(alphas) {

	if(DIRICHLET_HP_TYPE == EXP_DIRICHLET_HP_TYPE) {
		expDistr = boost::math::exponential_distribution<>(1./alpha0);
	} else if(DIRICHLET_HP_TYPE == GAMMA_HP_TYPE) {
		for(size_t iG=0; iG<alphas.size(); ++iG) {
			gammasDistr.push_back(boost::math::gamma_distribution<>(alphas[iG], alpha0));
		}
	} else {
 		assert(false && "Unsupported 'DIRICHLET_HP_TYPE'.");
 	}
}

DirichletHyperPrior::~DirichletHyperPrior() {
}

std::vector<double> DirichletHyperPrior::drawFromDistribution(RNG *aRNG) const {
	std::vector<double> vals(alphas.size());

	// simulated parameters for dirichlet(thetas) given parameters alpha0 and alphas
	if(DIRICHLET_HP_TYPE == EXP_DIRICHLET_HP_TYPE) {
		// assume thetas ~ v * m_i
		// draw v ~ exp(alpha0)
		double invScale = aRNG->genExponential(1./alpha0);
		// draw m_i ~ dirichlet(alphas)
		vals = aRNG->genDirichlet(alphas);
		// compute v*m_i
		for(size_t iV=0; iV<alphas.size(); ++iV) {
			vals[iV] *= invScale;
		}
	} else if (DIRICHLET_HP_TYPE == GAMMA_HP_TYPE) {
		// assume thetas ~ v*m_i
		// v*m_i ~ gamma(alphas_i, alpha0)
		for(size_t iV=0; iV<alphas.size(); ++iV) {
			vals[iV] = aRNG->genGamma(alphas[iV], alpha0);
		}
		// Given that scale parameters is the same for all thetas (alpha0)
		// v = sum(val_i) and m_i = val_i/sum(val_i) = val_i/v
 	} else {
 		assert(false && "Unsupported 'DIRICHLET_HP_TYPE'.");
 	}

	return vals;
}

double DirichletHyperPrior::computePDF(const std::vector<double> &X) const {
	assert(X.size() == alphas.size());

	// Compute probability of X from dirichlet(X) given parameters alpha0 and alphas
	double prob = 1.0;
	if(DIRICHLET_HP_TYPE == EXP_DIRICHLET_HP_TYPE) {
		// Remember that X ~ v * m_i
		double v = 0;
		for(size_t iX=0; iX<X.size(); ++iX) v += X[iX];
		std::vector<double> M(X);
		for(size_t iM=0; iM<M.size(); ++iM) M[iM] /= v;

		double probV = boost::math::pdf(expDistr, v);
		double probM = dirichletPDF.computePDF(M);

		prob = probV*probM;

	} else if (DIRICHLET_HP_TYPE == GAMMA_HP_TYPE) {
		// Remember that X ~ v * m_i and  v*m_i ~ gamma(alphas_i, alpha0)
		for(size_t iX=0; iX<alphas.size(); ++iX) {
			prob *= boost::math::pdf(gammasDistr[iX], X[iX]);
		}
		// Given that scale parameters is the same for all thetas (alpha0)
		// v = sum(val_i) and m_i = val_i/sum(val_i) = val_i/v
	} else {
		assert(false && "Unsupported 'DIRICHLET_HP_TYPE'.");
	}

	return prob;
}



} /* namespace Statistics */
} /* namespace Utils */
