//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file CodonFrequencies.cpp
 *
 * @date Feb 18, 2015
 * @author meyerx
 * @brief
 */
#include "CodonFrequencies.h"

#include <assert.h>

#include "Utils/MolecularEvolution/DataLoader/Alignment/Frequencies/../MSA.h"
#include "Utils/MolecularEvolution/Definitions/Codons.h"
#include "Utils/MolecularEvolution/Definitions/Nucleotides.h"

namespace MolecularEvolution {
namespace DataLoader {

/// Max number of iterations to resolve codon frequencies in presence of ambiguous codons.
const size_t CodonFrequencies::CODON_FREQ_MAX_ITER  = 20;
/// Max cumulative difference between an iteration and the next to stop iterations to resolve codon frequencies in presence of ambiguous codons.
const double CodonFrequencies::CODON_FREQ_MAX_ERROR = 1e-12;

CodonFrequencies::CodonFrequencies(const MSA &aMSA, const bool aWithStops) : msa(aMSA), withStops(aWithStops) {
	assert(msa.getSequenceType() == MSA::CODON);

}

CodonFrequencies::~CodonFrequencies() {
}



std::vector<double> CodonFrequencies::getCodonF61() {
	using MolecularEvolution::Definition::Codons;

	if(!frequenciesF61.empty()) return frequenciesF61;

	size_t NB_CODON = withStops ? Codons::getInstance()->NB_CODONS : Codons::getInstance()->NB_CODONS_WO_STOP;
	const std::vector<bool> &validSite = msa.getSiteValidity();
	const std::vector<Alignment> &alignments = msa.getAlignments();

	frequenciesF61.assign(NB_CODON, 0.);

	for(size_t iA=0; iA<msa.getNAlignments(); ++iA) {  // For each alignment

		for(size_t iC=0; iC< msa.getNSite(); ++iC) {
			if(validSite[iC]) {
				Alignment::vecCode_t code = alignments[iA].getCodonSequence(iC, msa.isWithStops());
				// Increment is 1/(NB_CODON*NB_AMBIGUOUS_CODON)
				//double val = 1./(double)(codonSequence[i].size());
				double val = 1.;
				for(size_t j=0; j<code.size(); ++j) {
					frequenciesF61[code[j]] += val;
				}
			}
		}
	}

	double denom = msa.getNValidSite()*alignments.size();
	for(size_t i=0; i<frequenciesF61.size(); ++i) {
		frequenciesF61[i] /= denom;
	}

	return frequenciesF61;
}


std::vector<double> CodonFrequencies::getCodonFrequency(const codonFrequencies_t codonFreqType) {
	if(codonFreqType == Uniform) {
		return getCodonUniform();
	} else if(codonFreqType == F61) {
		return getCodonF61();
	} else {
		std::cerr << "Error : std::vector<double> CodonFrequencies::getCodonFrequency(const codonFrequencies_t codonFreqType); "<< std::endl;
		std::cerr << "Unknown type of codon frequency."<< std::endl;
		abort();
		return std::vector<double>(1,0.);
	}
}

std::vector<double> CodonFrequencies::getCodonUniform() {
	using MolecularEvolution::Definition::Nucleotides;
	using MolecularEvolution::Definition::Codons;

	size_t NB_CODON = withStops ? Codons::getInstance()->NB_CODONS : Codons::getInstance()->NB_CODONS_WO_STOP;
	double value = 1./NB_CODON;

	std::vector<double> cFreq(NB_CODON, value);

	return cFreq;
}




} /* namespace DataLoader */
} /* namespace MolecularEvolution */
