//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file TreeManager.cpp
 *
 * @date Nov 3, 2015
 * @author meyerx
 * @brief
 */
#include "TreeManager.h"

#include <assert.h>
#include <stddef.h>

#include "Utils/MolecularEvolution/TreeReconstruction/Tree.h"
#include "Utils/MolecularEvolution/TreeReconstruction/TreeNode.h"
#include "Parallel/Manager/MCMCManager.h"

namespace MolecularEvolution {
namespace TreeReconstruction {

TreeManager::TreeManager(const std::vector<std::string> &aOrderedTaxaNames) :
		mcmcMgr(Parallel::mcmcMgr()), orderedTaxaNames(aOrderedTaxaNames) {

	for(size_t iN=0; iN<orderedTaxaNames.size(); ++iN) {
		taxaNameMap[orderedTaxaNames[iN]] = iN;
	}
}

TreeManager::~TreeManager() {
}

void TreeManager::setTree(Tree::sharedPtr_t aTree) {
	tree = aTree;
}

Tree::sharedPtr_t TreeManager::getCurrentTree() {
	return tree;
}

Tree::sharedPtr_t TreeManager::getTree(long int aTreeIdx) {
	if(tree->getHashKey() != aTreeIdx) {
		std::cerr << "[" << Parallel::mpiMgr().getRank() << "] currentTree = " << tree->getHashKey() << " -- asked tree = " << aTreeIdx << std::endl;
	}
	assert(tree->getHashKey() == aTreeIdx);
	return tree;
}

const std::vector<std::string>& TreeManager::getOrderedNames() const {
	return orderedTaxaNames;
}

const TreeManager::nameMap_t & TreeManager::getNameMapping() const {
	return taxaNameMap;
}

std::string TreeManager::getTreeNodeName(const TreeNode *node) const {
	if(!node->isTerminal()) {
		return "";
	} else {
		assert(node->getIdName() < (long int)orderedTaxaNames.size());
		return orderedTaxaNames[node->getIdName()];
	}
}

void TreeManager::memorizeTree() {
	// Manager has to keep track of this tree
	if(mcmcMgr.isOutputManager()) {
		itTreeStringMap_t it = treeStringMap.find(tree->getHashKey());
		if(it != treeStringMap.end() && it->second != tree->getIdString()) {
			std::cerr << "[WARNING] void TreeManager::memorizeTree(Tree::sharedPtr_t aTree);" << std::endl;
			std::cerr << "There migh have been a hash collision for tree : " << std::endl;
			std::cerr << tree->getIdString() << std::endl;
		}
		treeStringMap[tree->getHashKey()] = tree->getIdString();
	}
}

void TreeManager::setProposedMove(Move::sharedPtr_t aMove) {
	moveManager.setLocalMove(aMove);
}

Move::sharedPtr_t TreeManager::getProposedMove() {
	return moveManager.getLocalMove();
}

void TreeManager::rejectProposedMove() {
	Move::sharedPtr_t aMove = moveManager.getLocalMove();
	if(aMove != NULL) {
		if(aMove->getMoveType() == Move::ESPR) {
			espr.undoMove(tree, aMove);
		} else if (aMove->getMoveType() == Move::STNNI) {
			stnni.undoMove(tree, aMove);
		} else {
			std::cerr << "[ERROR] Tree::sharedPtr_t TreeManager::getTree(long int aTreeIdx);" << std::endl;
			std::cerr << "Unknown move type for move : " << std::endl << aMove->toString() << std::endl;
			assert(false);
		}
		// Move has been "processed"
		moveManager.resetLocalMove();
	}
}

void TreeManager::acceptProposedMove() {
	Move::sharedPtr_t aMove = moveManager.getLocalMove();
	assert(aMove != NULL);
	moveManager.shareLocalMove();
	// Move has been "processed"
	moveManager.resetLocalMove();
	// Memorize the new tree
	memorizeTree();
}

void TreeManager::acceptRemoteMove() {
	// First we reset pending moves that will not be accepted (if there is one)
	rejectProposedMove();
	// Then we wait for the remote move
	Move::sharedPtr_t aMove = moveManager.getRemoteMove();
	assert(aMove!=NULL);
	// We apply locally this move
	if(aMove->getMoveType() == Move::ESPR) {
		espr.applyMove(tree, aMove);
	} else if (aMove->getMoveType() == Move::STNNI) {
		stnni.applyMove(tree, aMove);
	} else {
		std::cerr << "[ERROR] Tree::sharedPtr_t TreeManager::getTree(long int aTreeIdx);" << std::endl;
		std::cerr << "Unknown move type for move : " << std::endl << aMove->toString() << std::endl;
		assert(false);
	}
	// Memorize the new tree
	memorizeTree();
}

void TreeManager::initTreeFile(const std::string &fnPrefix, const std::string &fnSuffix, std::streamoff offset) {
	std::stringstream ssFName;
	ssFName << fnPrefix << Parallel::mpiMgr().getRank() << fnSuffix << ".logTrees";
	treeStringFN = ssFName.str();
	oFile.reset(new Utils::Checkpoint::File(treeStringFN, offset));
	if(offset == 0) { // init the ordered taxon names
		for(size_t iT=0; iT<orderedTaxaNames.size(); ++iT) {
			oFile->getOStream() << orderedTaxaNames[iT] << "\t";
		}
		oFile->getOStream() << std::endl;
	}
}

std::streamoff TreeManager::getTreeFileLength() const {
	return oFile->getSize();
}

void TreeManager::writeTreeString(const std::vector<long int> &treesHash) {

	assert(mcmcMgr.isOutputManager());

	// For each hash written in sample log
	for(size_t iH=0; iH<treesHash.size(); ++iH) {
		itTreeStringMap_t it = treeStringMap.find(treesHash[iH]); // Search the tree
		if(it == treeStringMap.end()) {
			std::cerr << "[ERROR] void TreeManager::writeTreeString(...);" << std::endl;
			std::cerr << "Tree with hash " << treesHash[iH] << " was not found." << std::endl;
			assert(it != treeStringMap.end());
		}
		// Write the tree
		oFile->getOStream() << it->first << std::endl << it->second << std::endl;
	}

	// Clean the memorized tree string
	treeStringMap.clear();

	// But must keep track of last trees
	treeStringMap[tree->getHashKey()] = tree->getIdString();
}


} /* namespace TreeReconstruction */
} /* namespace MolecularEvolution */




