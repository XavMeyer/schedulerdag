//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  X
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file Types.h
 *
 * @date Apr 20, 2017
 * @author meyerx
 * @brief
 */
#ifndef MOLECULAR_EVOLUTION_TYPES_H_
#define MOLECULAR_EVOLUTION_TYPES_H_


#include <bitset>
#include <string>
#include <vector>


namespace MolecularEvolution {
namespace Definition {

typedef std::bitset< 16 > bitset16b_t;
typedef std::vector<std::string> profile_t;
typedef std::vector<profile_t> vecProfiles_t;

}
}

#endif /* MOLECULAR_EVOLUTION_TYPES_H_ */
