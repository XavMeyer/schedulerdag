//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file COEVGTRMF.cpp
 *
 * @date Mar 29, 2017
 * @author meyerx
 * @brief
 */
#include "COEVGTRMF.h"

namespace MolecularEvolution {
namespace MatrixUtils {

const size_t COEVGTR_MF::COEV_NB_COEFF = 4;
const std::vector<std::string> COEVGTR_MF::PARAMETERS_NAME =  boost::assign::list_of("R1")("R2")("S")("D");


COEVGTR_MF::COEVGTR_MF(const std::vector<std::string> &aProfile) :
		NucleotidePairMF(COEV_NB_COEFF), profile(aProfile) {
	defineProfileIdx();
	MatrixFactory::initOperations();
}

COEVGTR_MF::COEVGTR_MF(const std::vector<std::string> &aProfile, const bool aDoRowsSumtoZero) :
		NucleotidePairMF(COEV_NB_COEFF, aDoRowsSumtoZero), profile(aProfile) {
	defineProfileIdx();
	MatrixFactory::initOperations();
}


COEVGTR_MF::~COEVGTR_MF() {
}

void COEVGTR_MF::doInitOperations() {
	// For each combination, call defineOperation or defineOperationDiagonal
	super::scanCombination();
}

Operations::Base* COEVGTR_MF::defineOperation(size_t iFrom, size_t iTo) const {

	//std::cout << "iFrom = " << iFrom << ", iTo = " << iTo << "\t ||";

	div_t resDivFrom = div(iFrom, MD::getNucleotides()->NB_BASE_NUCL);
	int iFrom1 = resDivFrom.quot;
	int iFrom2 = resDivFrom.rem;

	div_t resDivTo = div(iTo, MD::getNucleotides()->NB_BASE_NUCL);
	int iTo1 = resDivTo.quot;
	int iTo2 = resDivTo.rem;
	//std::cout << "Pair 1 (from) : " << iFrom1 << ", " << iFrom2;
	//std::cout << "\t || Pair 2 (to) : " << iTo1 << ", " << iTo2;

	int iFromProfileIdx = getProfileIdx(iFrom);
	int iToProfileIdx = getProfileIdx(iTo);

	//std::cout << "\t|| from idx = " <<  iFromProfileIdx << ", to idx = " << iToProfileIdx;

	std::vector<size_t> coeffId;
	/*if(iFromProfileIdx == -1 && iToProfileIdx == -1) { // Both are not in profile
		if(iFrom1 != iTo1) { // r1
			coeffId.push_back(R1_ID);
			//std::cout << "\t|| push R1";
		} else if(iFrom2 != iTo2) { // r2
			coeffId.push_back(R2_ID);
			//std::cout << "\t|| push R2";
		} else { // Should not happen
			std::cerr << "[ERROR] in COEVGTR_MF::defineOperation(..);" << std::endl;
			std::cerr << "Two symbols are out of profile and are differing at the two positions" << std::endl;
			abort();
		}
	} else if(iFromProfileIdx >= 0 && iToProfileIdx == -1) { // s : from profile to non-profile
		coeffId.push_back(S_ID);
		//std::cout << "\t|| push S";
	} else if(iFromProfileIdx == -1 && iToProfileIdx >= 0) { // d : from non-profile to profile
		coeffId.push_back(D_ID);
		//std::cout << "\t|| push D";
	} else { // Should not happen
		std::cerr << "[ERROR] in COEVGTR_MF::defineOperation(..);" << std::endl;
		std::cerr << "Both nucleotide pairs are in the profile but are not the same." << std::endl;
		abort();
	}
	//std::cout << std::endl;
*/
	//std::cout << myOp->getIdxMatrix() << std::endl;
	Operations::Base* myOp = new Operations::CoeffsDotFreq(iFrom, iTo, N, coeffId);
	return myOp;
}

void COEVGTR_MF::defineProfileIdx() {
	for(size_t iP=0; iP<profile.size(); ++iP) {
		int iNucl1 = MD::getNucleotides()->getNucleotideIdx(profile[iP][0]);
		int iNucl2 = MD::getNucleotides()->getNucleotideIdx(profile[iP][1]);

		size_t profileCode = iNucl1*MD::getNucleotides()->NB_BASE_NUCL + iNucl2;
		profileIdx.push_back(profileCode);
	}
}

int COEVGTR_MF::getProfileIdx(size_t profileCode) const {
	for(size_t iP=0; iP<profileIdx.size(); ++iP) {
		if(profileIdx[iP] == profileCode) return iP;
	}
	return -1;
}

} /* namespace MatrixFactory */
} /* namespace MolecularDefinition */
