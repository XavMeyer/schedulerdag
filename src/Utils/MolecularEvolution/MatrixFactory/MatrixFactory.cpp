//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MatrixFactory.cpp
 *
 * @date Feb 3, 2015
 * @author meyerx
 * @brief
 */
#include "MatrixFactory.h"

#include <assert.h>

namespace MolecularEvolution {
namespace MatrixUtils {

MatrixFactory::MatrixFactory(const size_t aN, const size_t aNB_COEFF) :
		N(aN), NB_COEFF(aNB_COEFF), doRowsSumtoZero(true),
		matrix(N*N, 0.), frequencies(N, 0.), freqDotFreq(N*N, 0.), coefficients(NB_COEFF, 0.) {

	scalingFactor = 0.;

	if(doRowsSumtoZero) {
		diagonalOperations.resize(N);
	}
}

MatrixFactory::MatrixFactory(const size_t aN, const size_t aNB_COEFF, const bool aDoRowsSumToZero) :
		N(aN), NB_COEFF(aNB_COEFF), doRowsSumtoZero(aDoRowsSumToZero),
		matrix(N*N, 0.), frequencies(N, 0.), freqDotFreq(N*N, 0.), coefficients(NB_COEFF, 0.) {

	scalingFactor = 0.;

	if(doRowsSumtoZero) {
		diagonalOperations.resize(N);
	}
}

MatrixFactory::~MatrixFactory() {

	for(size_t i=0; i<operations.size(); ++i){
		delete operations[i];
	}

	for(size_t i=0; i<diagonalOperations.size(); ++i){
		delete diagonalOperations[i];
	}
}

void MatrixFactory::initOperations() {

	// Fill the operations vector
	doInitOperations();

	// Sort operations so they are optimised for matrix order
	std::sort(operations.begin(), operations.end(), Operations::OperationMaxIdxMatrixCompare());

	for(size_t i=0; i<operations.size(); ++i){
		std::vector<Operations::MultStore> tmp = operations[i]->toInstructions(scalingFactor, matrix, frequencies, freqDotFreq, coefficients);
		instructions.insert(instructions.end(), tmp.begin(), tmp.end());
	}


	if(doRowsSumtoZero) {
		std::sort(diagonalOperations.begin(), diagonalOperations.end(), Operations::OperationMaxIdxMatrixCompare());

		for(size_t i=0; i<diagonalOperations.size(); ++i){
			std::vector<Operations::MultStore> tmp = diagonalOperations[i]->toInstructions(scalingFactor, matrix, frequencies, freqDotFreq, coefficients);
			instructions.insert(instructions.end(), tmp.begin(), tmp.end());
		}
	}

}

void MatrixFactory::setFrequencies(const std::vector<double>& aFrequencies) {
	assert(aFrequencies.size()==N);
	frequencies = aFrequencies;
	for(size_t i=0; i<N; ++i) {
		for(size_t j=0; j<N; ++j) {
			freqDotFreq[i+j*N] = frequencies[i]*frequencies[j];
		}
	}
}

void MatrixFactory::setCoefficients(const std::vector<double>& aCoefficients) {
	assert(aCoefficients.size()==NB_COEFF);
	coefficients = aCoefficients;
}

void MatrixFactory::fillMatrix(const std::vector<double>& aCoefficients) {

	scalingFactor = 0.0;

	setCoefficients(aCoefficients);

	for(size_t i=0; i<instructions.size(); ++i) {
		instructions[i]();
		/*std::cout << instructions[i].toString() << std::endl;
		std::cout << scalingFactor << std::endl;
		for(size_t iI=0; iI<N; iI++) {
			for(size_t iJ=0; iJ<N; iJ++) {
				std::cout << matrix[iI*N+iJ] << ", ";
			}
			std::cout << std::endl;
		}
		std::cout << "---------------------------------------------" << std::endl;*/
	}

}

size_t MatrixFactory::getNBCoefficients() const {
	return NB_COEFF;
}


double MatrixFactory::getScalingFactor() const {
	//std::cout << scalingFactor << std::endl;
	return scalingFactor;
}

size_t MatrixFactory::getMatrixDimension() const {
	return N;
}

std::vector<double>& MatrixFactory::getMatrix() {
	return matrix;
}

const std::vector<double>& MatrixFactory::getMatrix() const {
	return matrix;
}


} /* namespace MatrixFactory */
} /* namespace MolecularDefinition */
