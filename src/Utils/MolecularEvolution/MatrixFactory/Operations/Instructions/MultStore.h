//    HOGAN is an implementation of a parallel Metropolis-Hastings algorithm 
//    developped for evolutionnary biology model.
//    Copyright (C) 2016  Xavier Meyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
/**
 * @file MultStore.h
 *
 * @date Feb 5, 2015
 * @author meyerx
 * @brief
 */
#ifndef MULTSTORE_H_
#define MULTSTORE_H_

#include <iostream>
#include <sstream>

namespace MolecularEvolution {
namespace MatrixUtils {

namespace Operations {

class MultStore {
public:
	MultStore(const double &aOpA, const double &aOpB, const double &aAlpha, double &aDest);
	~MultStore();

	void operator()() const {

		if(!alphaEqualsZero) {
			double tmp = (*dest) * (*alpha);
			(*dest) = tmp + (*opA)*(*opB);
		} else {
			(*dest) = (*opA)*(*opB);
		}
	}

	std::string toString() const;

public:
	static const double ZERO, ONE, NEG_ONE;

private:

	/*enum OPERATIONS {ASSIGN, MULT, ADD, ADDMULT};
	typedef OPERATIONS operations_t;
	operations_t myOperation;*/

	bool alphaEqualsZero;
	const double *opA, *opB, *alpha;
	double *dest;

};

} /* namespace Operations */

} /* namespace MatrixFactory */
} /* namespace MolecularDefinition */

#endif /* MULTSTORE_H_ */
